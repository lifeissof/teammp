﻿using UnityEngine;
using System.Collections;

public class GetURL : MonoBehaviour {

    void Start()
    {
        string url = "https://game-kapi.kakao.com/v1/user/signup";
        WWW www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.data);
        }
        else {
            Debug.Log("WWW Error: " + www.error);
        }
    }   
}
