﻿using UnityEngine;
using System.Collections;

public class EndTest : MonoBehaviour {

    Ray ray;
    RaycastHit hit;

    public GameObject ResultBox;
    private bool EndState;



	// Use this for initialization
	void Start () {

        EndState = false;
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
        {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.tag.Substring(0, 3) == "End")
            {
                this.gameObject.SetActive(false);
                ResultBox.SetActive(true);
            }
        }
	
	}
}
