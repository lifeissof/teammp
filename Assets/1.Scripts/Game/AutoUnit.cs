﻿using UnityEngine;
using System.Collections;

public class AutoUnit : Unit
{

    public GameObject Target;


    public float ShortDistance = Mathf.Infinity;
    public float CurrDistance;



    public bool EmptyHP = false;


    public void StateMachine()
    {
        switch (state)
        {
            case (int)unitState.IDLE:
                {
                    idle();
                    setTarget();
                    break;
                }
            case (int)unitState.WALK:
                {
                    walk();
                    setTarget();
                    break;
                }
            case (int)unitState.ATTACK:
                {
                    attack();
                    setTarget();
                    break;
                }

            case (int)unitState.DIE:
                {
                    if (EmptyHP == false)
                    {
                        StartCoroutine(die());
                    }
                    break;
                }
            case (int)unitState.SEE:
                {
                    see();
                    setTarget();
                    break;
                }
            default:
                break;
        }

        if (state_L.Hp > 0)
        {
            //instanceHpBar.transform.position = this.gameObject.transform.position + (hpBarPosition * NScreen.ScaleNum);
            //instanceHpBar.transform.GetChild(0).GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(((float)state_L.Hp / (float)state_L.MaxHp) * 64, 13);
        }

        if (state_L.Hp <= 0)
        {
            state = (int)unitState.DIE;
        }

        if ((ShortDistance > state_L.attackRange || ShortDistance == Mathf.Infinity) && state_L.Hp > 0 && (state != (int)unitState.WALK && state != (int)unitState.SEE))
        {
            Atk_Timer = 0;

            state = (int)unitState.WALK;
            if (state_L.movingDelay > 0)
                StartCoroutine("WalkTime");
        }
        else if ((ShortDistance <= state_L.attackRange && ShortDistance >= 0) && state_L.Hp > 0)
        {
            if (Target != null)
            {
                state = (int)unitState.ATTACK;
            }
        }

        else
        {
            Target = null;
        }


        if (Target == null)
        {
            ShortDistance = Mathf.Infinity;
            CurrDistance = Mathf.Infinity;
        }
    }

}
