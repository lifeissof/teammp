﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class Result : MonoBehaviour {

    public tk2dTextMesh scoreText;
    public tk2dTextMesh bestScoreText;


    public tk2dTextMesh FireMaterialDropNum;
    public tk2dTextMesh WaterMaterialDropNum;
    public tk2dTextMesh PlantMaterialDropNum;
    public tk2dTextMesh NormalLevelMaterialDropNum;
    public tk2dTextMesh RareLevelMaterialDropNum;
    public tk2dTextMesh GoldsNum;
    public GameObject[] ResultStarSprite = new GameObject[3];

    public BattleScroll battle;

    bool isWin;
    int stageStarNumber;

    public void Init(bool isVictory)
    {
        int standard = 60;
        int score = SetGrade(battle.GetTime() / standard);
        int bestScore = ServerInterface.Instance.userState.GetScore();
        int stage = ServerInterface.Instance.userState.GetStage();

        LoadingManager loading = new LoadingManager(isVictory);

        if (isVictory == false)
        {
            score /= 10;
            isWin = false;
          
        }
        else
        {
            isWin = true;
            int zone = (stage / 100) - 1;
            print(StageManager.currStage + "curstage!!!");
            print(StageManager.selectStage + "selectstage!!!");
            print("stageStarNumber : " + stageStarNumber);


          

            
            if (StageManager.trueStage < 15)
            {
                if (StageManager.selectStage == StageManager.trueStage)
                {
                    stage++;
                    ServerInterface.Instance.userState.SetStage(stage);    // 직접 넣음
                    ServerInterface.Instance.userState.userStage.SetStarNumber(StageManager.selectStage - 1, stageStarNumber); // 직접 넣음
                    //if (StageManager.trueStage == 1)
                    //{
                    //    ServerInterface.Instance.UpdateStageData(StageManager.trueStage, stageStarNumber); //Update하면 기존에 있는 데이터들 다 덮어써서 전 스테이지 별 갱신하기 힘듬.
                   // }
                    //else
                    //{
                        ServerInterface.Instance.AddStageData(StageManager.selectStage - 1, stageStarNumber);
                   // }
                }
                else if(StageManager.selectStage < StageManager.trueStage)
                {
                    
                    if (stageStarNumber > ServerInterface.Instance.userState.userStage.GetStarNumber(StageManager.selectStage - 1))
                    {
                        ServerInterface.Instance.userState.userStage.SetStarNumber(StageManager.selectStage - 1, stageStarNumber); // 직접 넣음 
                        ServerInterface.Instance.ModifyStageData(StageManager.selectStage-1, stageStarNumber);
                      //  print("modifyyyyyyyyyyyyyy");
                    }
                }
            }
            else // 스테이지 15 이상
            {
                if (StageManager.selectStage == StageManager.trueStage)
                {
                    if(StageManager.trueStage == 15)
                    {
                        stage += 86;
                    }
                    else if(StageManager.trueStage == 30)
                    {
                        stage += 186;
                    }
                    else if (StageManager.trueStage == 45)
                    {
                        stage += 286;
                    }
                    else if (StageManager.trueStage == 60)
                    {
                        stage += 386;
                    }
                    else
                    {
                        stage += 1;
                    }
                    ServerInterface.Instance.userState.SetStage(stage);    // 직접 넣음
                    ServerInterface.Instance.userState.userStage.SetStarNumber(StageManager.selectStage - 1, stageStarNumber); // 직접 넣음
                    ServerInterface.Instance.AddStageData(StageManager.selectStage - 1, stageStarNumber);
                }
                else if (StageManager.selectStage < StageManager.trueStage)
                {
                    if (stageStarNumber > ServerInterface.Instance.userState.userStage.GetStarNumber(StageManager.selectStage - 1))
                    {
                        ServerInterface.Instance.userState.userStage.SetStarNumber(StageManager.selectStage - 1, stageStarNumber); // 직접 넣음 
                        ServerInterface.Instance.ModifyStageData(StageManager.selectStage - 1, stageStarNumber);
                    }
                }
            }
        }
        StartCoroutine("ResultStarOper");

        if (score > bestScore)
        {
            bestScore = score;
            bestScoreText.text = scoreText.text;
        }
        scoreText.text = score.ToString();   //player 레벨, 시간....
        bestScoreText.text = bestScore.ToString();

        FireMaterialDropNum.text = battle.FireMaterialDropNum.ToString();
        WaterMaterialDropNum.text = battle.WaterMaterialDropNum.ToString();
        PlantMaterialDropNum.text = battle.PlantMaterialDropNum.ToString();
        NormalLevelMaterialDropNum.text = battle.NLMaterialDropNum.ToString();
        RareLevelMaterialDropNum.text = battle.RLMaterialDropNum.ToString();
        GoldsNum.text = 3000.ToString(); // battle.GoldDropNum.ToString();  // 없길래 일단 넣어둠 랜덤으로 처리 후 주석 해제 필요
        //골드는 여기서 랜덤으로 처리할 예정이다.
        // 골드를 아직 처리하지 않아서 서버에 임시로 100원씩 넣어놓음 골드를 처리하면 밑 함수에 골드 변수를 넣어줘야함 16 08 05
        //서버 재료 최신화 하는 함수 호출
        ServerInterface.Instance.GetGameReward(bestScore, battle.FireMaterialDropNum, battle.WaterMaterialDropNum, battle.PlantMaterialDropNum, battle.NLMaterialDropNum, battle.RLMaterialDropNum, 3000);
        
        }

    int SetGrade(int grade)
    {
        int weight;
        switch(grade)
        {
            case 0:
                weight = 100000 + (60*(grade+1)- battle.GetTime());
                stageStarNumber = 3;
                break;
            case 1:
                weight = 10000 + (60 * (grade + 1) - battle.GetTime());
                stageStarNumber = 2;
                break;
            case 2:
                weight = 1000 + (60 * (grade + 1) - battle.GetTime());
                stageStarNumber = 1;
                break;
            case 3:
                weight = 100 + (60 * (grade + 1) - battle.GetTime());
                stageStarNumber = 1;
                break;
            case 4:
                weight = 10 + (60 * (grade + 1) - battle.GetTime());
                stageStarNumber = 1;
                break;
            default:
                weight = 1 + (60 * (grade + 1) - battle.GetTime());
                stageStarNumber = 1;
                break;
        }
        return weight;
    }


    public IEnumerator ResultStarOper()
    {
        if (isWin == false)
        {
            for (int i = 0; i < 3; ++i)
            {
                ResultStarSprite[i].GetComponent<tk2dSprite>().SetSprite("star_n");
                ResultStarSprite[i].transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f).SetEase(Ease.OutBounce);
                yield return new WaitForSeconds(0.2f);
            }
        }
        else
        {
            for (int i = 0; i < stageStarNumber; ++i)
            {
                ResultStarSprite[i].GetComponent<tk2dSprite>().SetSprite("star_s");
                ResultStarSprite[i].transform.DOScale(new Vector3(1f, 1f, 1f), 0.2f).SetEase(Ease.OutBounce);
                yield return new WaitForSeconds(0.2f);

            }
        }
    }


}
