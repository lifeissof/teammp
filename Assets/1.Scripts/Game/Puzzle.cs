﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class Puzzle : MonoBehaviour {



    public  GameObject[,] PuzzleBoard = new GameObject[5, 5];
    public GameObject[] Puzzles = new GameObject[25];
    int[,] CheckArray = new int[25, 4] { { -1, 5, -1, 1 }, { -1, 6, 0, 2 }, { -1, 7, 1, 3 }, { -1, 8, 2, 4 }, { -1, 9, 3, -1 },
                                         { 0, 10, -1, 6 }, { 1, 11, 5, 7 }, { 2, 12, 6, 8 }, { 3, 13, 7, 9 }, { 4, 14, 8, -1 },
                                         { 5, 15, -1, 11 }, { 6, 16, 10, 12 }, { 7, 17, 11, 13 }, { 8, 18, 12, 14 }, { 9, 19, 13, -1 },
                                         { 10, 20, -1, 16 }, { 11, 21, 15, 17 }, { 12, 22, 16, 18 }, { 13, 23, 17, 19 }, { 14, 24, 18, -1 },
                                         { 15, -1, -1, 21}, { 16, -1, 20, 22 }, { 17, -1, 21, 23 }, { 18, -1, 22, 24 }, { 19, -1, 23, -1 }};


    public GameObject[] ResultStars = new GameObject[3];
    int checkCount = 0;
    int suffleCheckCount = 0;
    int MonsterGaugePoint = 0;
    int comboNum = 0;
  
    bool comboState = false;
    bool missState = false;

    float GaugeTime = 0;
    float ComboTime = 0;
    float currentTime = 0.0f;
    public bool SuffleState = false;
    public static GameObject particle;
    public GameObject GameEntry;


    public GameObject DestroyParticleRed;
    public GameObject DestroyParticlePink;
    public GameObject DestroyParticleBlue;
    public GameObject DestroyParticleYellow;
    public GameObject suffleObject;

    public GameObject comboObject;
    public GameObject missObject;

     public tk2dTextMesh HaveEnergyText;
     public tk2dTextMesh HaveActEnergyText;



    public tk2dSlicedSprite MonsterGauge;
    public tk2dSlicedSprite SkillGauge;
    public AudioClip puzzlePop;
    public AudioClip puzzleMiss;
    



    public GameObject TouchedElements = null;
    Queue<int> CheckNumberQueue = new Queue<int>();
    Queue<int> CheckSuffleQueue = new Queue<int>();




    Ray ray;
    RaycastHit hit;

    public static int boxIndex = 0;

	// Use this for initialization
	void Start () {


        StartCoroutine("Time");

        comboObject.SetActive(false);
        missObject.SetActive(false);

        SettingBoard();
        suffleCheck();


        //게임 시작시 행동력을 세팅해준다.

	}
	
	// Update is called once per frame
	void Update () {

        MonsterGauge.dimensions = new Vector2((float)(DataManage._srcGame[0].GetEnergy() * 3.6f), 75);
        SkillGauge.dimensions = new Vector2((float)(DataManage._srcAct[0].GetEnergy() * 7.2f), 50);
        HaveEnergyText.text = DataManage._srcGame[0].GetEnergy().ToString();
        HaveActEnergyText.text = DataManage._srcAct[0].GetEnergy().ToString();
        //print(DataManage._srcGame[0].GetEnergy().GetHashCode() / 100);

        ActGaugeUp();

        if (Input.GetMouseButtonDown(0))
        {
            print(Input.mousePosition);
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                if (DataManage._srcAct[0].GetEnergy() > 0)
                {
                    if (hit.collider.gameObject.name.Substring(0, 3) == "Box")
                    {

                        TouchedElements = hit.collider.gameObject; // 터치한 캐릭터를 저장해둔다.
                        CheckDes(TouchedElements);
                        //DestroyMatchedBlock();

                    }
                }
            }
        }

        SoundControl();
    }

    IEnumerator Time()
    {
        yield return new WaitForSeconds(1.0f);
        GaugeTime += 1.0f;
       
        StartCoroutine(Time());
    }
    IEnumerator ComboTimeCheck()
    {
        ComboTime = 0.0f;
        while(true)
        {
            yield return new WaitForSeconds(1.0f);
            ComboTime += 1.0f;
        }

        //StartCoroutine(ComboTimeCheck());
    }

    void SettingBoard()
    {
        int i, j;
        for (i = 0; i < 5; ++i)
        {
            for (j = 0; j < 5; ++j)
            {
                PuzzleBoard[i, j] = GameObject.Find(string.Format("Box{0}{1}", i, j));   
            }
        }

        for (int a = 0; a < 5; ++a)
        {
            for (int b = 0; b < 5; ++b)
            {
                if (PuzzleBoard[a, b].transform.childCount == 0)
                {

                    PuzzleBoard[a, b].SendMessage("CreateBox");
                }
            }
        }
    }

    public IEnumerator Suffle()
    {

        suffleObject.SetActive(true);

        for (int i = 0; i < 5; ++i)
        {
            for (int j = 0; j < 5; ++j)
            {
                
                PuzzleBoard[i, j].SendMessage("CreateBox");
            }
        }
        yield return new WaitForSeconds(1.0f);
        //WaitForSeconds(2.0f);
        suffleObject.SetActive(false);
    }

   // IEnumerator SuffleChange()
   // {

        //for (int i = 0; i < 16; ++i)
        //{
        //    Puzzles[i].SetActive(false);
        //}
        //suffleObject.SetActive(true);

        //yield return new WaitForSeconds(2.0f);

        //for (int i = 0; i < 16; ++i)
        //{
        //    Puzzles[i].SetActive(true);
        //}
        //suffleObject.SetActive(false);
   // }


    void CheckDes(GameObject target)
    {
        int selectPuzzlenNum = target.GetComponent<Box>().PuzzleValue;
        check(selectPuzzlenNum);
        StartCoroutine("DeleteBox");
    }

    void CheckElement(int eleType, int checkCount)
    {
        switch(checkCount)
        {
            case 2:
                DataManage._srcGame[eleType].AddEnergy(2);     //잠시 테스트를 위해 추가한 부분이다. 재료가 있으면 애니메이션을 함.
                break;

            case 3:
                DataManage._srcGame[eleType].AddEnergy(2);
                break;

            case 4:
                DataManage._srcGame[eleType].AddEnergy(2);
                break;

            case 5:
                DataManage._srcGame[eleType].AddEnergy(3);
                break;

            case 6:
                DataManage._srcGame[eleType].AddEnergy(3);
                break;

            case 7:
                DataManage._srcGame[eleType].AddEnergy(3);
                break;

            case 8:
                DataManage._srcGame[eleType].AddEnergy(3);
                break;

            case 9:
                DataManage._srcGame[eleType].AddEnergy(4);
                break;

            case 10:
                DataManage._srcGame[eleType].AddEnergy(4);
                break;

            case 11:
                DataManage._srcGame[eleType].AddEnergy(4);
                break;

            case 12:
                DataManage._srcGame[eleType].AddEnergy(5);
                break;

            case 13:
                DataManage._srcGame[eleType].AddEnergy(5);
                break;
            case 14:
                DataManage._srcGame[eleType].AddEnergy(6);
                break;
            case 15:
                DataManage._srcGame[eleType].AddEnergy(6);
                break;

            default:
                break;
        }

        if (DataManage._srcAct[0].GetEnergy() >= 5)
        {
            DataManage._srcAct[0].AddEnergy(-5);   //3개이상을 눌렀을시 5 감소
        }
        else
        {
            DataManage._srcAct[0].SetEnergy(0);
        }    
    }

    IEnumerator showCombo()
    {
        if (comboState == true)
        {
            if (currentTime < 2.0f)
            {
                    comboFunc(comboNum);
                        
                    comboObject.transform.position = new Vector3(hit.point.x , hit.point.y , 1);
                    comboObject.transform.GetChild(0).GetComponent<tk2dTextMesh>().text = comboNum.ToString();
                    comboObject.transform.DOScale(new Vector3(1, 1, 1), 0.25f).SetEase(Ease.OutBounce);
                    comboObject.SetActive(true);

                    yield return new WaitForSeconds(0.25f);
                    comboObject.SetActive(false);
                    comboObject.transform.localScale = new Vector3(0, 0, 0);




            }
            else
            {
                comboNum = 0;
                ComboTime = 0.0f;
            }
        }
        else if (missState == true)
        {
            GetComponent<AudioSource>().clip = puzzleMiss;
            GetComponent<AudioSource>().Play();

            missObject.transform.position = new Vector3(hit.point.x, hit.point.y, 1);
            missObject.SetActive(true);
            missObject.transform.DOScale(new Vector3(1, 1, 1), 0.5f).SetEase(Ease.OutBounce);
          

            if (DataManage._srcGame[0].GetEnergy() <= 10)
            {
                DataManage._srcGame[0].SetEnergy(DataManage._srcGame[0].GetEnergy());
            }
            else
            {
                DataManage._srcGame[0].AddEnergy(-5);
            }

            if(DataManage._srcAct[0].GetEnergy() >= 4)
            {
                DataManage._srcAct[0].AddEnergy(-4);   //미스로 눌렀을시 4 감소
            }
            else
            {
                DataManage._srcAct[0].SetEnergy(0);
            }
            
            
         


            yield return new WaitForSeconds(0.5f);
            missObject.SetActive(false);
            missObject.transform.localScale = new Vector3(0, 0, 0);


            missState = false;
        }
        //else if(missState = true)
        //{
        //    missObject.SetActive(true);
        //}

        
    }
    void comboFunc(int inputCombonum)
    {
        if(DataManage._srcGame[0].GetEnergy() <= 200)
        {
            if (inputCombonum > 0 && inputCombonum <= 6)
            {
                if(DataManage._srcGame[0].GetEnergy() <= 197)
                {
                    DataManage._srcGame[0].AddEnergy(3);       
                }
                else
                {
                    DataManage._srcGame[0].SetEnergy(200);
                }
            }

            else if (inputCombonum > 6 && inputCombonum <= 12)
            {
                if (DataManage._srcGame[0].GetEnergy() <= 194)
                {
                    DataManage._srcGame[0].AddEnergy(6);
                }
                else
                {
                    DataManage._srcGame[0].SetEnergy(200);
                }
            }

            else if (inputCombonum > 12 && inputCombonum <= 18)
            {
                if (DataManage._srcGame[0].GetEnergy() <= 190)
                {
                    DataManage._srcGame[0].AddEnergy(10);
                }
                else
                {
                    DataManage._srcGame[0].SetEnergy(200);
                }
            }

            else if (inputCombonum > 18 /*&& inputCombonum <= 24 피버가 없어서 일단 최대콤보가 이어지도록 변경 16 08 13*/)
            {
                if (DataManage._srcGame[0].GetEnergy() <= 185)
                {
                    DataManage._srcGame[0].AddEnergy(15);
                }
                else
                {
                    DataManage._srcGame[0].SetEnergy(200);
                }
            }
            //else
           // {
                //fever에 관하여 코딩.. 해야하나 일단 15에너지씩 증가하도록 주석처리
           // }
        }
        
        // 최대 게이지? 200으로 제한
        else
        {
            DataManage._srcGame[0].SetEnergy(200);
        }
    }

    void ActGaugeUp()
    {

        if (GaugeTime >= 3.0f)
        {
            if (DataManage._srcAct[0].GetEnergy() >= 90)
            {
                DataManage._srcAct[0].SetEnergy(100);
            }
            else
            {
                DataManage._srcAct[0].AddEnergy(10);
            }
            GaugeTime = 0.0f;
        }

    }
    public IEnumerator DeleteBox() // 캐릭터를 Block에서 삭제한다.
    {
        boxIndex = 0;
        GetComponent<AudioSource>().clip = puzzlePop;
        GetComponent<AudioSource>().Play();
        StopCoroutine("ComboTimeCheck");
        currentTime = ComboTime;
        if (checkCount >= 2)
        {

            int temp = 0;// 의미없는값 이후 속성별 값으로 변경할 예정
            CheckElement(temp, checkCount); 

            if(comboNum == 0)
            {
                comboNum++;
                StartCoroutine("ComboTimeCheck"); //ComboTime 1초 2초.
                //tieme 0
            }
            else if (comboNum > 0)
            {
                comboNum++;
                StartCoroutine("ComboTimeCheck");
                comboState = true;
            }
            //else if(comboNum > 0)
            //{
            //    ComboTime = 0.0f;
            //    StartCoroutine("ComboTimeCheck");
            //    comboNum++;
            //}

            for (int i = 0; i < 25; ++i)
            {
                if (Puzzles[i].GetComponent<Box>().CheckState == true)
                {
                    boxIndex++;
                    switch(Puzzles[i].GetComponent<Box>().elementName)
                    {
                        case "Pink":
                            particle = Instantiate(DestroyParticlePink, Puzzles[i].transform.position, transform.rotation) as GameObject;
                            break;
                        case "Yellow":
                            particle = Instantiate(DestroyParticleYellow, Puzzles[i].transform.position, transform.rotation) as GameObject;
                            break;
                        case "Blue":
                            particle = Instantiate(DestroyParticleBlue, Puzzles[i].transform.position, transform.rotation) as GameObject;
                            break;
                        case "Red":
                            particle = Instantiate(DestroyParticleRed, Puzzles[i].transform.position, transform.rotation) as GameObject;
                            break;
                    }
                    Destroy(Puzzles[i].transform.GetChild(0).gameObject);
                    Puzzles[i].transform.SendMessage("CreateBox");  
                }
            }
        }
        else
        {
            missState = true;
            comboState = false;
            
            comboNum = 0;
            ComboTime = 0.0f;
            

            for (int i = 0; i < 25; ++i)
            {
                if (Puzzles[i].GetComponent<Box>().CheckState == true)
                {
                    Puzzles[i].GetComponent<Box>().CheckState = false;
                }

            }

        }

       
        StartCoroutine("showCombo");

        checkCount = 0;
        GameEntry.SendMessage("MainSlotSetting");
      
        while(boxIndex != 0)
        {
            yield return null;
        }

        suffleCheck();
    }

    //public IEnumerator ShowCombo(int ClickPuzzleNum)
    //{
        
    //    comboObject.SetActive(true);
    //    yield return new WaitForSeconds(0.5f);
    //    comboObject.SetActive(false);

    //}
    /*
    public void DeleteBox() // 캐릭터를 Block에서 삭제한다.
    {
        boxIndex = 0;
        if (checkCount >= 2)
        {
            for (int i = 0; i < 16; ++i)
            {
                if (Puzzles[i].GetComponent<Box>().CheckState == true)
                {
                    boxIndex++;
                    Destroy(Puzzles[i].transform.GetChild(0).gameObject, 0.2f);
                    Puzzles[i].transform.SendMessage("CreateBox");
                }
            }
        }
        else
        {
            for (int i = 0; i < 16; ++i)
            {
                if (Puzzles[i].GetComponent<Box>().CheckState == true)
                {
                    Puzzles[i].GetComponent<Box>().CheckState = false;
                }

            }

        }
        checkCount = 0;
        suffleCheck();
    }
     */

    public void suffleCheck()
    {
        SuffleState = false;
        for (int j = 0; j < 25; ++j)
        {
            Puzzles[j].GetComponent<Box>().suffleCheckState = true;

            CheckSuffleQueue.Enqueue(Puzzles[j].GetComponent<Box>().PuzzleValue);

            while (CheckSuffleQueue.Count != 0)
            {
                int num = CheckSuffleQueue.Dequeue();

                for (int i = 0; i < 4; ++i)
                {
                    if (CheckArray[num, i].GetHashCode() != -1)
                    {
                        //if (Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().CheckState == false)
                        if (Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().suffleCheckState == false)
                        {
                            if (Puzzles[num].GetComponent<Box>().elementName == Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().elementName)
                            {
                                //체크안되어있으면
                                CheckSuffleQueue.Enqueue(CheckArray[num, i].GetHashCode());
                                Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().suffleCheckState = true;
                                suffleCheckCount++;
                                

                            }
                        }
                    }
                }
            }
            //print(suffleCheckCount);
            if (suffleCheckCount >= 2)
            {
                SuffleState = true;
            }
            suffleCheckCount = 0;
        }
        
        if(SuffleState == false)
        {
           //Suffle();
            StartCoroutine("Suffle");
        }
        for(int i = 0; i < 25; ++i)
        {
         Puzzles[i].GetComponent<Box>().suffleCheckState = false;
        }
      
    }

    public void check(int puzzleNum)
    {
        Puzzles[puzzleNum].GetComponent<Box>().CheckState = true;

        CheckNumberQueue.Enqueue(Puzzles[puzzleNum].GetComponent<Box>().PuzzleValue);
        while (CheckNumberQueue.Count != 0)
        {
            int num = CheckNumberQueue.Dequeue();

            for (int i = 0; i < 4; ++i)
            {
                if (CheckArray[num, i].GetHashCode() != -1)
                {
                    //if (Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().CheckState == false)
                    if (Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().CheckState == false)
                    {
                        if (Puzzles[num].GetComponent<Box>().elementName == Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().elementName)
                        {
                            //체크안되어있으면
                            CheckNumberQueue.Enqueue(CheckArray[num, i].GetHashCode());
                            Puzzles[CheckArray[num, i].GetHashCode()].GetComponent<Box>().CheckState = true;
                            checkCount++;
                        }
                    }

                }

            }
        }
        print("count : " + checkCount);
    }

    void SoundControl()
    {
        if (Setting.soundOX)
        {
            GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            GetComponent<AudioSource>().volume = 0;
        }
    }


    void btnRetry()
    {

        for (int i = 0; i < Unit._lst_Ally.Count; ++i)
        {
            Unit._lst_Ally[i].SendMessage("Ending");
        }
        for (int i = 0; i < Unit._lst_Enemy.Count; ++i)
        {
            Unit._lst_Enemy[i].SendMessage("Ending");
            print(1);
        }
        Unit._lst_Ally.Clear();
        Unit._lst_Enemy.Clear();

       
        LobbyManager.isLoad = false;
        BattleScroll.isopening = true;
        BattleScroll.isending = false;
        God.playOnce = true;
        for (int i = 0; i < 3; ++i)
        {
            ResultStars[i].transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        }
        //Application.LoadLevel("Stage");
        Application.LoadLevel("Stage");
    }

    void btnHome()
    {

        for (int i = 0; i < Unit._lst_Ally.Count; ++i)
        {
            Unit._lst_Ally[i].SendMessage("Ending");
        }
        for (int i = 0; i < Unit._lst_Enemy.Count; ++i)
        {
            Unit._lst_Enemy[i].SendMessage("Ending");
            print(2);
        }

        Unit._lst_Ally.Clear();
        Unit._lst_Enemy.Clear();
        God.playOnce = true;
        BattleScroll.isopening = true;
        BattleScroll.isending = false;
        for (int i = 0; i < 3; ++i)
        {
            ResultStars[i].transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
        }
        //Application.LoadLevel("Lobby");
        Application.LoadLevel("Lobby");
    }
}
