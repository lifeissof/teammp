﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GameEntry : MonoBehaviour
{

    public GameObject[] mainEntrySlot = new GameObject[5];
    public GameObject[] entrySlot = new GameObject[5];
    public GameObject[] entryLock = new GameObject[2];
    public tk2dSprite[] SkillSprite = new tk2dSprite[5];

    public tk2dSpriteAnimation[] entryLib = new tk2dSpriteAnimation[15];
    


   
    public tk2dTextMesh[] entryNeedEnergy = new tk2dTextMesh[5];
    public tk2dTextMesh[] skillEntryNeedEnergy = new tk2dTextMesh[5];

    Ray ray;
    RaycastHit hit;


    void Start()
    {
        for (int i = 0; i < DataManage.SkillSlotNum; ++i)
        {
            skillEntryNeedEnergy[i].text = DataManage._skill[i].needSkillSource[0].ToString();
        }

        MainSlotSetting();
        EntrySetting();
    }


    void OnEnable()
    {
        MainSlotSetting();
        EntrySetting();
    }


    void MainSlotSetting()
    {

        for (int i = 0; i < DataManage.entrySlotNum; ++i)
        {
            string monsterName = DataManage._entrySlot[i].monsterName;
            tk2dSpriteAnimator monsterAnim = mainEntrySlot[i].GetComponent<tk2dSpriteAnimator>();
            entryNeedEnergy[i].text = DataManage._entrySlot[i].needGameSource[0].ToString();            //몬스터 소환시 필요한 에너지
            
            //EntryPosition[i] = (DataManage._entrySlot[i].needGameSource[0] * 7.2f);
            //DataManage._entrySlot[i].needGameSource[0]
            mainEntrySlot[i].name = monsterName;
            
            if (monsterName != "None")
            {
           
                if (DataManage._entrySlot[i].CheckGameSource(DataManage._srcGame) == true)
                {
                    monsterAnim.enabled = true;
                    monsterAnim.Library = entryLib[System.Convert.ToInt32(DataManage._entrySlot[i].monsterNumber)];
                    monsterAnim.Play(string.Format("Character{0}", DataManage._entrySlot[i].monsterNumber));
                }
                else
                {
                    monsterAnim.enabled = true;
                    monsterAnim.Library = entryLib[System.Convert.ToInt32(DataManage._entrySlot[i].monsterNumber)];
                    tk2dSprite sprite = mainEntrySlot[i].GetComponent<tk2dSprite>();
                    monsterAnim.Play(string.Format("Character{0}", DataManage._entrySlot[i].monsterNumber));
                    monsterAnim.enabled = false;
                    int ID_x = sprite.GetSpriteIdByName(string.Format("character{0}_g", DataManage._entrySlot[i].monsterNumber));
                    sprite.spriteId = ID_x;
                }
            }
            else
            {
                mainEntrySlot[i].transform.position = new Vector3(450, 210, 1);
                monsterAnim.enabled = true;
                tk2dSprite sprite = mainEntrySlot[i].GetComponent<tk2dSprite>();
                monsterAnim.Library = entryLib[0];
                monsterAnim.Play("Character00");
                monsterAnim.enabled = false;
                int ID_x = sprite.GetSpriteIdByName("character00_h");
                sprite.spriteId = ID_x;

            }
        }


        //for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
        //{
        //    for (int j = 0; j < DataManage.SkillSlotNum; ++i)
        //    {
        //        if (DataManage._srcGame[i].GetEnergy() > DataManage._skill[j].needSkillSource[i])
        //        {
        //            SkillSprite[j].DOFade(1.0f, 1.0f);
        //        }              
                
        //    }
        //}

      

        

        //친구 몬스터에 대한 코드 나중에 추가.
    }
    void EntrySetting()
    {
        switch (DataManage.entrySlotNum)
        {
            case 3:
                {
                    entrySlot[3].SetActive(false);
                    entrySlot[4].SetActive(false);
                    //entrySlot[5].SetActive(false);
                    entryLock[0].SetActive(true);
                    entryLock[1].SetActive(true);
                    break;
                }
            case 4:
                {
                    entrySlot[3].SetActive(true);
                    entrySlot[4].SetActive(false);
                    //entrySlot[5].SetActive(false);
                    entryLock[0].SetActive(false);
                    entryLock[1].SetActive(true);
                    break;
                }
            case 5:
                {
                    entrySlot[3].SetActive(true);
                    entrySlot[4].SetActive(true);
                    //entrySlot[5].SetActive(false);
                    entryLock[0].SetActive(false);
                    entryLock[1].SetActive(false);
                    break;
                }
            default:
                {
                    entrySlot[3].SetActive(false);
                    entrySlot[4].SetActive(false);
                    //entrySlot[5].SetActive(false);
                    entryLock[0].SetActive(true);
                    entryLock[1].SetActive(true);
                    break;
                }
        }
    }


 
}