﻿using UnityEngine;
using System.Collections;

public class SlidingPattern : MonoBehaviour
{

    private bool isSlidingCheck = false;
    private string UserPatten;

    private Vector2 nowPosition;
    private Vector2 currPosition;
    private Vector3 hitPosition;

    Ray ray;
    RaycastHit hit;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //화면의 4/10 위 또는 7/10 아래 를 누르면..
            if (Application.loadedLevelName == "Lobby" && Input.mousePosition.y < Screen.height * 0.7f && Input.mousePosition.y > Screen.height * 0.4f)
            {
                isSlidingCheck = true;
            }

            //스테이지 화면일때의 기능구현.
            if (Application.loadedLevelName == "Stage" && Input.mousePosition.y < Screen.height * 0.5f && Input.mousePosition.y > Screen.height * 0.2f)
            {
                isSlidingCheck = true;
            }
            //
            if (Application.loadedLevelName == "NewHive" && Input.mousePosition.y < Screen.height * 0.6f && Input.mousePosition.y > Screen.height * 0.1f)
            {
                isSlidingCheck = true;
            }

            currPosition = Input.mousePosition;
            nowPosition = Input.mousePosition;
        }

        else if (Input.GetMouseButton(0) && isSlidingCheck)
        {

            //입력이 왼쪽 마우스이고  //화면의 4/10 위 또는 7/10 아래 를 눌렀다면..

            Vector2 ResultPosition = Vector2.zero;
            nowPosition = Input.mousePosition;

            ResultPosition.x = (nowPosition.x - currPosition.x);
            ResultPosition.y = (nowPosition.y - currPosition.y);


            if (ResultPosition.x >= 20 && (ResultPosition.y < 20 && ResultPosition.y > -20))
            {
                UserPatten = "R";
            }
            else if (ResultPosition.x <= -20 && (ResultPosition.y < 20 && ResultPosition.y > -20))
            {
                UserPatten = "L";
            }
            currPosition = Input.mousePosition;
        }

        else if (Input.GetMouseButtonUp(0) && isSlidingCheck)
        {
            switch (UserPatten)
            {
                case "R":
                    if ((Application.loadedLevelName == "Lobby") || (Application.loadedLevelName == "NewHive")) { SendMessage("btnTouchRight"); }
                    else
                        SendMessage("btnTouchRight");     
                    
                    isSlidingCheck = false;
                    break;


                case "L":
                    if ((Application.loadedLevelName == "Lobby") || (Application.loadedLevelName == "NewHive")) { SendMessage("btnTouchLeft"); }
                    else
                        SendMessage("btnTouchLeft");

                    isSlidingCheck = false;
                    break;

                case null:
                    isSlidingCheck = false;
                    break;

            }
            UserPatten = null;
        }
    }
}
