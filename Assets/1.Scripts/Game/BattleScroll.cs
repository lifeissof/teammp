﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BattleScroll : MonoBehaviour {

    public GameObject battle;
    public GameObject spaceBattleship;
    public GameObject pincers;
    public tk2dSpriteAnimator pincersArm;
    public GameObject godBall;

    private Vector2 nowPosition;
    private Vector2 firstPosition;
    private Vector3 swipePosition;

    public List<GameObject> BackGround = new List<GameObject>();

    public GameObject SoundObject;
    public GameObject SpaceShipSoundObject;
    public GameObject ForGameSoundObject;
    public AudioClip spaceship0_landing;

    //public GameObject ObjectForBgm;
    public AudioClip bgm;
    public AudioClip gameover;
    //public AudioClip warningSound;
    public AudioClip bgmLost;
    public AudioClip bgmWin;


   
    public int FireMaterialDropNum;
    public int WaterMaterialDropNum;
    public int PlantMaterialDropNum;

    public int NLMaterialDropNum;
    public int RLMaterialDropNum;
    public int GoldDropNum;



    public GameObject ResultBox;
    public GameObject Congratulations;
    public GameObject PleaseInspired;
    public Result result = new Result();


    private bool isSlidingCheck = false;



    //public GameObject friendRequestBoard;


    public static bool isopening = true;
    public static bool isending = false;

    float dist;
    int playTime;


    tk2dUIScrollableArea area;
    bool autoscroll;            //아군 몬스터가 움직이면 자동으로 스크롤을 해야하므로 사용하는 상태변수
    bool isPlayTime;
    int scrollSpeed;
    int prevScrollSpeed;

	// Use this for initialization
	void Start () {

        PlanetMonster.AllyDie += ScrollMove;
        playTime = 0;
        scrollSpeed = 0;
        prevScrollSpeed = -1;
        isPlayTime = false;

        SoundObject.GetComponent<AudioSource>().clip = bgm;
        SoundObject.GetComponent<AudioSource>().loop = true;
        SoundObject.GetComponent<AudioSource>().Play();


        FireMaterialDropNum = 0;
        WaterMaterialDropNum = 0;
        PlantMaterialDropNum = 0;
        NLMaterialDropNum = 0;
        RLMaterialDropNum = 0;


        for (int i = 0; i < BackGround.Count; ++i)
        {
            if (i == StageManager.selectZone - 1)
                BackGround[i].SetActive(true);
            else
                BackGround[i].SetActive(false);
        }

        //ResultBox = GameObject.FindGameObjectWithTag("Result");
        //Congratulations = GameObject.FindGameObjectWithTag("Congratulations");
        //PleaseInspired = GameObject.FindGameObjectWithTag("PleaseInspired");

        area = battle.GetComponent<tk2dUIScrollableArea>();
        pincers.transform.position = new Vector3(1350 , 650, 0);

        //ResultBox.SetActive(false);
        StartCoroutine(Opening());
        autoscroll = false;

	}
	
	// Update is called once per frame
	void Update () {

        SoundControl();

        if (isopening == false && isending == false && (Camera.main.ScreenToWorldPoint(Input.mousePosition).y > 350))
        {
            if (Input.GetMouseButtonDown(0))
            {

                isSlidingCheck = true;
                autoscroll = false;
                if (autoscroll) DOTween.Pause("autoScroll");
                firstPosition = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0) && isSlidingCheck && firstPosition.x > 220)
            {
                print(firstPosition);
                nowPosition = Input.mousePosition;
                swipePosition = firstPosition - nowPosition;
                //battle.transform.position += Vector3.left * (swipePosition.x / 20);// = //new Vector3(battle.transform.position.x - (swipePosition.x / 20), 0, 0);
                //battle.transform.position = new Vector3(battle.transform.position.x - (swipePosition.x / 20), 0, 0);
                battle.transform.GetChild(0).position = new Vector3(battle.transform.GetChild(0).position.x - (swipePosition.x / 20), 0, 0);
                if (battle.transform.GetChild(0).position.x > 0 || battle.transform.GetChild(0).position.x < -850)
                {
                    //area.enabled = false;
                    battle.transform.position = new Vector3(0, 0, 0);
                    isSlidingCheck = false;
                }
                
            }

            else if (Input.GetMouseButton(0) && isSlidingCheck && firstPosition.x < 120)
            {
                print(firstPosition);
                nowPosition = Input.mousePosition;
                swipePosition = nowPosition -  firstPosition ;
                //battle.transform.position += Vector3.left * (swipePosition.x / 20);// = //new Vector3(battle.transform.position.x - (swipePosition.x / 20), 0, 0);
                //battle.transform.position = new Vector3(battle.transform.position.x + (swipePosition.x / 20), 0, 0);
                battle.transform.GetChild(0).position = new Vector3(battle.transform.GetChild(0).position.x + (swipePosition.x / 20), 0, 0);


                if (battle.transform.GetChild(0).position.x > 0 || battle.transform.GetChild(0).position.x < -850)
                {
                    //area.enabled = false;
                    battle.transform.position = new Vector3(0, 0, 0);
                    isSlidingCheck = false;
                }
            }
            if (Input.GetMouseButtonUp(0))  //마우스 버튼을 떼면 자동스크롤링 시작
            {
                battle.transform.position = new Vector3(0, 0, 0);
               // area.enabled = true;
                print("autoscroll");
                StopCoroutine("AutoScrollTimer");
                StartCoroutine("AutoScrollTimer");

            }
        }
        if (autoscroll)
        {

            AutoScrolling();
        }
	}

  
    IEnumerator Opening()
    {
        spaceBattleship.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        battle.transform.DOMoveX(-820, 1, false).OnComplete(spaceBattleshiplanding).SetTarget(this.gameObject);

        yield return new WaitForSeconds(5f);

        battle.transform.DOMoveX(0, 1, false);
        yield return new WaitForSeconds(1f);

        isopening = false;
        area.enabled = true;
        autoscroll = true;
        
    }
    void AutoScrolling()
    {
        if (isPlayTime == false)
        {
            StartCoroutine("TimeCheck");
            isPlayTime = true;
        }
        float front = -100000;

        for (int i = 0; i < Unit._lst_Ally.Count; ++i)
        {

            if (Unit._lst_Ally[i].transform.position.x - battle.transform.GetChild(0).position.x > front)
            {
                if (Unit._lst_Ally[i].gameObject.name != "God")
                {
                    front = Unit._lst_Ally[i].transform.position.x - battle.transform.GetChild(0).position.x;

                    if (Unit._lst_Ally[i].gameObject.name != "God" && scrollSpeed != prevScrollSpeed)
                    {
                        scrollSpeed = Unit._lst_Ally[i].GetComponent<PlanetMonster>().state_L.speed;
                        prevScrollSpeed = scrollSpeed;
                    }
                }
            }
        }

        if (front > 0 && front < 840  && isopening == false && isending == false)
        {
            if (battle.transform.GetChild(0).gameObject.transform.position.x > -front) 
                battle.transform.GetChild(0).gameObject.transform.position -= Vector3.right.normalized * Time.deltaTime * scrollSpeed;
            //battle.transform.GetChild(0).gameObject.transform.position = new Vector3(-front, battle.transform.GetChild(0).gameObject.transform.position.y, battle.transform.GetChild(0).gameObject.transform.position.z);
        }
        else if (front <= 0 && isopening == false && isending == false)
        {
            battle.transform.GetChild(0).gameObject.transform.DOMoveX(0, 0.3f, false).SetEase(Ease.OutSine);
           
        }
        else if (front >= 840 && isopening == false && isending == false)
        {
            battle.transform.GetChild(0).gameObject.transform.DOMoveX(-840, 0.3f, false).SetEase(Ease.OutSine);
          
        }
    }

    IEnumerator AutoScrollTimer()
    {
        yield return new WaitForSeconds(2f);

        float front = -100000;

        for (int i = 0; i < Unit._lst_Ally.Count; ++i)
        {
            if (Unit._lst_Ally[i].transform.position.x - battle.transform.GetChild(0).position.x > front)
            {
                front = Unit._lst_Ally[i].transform.position.x - battle.transform.GetChild(0).position.x;
            }
        }
       

        if (front > 0 && front < 840 && isopening == false && isending == false)
        {
            battle.transform.GetChild(0).gameObject.transform.DOMoveX(-front, 0.3f, false).SetEase(Ease.OutSine).SetId("autoScroll");
      
        }
        else if (front <= 0 && isopening == false && isending == false)
        {
            battle.transform.GetChild(0).gameObject.transform.DOMoveX(0, 0.3f, false).SetEase(Ease.OutSine).SetId("autoScroll");
        }
        else if (front >= 840 && isopening == false && isending == false)
        {
            battle.transform.GetChild(0).gameObject.transform.DOMoveX(-840, 0.3f, false).SetEase(Ease.OutSine).SetId("autoScroll");
        }

        autoscroll = true;
    }

    public void ScrollMove()
    {
        if (Unit._lst_Ally.Count > 1)
        {
            autoscroll = false;
            float front = Unit._lst_Ally[0].transform.localPosition.x;

            for (int i = 0; i < Unit._lst_Ally.Count; ++i)
            {
                if (Unit._lst_Ally[i].transform.position.x > front)
                {
                    front = Unit._lst_Ally[i].transform.localPosition.x;
                }
            }
            //동료가 우주선 근처일 때는 -front가 적당
            // 갓 근처일 때는 front가 적당.
            if (front > 0)
            {
                if (front > 840) front = 840;
                front *= -1;
            }

            battle.transform.GetChild(0).gameObject.transform.DOMoveX(front, 1, false);
            autoscroll = true;
        }
    }

    public IEnumerator Ending()
    {
        StopCoroutine("TimeCheck");

        //if (battle.transform.GetChild(0).gameObject.transform.position.x < 0)
        //{
            battle.transform.GetChild(0).gameObject.transform.DOMoveX(0, 1.0f, false).SetEase(Ease.OutSine);
            yield return new WaitForSeconds(0.5f);
        //}

        ForGameSoundObject.GetComponent<AudioSource>().clip = gameover;
        ForGameSoundObject.GetComponent<AudioSource>().Play();
        

        for (int i = 0; i < Unit._lst_Enemy.Count; ++i)
        {
            Unit._lst_Enemy[i].SendMessage("Ending");
        }
        for (int i = 0; i < Unit._lst_Ally.Count; ++i)
        {
            Unit._lst_Ally[i].SendMessage("Ending");
        }

        Unit._lst_Ally.Clear();
        Unit._lst_Enemy.Clear();

        ChangeEnemyValue();

        area.enabled = false;
        isending = true;
        autoscroll = false;

        pincers.SetActive(true);
        pincers.transform.DOMoveX(-400, 1, false).SetEase(Ease.OutSine);
        yield return new WaitForSeconds(1.5f);
        pincersArm.gameObject.transform.DOMoveY(500, 0.5f, false).OnComplete(PrincersArm).SetTarget(this.gameObject);
        yield return new WaitForSeconds(1.8f);
        pincersArm.gameObject.transform.DOMoveY(600, 0.3f);
        yield return new WaitForSeconds(0.6f);
        pincers.transform.DOMoveX(-1000, 1, false).OnComplete(ShowResultLose).SetTarget(this.gameObject);

        isopening = true;
        isending = false;

        //이후 서버에서 업데이트 하는 함수 호출
        
        
    }

    void PrincersArm()
    {
        pincersArm.Play("pincers");
        godBall.transform.parent = pincersArm.gameObject.transform;
    }

    void ShowResultLose()
    {

        if (Setting.bgmOX)
        {
            SoundObject.GetComponent<AudioSource>().clip = bgmLost;
            SoundObject.GetComponent<AudioSource>().loop = false;
            SoundObject.GetComponent<AudioSource>().Play();
        }

        ResultBox.SetActive(true);
        Congratulations.SetActive(false);
        PleaseInspired.SetActive(true);

        PlanetMonster.AllyDie -= ScrollMove;

        result.Init(false);
    }

    public void ChangeEnemyValue()
    {
        print("적들의 초기 공격력으로 초기화");
        for (int i = 0; i < 3; ++i)
        {
            ServerInterface.Instance.enemyState_L[i].attackGround = StageManager.InitEnemy[i,0];
            ServerInterface.Instance.enemyState_L[i].attackAir = StageManager.InitEnemy[i,1];
            ServerInterface.Instance.enemyState_L[i].MaxHp = StageManager.InitEnemy[i,2];
        }
    }

    public void ShowResultWin()
    {


        if (Setting.bgmOX)
        {
            SoundObject.GetComponent<AudioSource>().clip = bgmWin;
            SoundObject.GetComponent<AudioSource>().loop = false;
            SoundObject.GetComponent<AudioSource>().Play();
        }


        ResultBox.SetActive(true);
        Congratulations.SetActive(true);
        PleaseInspired.SetActive(false);
        PlanetMonster.AllyDie -= ScrollMove;

        result.Init(true);
    }


    void spaceBattleshiplanding()
    {
        spaceBattleship.transform.DOMove(new Vector2(spaceBattleship.transform.position.x - 400, spaceBattleship.transform.position.x - 200), 1, false).OnComplete(spaceBattleshipOpen).SetTarget(this.gameObject);
    }

    void spaceBattleshipOpen()
    {
        SpaceShipSoundObject.GetComponent<AudioSource>().clip = spaceship0_landing;
        SpaceShipSoundObject.GetComponent<AudioSource>().Play();
        tk2dSpriteAnimator animator = spaceBattleship.transform.FindChild("door").GetComponent<tk2dSpriteAnimator>();
        animator.Play(spaceBattleship.name + "_door");
    }

    void SoundControl()
    {
        if (Setting.bgmOX)
        {
            SoundObject.GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            SoundObject.GetComponent<AudioSource>().volume = 0;
        }

        if (Setting.soundOX)
        {
            SpaceShipSoundObject.GetComponent<AudioSource>().volume = 1;
            GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            SpaceShipSoundObject.GetComponent<AudioSource>().volume = 0;
            GetComponent<AudioSource>().volume = 0;
        }
    }


    public IEnumerator TimeCheck()
    {
        yield return new WaitForSeconds(1.0f);
        playTime++;
        StartCoroutine("TimeCheck");
    }

    public void StopTimeCheck()
    {
        StopCoroutine("TimeCheck");
    }

    public int GetTime()
    {
        return playTime;
    }

    public void ReleaseEvent()
    {
        PlanetMonster.AllyDie -= ScrollMove;
    }




    
}
