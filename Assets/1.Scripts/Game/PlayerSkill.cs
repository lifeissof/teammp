﻿using UnityEngine;
using System.Collections;

public class PlayerSkill : MonoBehaviour {

    float SkillTime = 0.0f;
    float SkillCoolTime = 10.0f;
    bool AllmightyState = false;
    bool StimPackState = false;
    bool TimeStopState = false;
    bool onceState = false;

    float Speed = 15.0f;

    public GameObject[] Thunder = new GameObject[5];
    public GameObject stimpack;
    public GameObject health;
    public GameObject time;
    int effectCharacterNum = 0;
    int[] currSpeed;
    int[] currMoveSpeed;

    

    void Start()
    {

    }

    void Update()
    {
        StimPackOperation();
        ExtremeOperation();
        TimeStopOperation();

        

        

    }

    IEnumerator TimeCheck()
    {
        SkillTime = 0.0f;
        while (true)
        {
            yield return new WaitForSeconds(1.0f);
            SkillTime += 1.0f;
        }
    }

    IEnumerator ThunderActive()
    {
        for (int k = 0; k < 5; k++)
        {
            Thunder[k].SetActive(true);
            //Thunder[k].transform.Translate(-15, 0, 0);
                
            //Thunder[k].transform.Translate(15, 0, 0);
        }
        yield return new WaitForSeconds(0.1f);
        for (int l = 0; l < 5; l++)
        {
            Thunder[l].SetActive(false);
        }
        
    }
    IEnumerator ThunderStorm()
    {
        int SkillDamageTime = 10; //10번동안 지속 데미지를 준다.
                                  //효과음 추가
        
        for (int i = 0; i < SkillDamageTime; ++i)
                {
            StartCoroutine("ThunderActive");
            for (int j = 1; j < Unit._lst_Enemy.Count; j++)
                    {
                
                try
                        {
                    
                    Unit._lst_Enemy[j].GetComponent<Enemy>().state_L.Hp -= 20;
                    
                    print("체력감소");
                        }
                        catch {}

                        yield return new WaitForSeconds(0.025f);
                        
                
                    }
            
        }
        
    }

    void StimPack()
    {
        //10초동안 효과가 있게
        //효과음 추가
        StimPackState = true;
        
        
        StartCoroutine("TimeCheck");
        
        
    }

    void AllMighty()
    {
        AllmightyState = true;
        StartCoroutine("TimeCheck");
        
    }

    void TimeStop()
    {
        TimeStopState = true;
        StartCoroutine("TimeCheck");
        
    }

    void DoubleProduct()
    {

    }

    void StimPackOperation()
    {
        if (StimPackState)
        {
            stimpack.SetActive(true);
           // print("화살표 on");
            if (onceState == false)
            {
                for (int j = 1; j < Unit._lst_Ally.Count; j++)
                {
                    
                    Unit._lst_Ally[j].GetComponent<PlanetMonster>().state_L.speed *= 2;     //2배상승
                    Unit._lst_Ally[j].GetComponent<PlanetMonster>().state_L.movingTime *= 2;
                    print("스팀팩 상태");

                }
                onceState = true;
            }

            stimpack.transform.Translate(Speed, 0, 0);

            if (SkillTime > 5.0f)
            {

                StopCoroutine("TimeCheck");
                for (int j = 1; j < Unit._lst_Ally.Count; j++)
                {
                    Unit._lst_Ally[j].GetComponent<PlanetMonster>().state_L.speed /= 2;     //2배상승 끝
                    Unit._lst_Ally[j].GetComponent<PlanetMonster>().state_L.movingTime /= 2;
                    print("스팀팩 상태 해제");
                    
                    print("stim!!!!!!!!!!!");
                }
                print("화살표 off");
                stimpack.transform.position = new Vector3(-535, 515, 0);
                stimpack.SetActive(false);
                StimPackState = false;
                onceState = false;
                SkillTime = 0.0f;
            }
            

        }

    }

    void ExtremeOperation()
    {
        if (AllmightyState)
        {
            health.SetActive(true);
            if (onceState == false)
            {
                for (int j = 1; j < Unit._lst_Ally.Count; j++)
                {

                    Unit._lst_Ally[j].GetComponent<PlanetMonster>().state_L.Hp *= 2;     //2배상승
                    print("HP 2배 상태");
                }
                onceState = true;
            }

            health.transform.Translate(0, Speed, 0);
            
            if (SkillTime > 5.0f)
            {

                StopCoroutine("TimeCheck");
                for (int j = 1; j < Unit._lst_Ally.Count; j++)
                {
                    Unit._lst_Ally[j].GetComponent<PlanetMonster>().state_L.Hp /= 2;
                    print("2배 해제");
                }
                health.transform.position = new Vector3(0, 150, 0);
                health.SetActive(false);
                AllmightyState = false;
                onceState = false;
                SkillTime = 0.0f;
            }
        }
    }

    void TimeStopOperation()
    {
        if (TimeStopState)
        {
            time.SetActive(true);
            if (onceState == false)
            {
                for (int j = 1; j < Unit._lst_Enemy.Count; j++)
                {

                    Unit._lst_Enemy[j].GetComponent<Enemy>().state_L.attackGroundDelay *= 2;    //2배상승
                    print("타임스탑");
                }
                onceState = true;
            }

            time.transform.Translate(0, -Speed, 0);
            if (time.transform.position.y < 150)
            {
                time.SetActive(false);
            }

                if (SkillTime > 5.0f)
            {

                StopCoroutine("TimeCheck");
                for (int j = 1; j < Unit._lst_Enemy.Count; j++)
                {

                    Unit._lst_Enemy[j].GetComponent<Enemy>().state_L.attackGroundDelay /= 2;    //2배 끝
                    print("타임스탑 해제");
                }
                time.transform.position = new Vector3(0, 762, 0);
                TimeStopState = false;
                onceState = false;
                SkillTime = 0.0f;
            }
        }
    }

}
