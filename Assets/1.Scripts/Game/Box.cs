﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Box : MonoBehaviour {


    public List<GameObject> Item = new List<GameObject>(10);
    //public int[] CheckArrary = new int[4];      //인접한 퍼즐을 찾기위한 배열 상 하 좌 우 순서이며 상하좌우중에 인접한 퍼즐이 없을시 -1로 처리하였다.
    public int PuzzleValue;
    public bool CheckState = false;
    public bool suffleCheckState = false;
    public string elementName;


    Box target;
  

     Ray ray;
    RaycastHit hit;
    	

    public void CreateBox()
    {
        StartCoroutine("Create");        
    }
    IEnumerator Create()    
    {
        if (this.transform.childCount > 0)
        {
            Destroy(this.transform.GetChild(0).gameObject, 1.0f);
        }
        while (this.transform.childCount > 0)
        {
            yield return true;
        }

        if (this.transform.childCount == 0)
        {
            int rand;
            GameObject temp;
            rand = Random.Range(0, 4);
            temp = Instantiate(Item[rand], new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 1), transform.rotation) as GameObject;
            temp.transform.parent = this.transform;
            elementName = temp.tag;

            temp.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            temp.transform.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 0.5f);
            CheckState = false;
        }
        if (this.transform.childCount == 0)
        {          
            StartCoroutine("Create");
        }
        Puzzle.boxIndex--;
    }
}


