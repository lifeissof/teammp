﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public class Battle : MonoBehaviour {


    public Vector3 monsterStandardPoint;        //몬스터의 소환 시작 포인트
    public Vector3 enemyStandardPoint = new Vector3(); //적몬스터의 소환 시작 포인트
    public bool CreateEnemyState = false;

    public PlayerSkill _PlayerSkill = new PlayerSkill();
   
    bool CreateFlag = false;

    public AudioClip createMonsterAudio;
    public GameObject[] monster = new GameObject[15];
    public GameObject[] enemy = new GameObject[15];
    float SpawnTime = 0.0f;
    //public tk2dSlicedSprite MonsterGauge;

    Ray ray;
    RaycastHit hit;
    public GameObject GameEntry;




    // Use this for initialization
    void Start()
    {
        
        StartCoroutine("spawnEnemy");
        StartCoroutine("spawnMonster");
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "SkillBtn" && BattleScroll.isopening == false && BattleScroll.isending == false)
            {
                //CreateMonsterSound = false;
                //if (CreateMonsterSound == false)
                //{
                //    GameEntry.GetComponent<AudioSource>().clip = createMonsterAudio;
                //    GameEntry.GetComponent<AudioSource>().Play();
                //    GameEntry.GetComponent<AudioSource>().loop = false;
                //    CreateMonsterSound = true;
                //}

                if (hit.collider.name != "None")
                {
                    UseSkill(System.Convert.ToInt32(hit.collider.name.Substring(9, 1)));
                }
            }


            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "CreateBtn" && BattleScroll.isopening == false && BattleScroll.isending == false)
            {
                if (hit.collider.name != "None")
                {
                    CreateMonster(hit.collider.name);
                }
            }

            //재료가 있을시 캐릭터 선택.
        }

    }



    void UseSkill(int skillNumber)
    {
        switch (skillNumber)
        {
            case 0:
                for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
                {
                    if(DataManage._srcGame[i].GetEnergy() >= DataManage._skill[skillNumber].needSkillSource[i])
                    {
                        DataManage._srcGame[i].AddEnergy(-DataManage._skill[skillNumber].needSkillSource[i]);
                        _PlayerSkill.StartCoroutine("ThunderStorm");
                        
                    }
                }
                
                break;

            case 1:
                for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
                {
                    if (DataManage._srcGame[i].GetEnergy() >= DataManage._skill[skillNumber].needSkillSource[i])
                    {
                        DataManage._srcGame[i].AddEnergy(-DataManage._skill[skillNumber].needSkillSource[i]);
                        _PlayerSkill.SendMessage("StimPack");
                    }
                        
                }
                
                break;

            case 2:
                for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
                {
                    if (DataManage._srcGame[i].GetEnergy() >= DataManage._skill[skillNumber].needSkillSource[i])
                    {
                        DataManage._srcGame[i].AddEnergy(-DataManage._skill[skillNumber].needSkillSource[i]);
                        _PlayerSkill.SendMessage("AllMighty");
                    }
                }
                break;

            case 3:
                for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
                {
                    if (DataManage._srcGame[i].GetEnergy() >= DataManage._skill[skillNumber].needSkillSource[i])
                    {
                        DataManage._srcGame[i].AddEnergy(-DataManage._skill[skillNumber].needSkillSource[i]);
                        _PlayerSkill.SendMessage("TimeStop");
                    }
                }
                break;

            case 4:
                for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
                {
                    if (DataManage._srcGame[i].GetEnergy() >= DataManage._skill[skillNumber].needSkillSource[i])
                    {
                        DataManage._srcGame[i].AddEnergy(-DataManage._skill[skillNumber].needSkillSource[i]);
                        _PlayerSkill.SendMessage("DoubleProduct");
                    }
                        
                }
                
                break;
        }

        //bool checkOK = false;
        //int num = -1;
        //for (int i = 0; i < DataManager._monster.Length; ++i)
        //{
        //    if (DataManager._monster[i].monsterName == name)
        //    {
        //        num = i;
        //        break;
        //    }
        //}
        //checkOK = DataManager._monster[num].CheckGameSource(DataManager._srcGame);
        //if (checkOK == true)
        //{
        //    GetComponent<AudioSource>().clip = createMonster;
        //    GetComponent<AudioSource>().Play();

        //    for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
        //        DataManager._srcGame[i].AddEnergyBig(-DataManager._monster[num].needGameSource[i]);

        //    int randPosY = Random.Range(-10, 11);

        //    float distance = CheckEnemyDistance();

        //    Vector3 tempVector;
        //    if (distance > monster[num].GetComponent<Ally>().UnitSize) //monster[num].transform.position.x 0인 기준으로 적이 멀리 있는 경우
        //    {
        //        tempVector = new Vector3(0, (randPosY + ServerInterface.Instance.monsterState_P[num].posY) * NScreen.ScaleNum, monster[num].transform.position.z);
        //    }
        //    else
        //    {
        //        tempVector = new Vector3(monster[num].transform.position.x * NScreen.ScaleNum, (ServerInterface.Instance.monsterState_P[num].posY - 28) * NScreen.ScaleNum, monster[num].transform.position.z);
        //    }

        //    GameObject temp = Instantiate(monster[num], monsterStandardPoint * NScreen.ScaleNum + this.gameObject.transform.GetChild(0).transform.position + tempVector, monster[num].transform.rotation) as GameObject;
        //    temp.transform.parent = this.gameObject.transform.GetChild(0);
        //    temp.transform.localScale = temp.transform.localScale * NScreen.ScaleNum;
        //    temp.GetComponent<tk2dSprite>().SortingOrder = temp.GetComponent<tk2dSprite>().SortingOrder - (randPosY - 10);
        //    temp.transform.GetChild(0).GetComponent<tk2dSprite>().SortingOrder = temp.GetComponent<tk2dSprite>().SortingOrder;
        //    GameEntry.SendMessage("MainSlotSetting");
        //    GameEntry.SendMessage("SlotSetting");
        //    if ((Tutorial.stepNumber == 0 || Tutorial.stepNumber == 2) && countForTutorial < 1) OnSpawnMonster();
        //}
    }
    void CreateMonster(string name)
    {
        bool checkOK = false;
        int num = -1;

        for (int i = 0; i < DataManage._monster.Length; ++i)
        {
            if (DataManage._monster[i].monsterName.Equals(name))
            {
                num = i;
                break;
            }
        }

        checkOK = DataManage._monster[num].CheckGameSource(DataManage._srcGame);
        if (checkOK == true)
        {
            GameEntry.GetComponent<AudioSource>().clip = createMonsterAudio;
            GameEntry.GetComponent<AudioSource>().Play();
            GameEntry.GetComponent<AudioSource>().loop = false;
            for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
            {
                DataManage._srcGame[i].AddEnergy(-DataManage._monster[num].needGameSource[i]);
            }
           
            int randPosY = Random.Range(-10, 11);
           
            if(num == 13)
            {
                monsterStandardPoint = new Vector3(-200, 200, 0);
            }
            GameObject temp = Instantiate(monster[num], monsterStandardPoint + this.gameObject.transform.GetChild(0).transform.position, monster[num].transform.rotation) as GameObject;
            temp.transform.parent = this.gameObject.transform.GetChild(0);
            temp.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            temp.GetComponent<tk2dSprite>().SortingOrder = temp.GetComponent<tk2dSprite>().SortingOrder - (randPosY - 10);
            temp.transform.GetChild(0).GetComponent<tk2dSprite>().SortingOrder = temp.GetComponent<tk2dSprite>().SortingOrder;
            monsterStandardPoint = new Vector3(0, 300, 0);
            GameEntry.SendMessage("MainSlotSetting");
        }
    }

    IEnumerator spawnEnemy()
    {
        print("생성");
        if (BattleScroll.isopening == false && BattleScroll.isending == false && Unit._lst_Enemy.Count != 0)
        {
                int i = Random.Range(0, 3);
                int randPosY = Random.Range(-10, 11);           //몬스터가 여러개 생길수 있기때문에 sorting order를 다르게 하여 그려준다.
                GameObject temp = Instantiate(enemy[i], enemy[i].transform.position, enemy[i].transform.rotation) as GameObject;
                temp.transform.position = enemyStandardPoint;
                temp.name = enemy[i].name;
                temp.transform.parent = this.gameObject.transform.GetChild(0);
                temp.transform.localScale = temp.transform.localScale;
                temp.GetComponent<tk2dSprite>().SortingOrder = temp.GetComponent<tk2dSprite>().SortingOrder - (randPosY - 10);
                temp.transform.GetChild(0).GetComponent<tk2dSprite>().SortingOrder = temp.GetComponent<tk2dSprite>().SortingOrder;


                //this.gameObject.transform.DOMoveX(-820f, 1, false);
                //yield return new WaitForSeconds(3f);
                //this.gameObject.transform.DOMoveX(0, 1, false);
        }
        SpawnTime = Random.Range(3.0f, 6.0f);
        yield return new WaitForSeconds(SpawnTime);
        StartCoroutine(spawnEnemy());
    }

    float CheckEnemyDistance()      //유닛과의 거리를 체크하기위한 함수
    {
        float distance = 10000;
        int number;
        if (Unit._lst_Enemy.Count > 1)
        {
            distance = Unit._lst_Enemy[1].transform.localPosition.x;
            number = 1;
            for (int i = 2; i < Unit._lst_Enemy.Count; i++)
            {
                if (Unit._lst_Enemy[i].transform.localPosition.x < distance)
                {
                    distance = Unit._lst_Enemy[i].transform.localPosition.x;
                    number = i;
                }
            }
            distance -= Unit._lst_Enemy[number].GetComponent<Enemy>().UnitSize * 0.5f;
        }

        return distance;
    }
    
}

