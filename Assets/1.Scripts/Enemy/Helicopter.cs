﻿using UnityEngine;
using System.Collections;

public class Helicopter : Skill {

    int groundDamage;
    int airDamage;

    public override int ConfirmAttackType(int number)
    {
        switch (number)
        {
            case 0:
                return 0;
            case 1:
                return 1;
            default:
                return 0;
            /*   case 3:
                   return
               case 4:
                   return
              */
        }
    }

    public override IEnumerator Skill1(Unit ally, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("enemy{0}_atk", self.number)))
        {
            self.animation.Play(string.Format("enemy{0}_atk", self.number));
        }

        while (self.animation.CurrentClip.name == string.Format("enemy{0}_atk", self.number) && self.animation.CurrentFrame <= 47)
        {
            yield return true;
        }

        Damage(ally, self, "ground");

        while (self.animation.CurrentClip.name == string.Format("enemy{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        self.isAttackPlaying = false;
        if (self.state_L.Hp > 0)
            self.state = (int)unitState.IDLE;
    }

    public override IEnumerator Skill2(Unit ally, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("enemy{0}_atk", self.number)))
        {
            self.animation.Play(string.Format("enemy{0}_atk", self.number));
        }
        while (self.animation.CurrentClip.name == string.Format("enemy{0}_atk", self.number) && self.animation.CurrentFrame <= 23)
        {
            yield return true;
        }

        Damage(ally, self, "air");

        while (self.animation.CurrentClip.name == string.Format("enemy{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        self.isAttackPlaying = false;
        if (self.state_L.Hp > 0)
            self.state = (int)unitState.IDLE;
    }
}
