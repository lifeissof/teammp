﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpaceBattleShip : Unit
{
    BattleScroll battleScroll = new BattleScroll();
    bool isPlaying;

    public GameObject SpaceShipSoundObject;
    public AudioClip gameover;

    public float height;

    // Use this for initialization
    void Start()
    {
        isPlaying = false;
        battleScroll = GameObject.Find("Manager").GetComponent<BattleScroll>();

        state_L.MaxHp = 1000;
        state_L.Hp = 1000;

        instanceHpBar = Instantiate(hpBar, this.gameObject.transform.position + hpBarPosition, this.gameObject.transform.rotation) as GameObject;
        instanceHpBar.transform.parent = this.gameObject.transform;
        instanceHpBar.transform.localScale = instanceHpBar.transform.localScale;

        _lst_Enemy.Add(this.gameObject);
        animation.enabled = false;

    }

    void Update()
    {
        
        if (BattleScroll.isopening == false && BattleScroll.isending == false && state_L.Hp > 0)
        {
            //instanceHpBar.transform.position = this.gameObject.transform.position + hpBarPosition;

            //instanceHpBar.transform.GetChild(0).GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(((float)state_L.Hp / (float)state_L.MaxHp) * 64, 13);
        }

        if (state_L.Hp <= 0 && isPlaying == false)
        {
            isPlaying = true;
            StartCoroutine(die());
        }
        
    }


    public override IEnumerator die()
    {

        battleScroll.StopTimeCheck();
        SpaceShipSoundObject.GetComponent<AudioSource>().clip = gameover;
        SpaceShipSoundObject.GetComponent<AudioSource>().loop = false;
        SpaceShipSoundObject.GetComponent<AudioSource>().Play();


        Destroy(instanceHpBar);

        for (int i = 0; i < Unit._lst_Enemy.Count; ++i)
        {
            Unit._lst_Enemy[i].SendMessage("Ending");
        }

        Unit._lst_Ally.Clear();
        Unit._lst_Enemy.Clear();

        BattleScroll.isending = true;
        animation.enabled = true;

        while (animation.IsPlaying(string.Format("{0}_crash", this.gameObject.name)) == false)
        {
            animation.Play(string.Format("{0}_crash", this.gameObject.name));
            yield return true;
        }

        while (animation.CurrentClip.name == string.Format("{0}_crash", this.gameObject.name) && animation.CurrentFrame != animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        while (_lst_Enemy.Count != 0)
        {
            yield return true;
        }


  
        yield return new WaitForSeconds(3f);
        battleScroll.SendMessage("ChangeEnemyValue");
        battleScroll.ShowResultWin();

        Destroy(this.gameObject);
    }

    public override void Ending()
    {
        //Destroy(instanceHpBar);
    }

    public void Damaged()
    {
        if (state_L.Hp > 0)
        {
            if (animation.enabled == false)
            {
                animation.enabled = true;
            }

            if (animation.IsPlaying(string.Format("{0}_damaged", this.gameObject.name)) == false)
            {
       
                animation.Play(string.Format("{0}_damaged", this.gameObject.name));
            }
            


        }
        else
        {
            if (animation.IsPlaying(string.Format("{0}_crash", this.gameObject.name)) == false)
                animation.Play(string.Format("{0}_crash", this.gameObject.name));
        }
    }
}
