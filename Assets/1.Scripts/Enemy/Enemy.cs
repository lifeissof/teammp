﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;

public class Enemy : AutoUnit {

	// Use this for initialization
    bool meeting = false;
    bool isReadytoMove = false;
    static int enemyCount = 1;

    float hiveSourceGetChance;
    public List<GameObject> hiveSource = new List<GameObject>();
    GameObject hiveSourceDrop;
    tk2dSprite Source = new tk2dSprite();

    public GameObject MaterialManager;

    public int fireMaterial;
    public int waterMaterial;
    public int plantMaterial;
    public int normalLevelMaterial;
    public int rareLevelMaterial;
    public int golds;
    

    //public tk2dSpriteAnimation[] EnemyLib = new tk2dSpriteAnimation[3];

     void Awake()
    {

        isAttackPlaying = false;
        MaterialManager = GameObject.FindGameObjectWithTag("BattleManager");
        this.gameObject.transform.position = this.gameObject.transform.position + new Vector3(0, state_P.posY, 0);
        number = System.Convert.ToInt32(this.gameObject.name.Substring(6, 1));
        //state_P = DataManage.enemyPublicInfo[number];       //마찬가지로 로컬내에서 변수값을 넣는다
        //state_L = DataManage.enemyLevelInfo[number];


        state_P = ServerInterface.Instance.enemyState_P[number];      //나중에 바꿔줄예정
        state_L = ServerInterface.Instance.enemyState_L[number];

        mySkill = CreateSkill(this.gameObject.name);
        dotTime = 0f;
        isDotDamaged = false;

        //나오는 재료에 대한 변값 80%
        hiveSourceGetChance = 80;


        enemyCount++;

        //hiveSourceGetChance = 80;

    }

	void Start () {



         _lst_Enemy.Add(this.gameObject);

         instanceHpBar = Instantiate(hpBar, this.gameObject.transform.position + (hpBarPosition), this.gameObject.transform.rotation) as GameObject;
         instanceHpBar.transform.parent = this.gameObject.transform;
         instanceHpBar.transform.localScale = instanceHpBar.transform.localScale;
         isHpBarExist = true;


         isReadytoMove = true;      

	}
	
	// Update is called once per frame
    void Update()
    {

        if (isHpBarExist == true)
        {
            instanceHpBar.transform.GetChild(0).GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(((float)state_L.Hp / (float)state_L.MaxHp) * 64, 13);
        }
        
        StateMachine();
    }

    public override Skill CreateSkill(string name)
    {
        switch (name)
        {
            case "enemy00(Clone)":
                return new Dron();
            case "enemy01(Clone)":
                return new Tank();
            case "enemy02(Clone)":
                return new Helicopter();
            default:
                return null;
        }
    }

 

    public override void setTarget()
    {
        if (_lst_Ally.Count > 0)
        {
            for (int i = 0; i < _lst_Ally.Count; ++i)
            {
                //print(1);
                if (_lst_Ally[i].transform.position.x <= this.gameObject.transform.position.x)
                {
                   // print(2);
                    Unit allySize = _lst_Ally[i].GetComponent<Unit>();
                    if ((allySize.state_P.unitType == (int)type.AIR && state_L.attackAir != 0) || (allySize.state_P.unitType == (int)type.GROUND && state_L.attackGround != 0))
                    {
                        CurrDistance = (this.gameObject.transform.position.x - (UnitSize * 0.5f)) - (_lst_Ally[i].transform.position.x + (allySize.UnitSize * 0.5f));
                        if (CurrDistance >= 0 && CurrDistance <= ShortDistance)
                        {
                            Target = _lst_Ally[i];
                            ShortDistance = (this.gameObject.transform.position.x - (UnitSize * 0.5f)) - (Target.transform.position.x + (allySize.UnitSize * 0.5f));
                        }
                    }
                    else if (_lst_Ally[i].gameObject.tag == "god")
                    {
                       
                        CurrDistance = ((this.gameObject.transform.position.x + (allySize.UnitSize * 0.5f)) - (_lst_Ally[i].transform.position.x - (UnitSize * 0.5f)));
                        if (CurrDistance >= 0 && CurrDistance < ShortDistance)
                        {
                            Target = _lst_Ally[i];
                            ShortDistance = (Target.transform.position.x - (UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (allySize.UnitSize * 0.5f));
                        }
                    }
                    else
                    {
                        //CurrDistance = (this.gameObject.transform.position.x - (UnitSize * 0.5f)) - (_lst_Ally[i].transform.position.x + (allySize.UnitSize * 0.5f));
                        //if (CurrDistance <= state_L.attackRange && CurrDistance >= 0)
                        //{
                        //    StopCoroutine("Meeting");
                        //}
                    }
                }
            }
            if (Target != null)
            {
            
                Unit ally = Target.GetComponent<Unit>();
                ShortDistance = (this.gameObject.transform.position.x - (UnitSize * 0.5f)) - (Target.transform.position.x + (ally.UnitSize * 0.5f));
            }
        }
    }

    public override void walk()
    {
        if (state_P.unitType == 0)
            animation.Play(string.Format("enemy{0}_walk", number));
        else if (state_P.unitType == 1)
            animation.Play(string.Format("enemy{0}_fly", number));
        if (meeting == false)
            this.gameObject.transform.position += (Vector3.left.normalized * state_L.speed * Time.deltaTime);
    }

    public override IEnumerator WalkTime()
    {
        yield return new WaitForSeconds(state_L.movingTime);
        if (state == (int)unitState.WALK && state_L.Hp > 0)
        {
            state = (int)unitState.SEE;
            StartCoroutine("WalkDelay");
        }
    }
    public override IEnumerator WalkDelay()
    {
        yield return new WaitForSeconds(state_L.movingDelay);
        if (state == (int)unitState.SEE && state_L.Hp > 0)
        {
            state = (int)unitState.WALK;
            StartCoroutine("WalkTime");
        }
    }

    public override IEnumerator meet()
    {
        meeting = true;
        yield return new WaitForSeconds(1f);
        meeting = false;
    }


    public override void see()
    {
        animation.Play(string.Format("enemy{0}_see", number));
    }

    public override void idle()
    {
        animation.Play(string.Format("enemy{0}_idle", number));
    }
    public override void attack()
    {
        if (Target != null)
        {
            Unit ally = Target.GetComponent<Unit>();

            float delay;
            if (ally.state_P.unitType == 0)
            {
                delay = state_L.attackGroundDelay;
            }
            else
            {
                delay = state_L.attackAirDelay;
            }

            if (Atk_Timer == 0 && isAttackPlaying == false)
            {
                Attacking(ally);
            }

            Atk_Timer += Time.deltaTime;

            if (Atk_Timer >= delay)
            {
                Atk_Timer = 0;
            }

            
            if ((Target.transform.position.x + (ally.UnitSize * 0.5f)) > (this.gameObject.transform.position.x - (UnitSize * 0.5f)))
            {
                Target = null;
            }

            if (null != Target && 0 >= ally.state_L.Hp)
            {
                Target = null;
            }
        }
        else
            state = (int)unitState.IDLE;
    }


    void Attacking(Unit ally)
    {
        isAttackPlaying = true;
        if ((Target.transform.position.x + (ally.UnitSize * 0.5f)) <= (this.gameObject.transform.position.x - (UnitSize * 0.5f)))
        {
            //if ((int)type.GROUND == state_P.unitType || (int)type.AIR == state_P.unitType)        //if문을 제거해서 적은 지상 공중 모두 공격가능        //if문을 풀어버리면 지상은 지상 공중은 공중끼리 공격함
            //{
            //   
            UseSkill(ally, ally.state_P.unitType, 1);
            //}
        }
        else
        {
            isAttackPlaying = false;
            if (state_L.Hp > 0)
                state = (int)unitState.IDLE;
        }
    }


    public override IEnumerator die()
    {
    
        Destroy(instanceHpBar);
        isHpBarExist = false;
        EmptyHP = true;
        _lst_Enemy.Remove(this.gameObject);

        while (animation.IsPlaying(string.Format("enemy{0}_crash", number)) == false)
        {
            animation.Play(string.Format("enemy{0}_crash", number));
            yield return true;
        }
        if (state_P.unitType == 1 && animation.IsPlaying(string.Format("enemy{0}_crash", number)))
        {
            float tweenTime = animation.CurrentClip.frames.Length / animation.CurrentClip.fps;
            this.gameObject.transform.DOMoveY(-state_P.posY * 0.5f ,tweenTime , false ).SetEase(Ease.InQuart);
        }

        yield return new WaitForSeconds(animation.CurrentClip.frames.Length / animation.CurrentClip.fps);


        //계속 생성되는 오류가 존재한다
        if (Random.Range(1, 101) <= hiveSourceGetChance)//확률 80%
        {
            int rand;
            rand = Random.Range(0, 4); // 하이브소스 종류
            

            hiveSourceDrop = Instantiate(hiveSource[rand], this.gameObject.transform.position, this.gameObject.transform.rotation) as GameObject;
            Source = hiveSourceDrop.GetComponent<tk2dSprite>();
            hiveSourceDrop.transform.parent = this.gameObject.transform.parent;
            hiveSourceDrop.transform.localScale = new Vector3(hiveSourceDrop.transform.localScale.x, hiveSourceDrop.transform.localScale.y, 0);
            hiveSourceDrop.GetComponent<tk2dSprite>().SortingOrder = this.gameObject.GetComponent<tk2dSprite>().SortingOrder + 10;
            hiveSourceDrop.transform.DOMoveY(hiveSourceDrop.transform.position.y + 50, 1, false).SetEase(Ease.OutQuad);
            hiveSourceDrop.GetComponent<tk2dSprite>().DOFade(0, 1.5f);


            switch (rand)
            {
                case 0:
                    {
                        fireMaterial = ServerInterface.Instance.userState.GetFireMaterial();
                        fireMaterial++;
                        ServerInterface.Instance.userState.SetFireMaterial(fireMaterial);
                        MaterialManager.GetComponent<BattleScroll>().FireMaterialDropNum++;
                    }
                    break;

                case 1:
                    {
                        waterMaterial = ServerInterface.Instance.userState.GetWaterMaterial();
                        waterMaterial++;
                        ServerInterface.Instance.userState.SetWaterMaterial(waterMaterial);
                        MaterialManager.GetComponent<BattleScroll>().WaterMaterialDropNum++;
                    }
                    break;

                case 2:
                    {
                        plantMaterial = ServerInterface.Instance.userState.GetPlantMaterial();
                        plantMaterial++;
                        ServerInterface.Instance.userState.SetPlantMaterial(plantMaterial);
                        MaterialManager.GetComponent<BattleScroll>().PlantMaterialDropNum++;
                    }
                    break;

                case 3:
                    {
                        normalLevelMaterial = ServerInterface.Instance.userState.GetNormalMaterial();
                        normalLevelMaterial++;
                        ServerInterface.Instance.userState.SetNormalMaterial(normalLevelMaterial);
                        MaterialManager.GetComponent<BattleScroll>().NLMaterialDropNum++;
                    }
                    break;

                case 4:
                    {
                        rareLevelMaterial = ServerInterface.Instance.userState.GetRareMaterial();
                        rareLevelMaterial++;
                        ServerInterface.Instance.userState.SetRareMaterial(rareLevelMaterial);
                        MaterialManager.GetComponent<BattleScroll>().RLMaterialDropNum++;
                    }
                    break; 
            }

            Destroy(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public override void splash(int num)
    {
        float dis = 0;
        int HitNum = 0;
        Unit targetunit = Target.GetComponent<Unit>();
        for (int i = 0; i < _lst_Ally.Count; ++i)
        {
            if (_lst_Ally[i] != Target)
            {
                Unit allyunit = _lst_Enemy[i].GetComponent<Unit>();
                dis = Mathf.Abs((Target.transform.position.x + (targetunit.UnitSize * 0.5f)) - (_lst_Ally[i].transform.position.x + (allyunit.UnitSize * 0.5f)));
                if (dis < 10)
                {
                    HitNum++;
                    if (allyunit.state_P.unitType == (int)type.AIR)
                        allyunit.state_L.Hp -= state_L.attackAir;
                    else
                        allyunit.state_L.Hp -= state_L.attackGround;

                }
            }
            if (HitNum >= num)
                break;
        }
    }

    public override void Ending()
    {
        hiveSourceGetChance = 0;
        state_L.Hp = 0;
        //Destroy(instanceHpBar);
        //Destroy(this.gameObject);
    }

    public override void DotDamaged(Unit target, int damage)
    {
        if (target.isDotDamaged == false) StartCoroutine(DotDamagedPerSecond(target, damage));
        else
        {
            if (target.state_L.Hp - damage >= 0)
                target.state_L.Hp -= damage;
            else
                target.state_L.Hp = 0;
        }
        target.instanceHpBar.GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(target.state_L.Hp * 1, 10.0f);
    }

    public IEnumerator DotDamagedPerSecond(Unit target, int damage)
    {
        target.isDotDamaged = true;
        if (target.state_L.Hp - damage >= 0)
            target.state_L.Hp -= damage;
        else
        {
            target.state_L.Hp = 0;
            target.dotTime = 3;
        }
        yield return new WaitForSeconds(1.0f);
        if (target.dotTime < 3)
        {
            target.dotTime++;
            StartCoroutine(DotDamagedPerSecond(target, damage));
        }
        else
        {
            target.isDotDamaged = false;
        }

        
    }

}

