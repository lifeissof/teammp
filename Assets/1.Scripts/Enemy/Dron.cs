﻿using UnityEngine;
using System.Collections;

public class Dron : Skill{

    public override IEnumerator Skill1(Unit ally, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("enemy{0}_atk", self.number)))
        {
            self.animation.Play(string.Format("enemy{0}_atk", self.number));
            yield return new WaitForSeconds(0.73f);
        }
        while (self.animation.CurrentClip.name == string.Format("enemy{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        Damage(ally, self, "ground");

        self.isAttackPlaying = false;
        if (self.state_L.Hp > 0)
            self.state = (int)unitState.IDLE;
    }
}
