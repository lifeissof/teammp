﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
public class MainManager : MonoBehaviour {

    public DataManage dataManager;
    public AndroidManager androidManager;
    public GameObject ForBgm;
    public AudioClip bgm;
    public AudioClip stemp_txt1;
    public AudioClip stemp_txt2;
    public AudioClip stemp_planet;
    public AudioClip btn;


    void Awake()
    {
        //Screen.SetResolution(Screen.width, Screen.width * 16 / 10, true);
    }
    // Use this for initialization

    void Start () {
        PlayerPrefs.SetString("utoken", "testid");
        ServerInterface.Instance.GetUserData();
        ServerInterface.Instance.GetStageData();
        ServerInterface.Instance.GetPublicMonster();
        ServerInterface.Instance.GetPublicEnemy();
        ServerInterface.Instance.GetBatchByLevelMonster(1);
        ServerInterface.Instance.GetBatchByLevelEnemy(1);
        ServerInterface.Instance.GetMonsterExist();
        ServerInterface.Instance.GetEntryMonster();

        //print("utoken : " + PlayerPrefs.GetString("utoken"));
        //ServerInterface.Instance.GetSingleMonster(1, "mon01");
        //ServerInterface.Instance.userState.SetEntryNumber(3); // 엔트리 넘버를 불러오는 구문이 필요함
        //ServerInterface.Instance.userState.SetStage(1);
        //ServerInterface.Instance.userState.userStage.SetStarNumber(0, 3); // 이걸로 별 갯수 설정 가능
        //ServerInterface.Instance.GetTest();
        //ServerInterface.Instance.GetSingleMonster(1, "mon00");
        //ServerInterface.Instance.AddStageData(3, 3);
        //print(ServerInterface.Instance.monsterState_L[0].Level);
        //print(ServerInterface.Instance.monsterState_L[1].Level);
        //ServerInterface.Instance.GetPublicMonster();
        //ServerInterface.Instance.GetBatchByLevelEnemy(1);
        //ServerInterface.Instance.GetBatchByLevelMonster(1);

        //if (PlayerPrefs.GetString("utoken") == "testid") // null
        //{
        //    ServerInterface.Instance.JoinUser("testid", "1111");
        //}

        //else
        // {
        //     ServerInterface.Instance.GetUserData();
        //  }
        //ServerInterface.Instance.UpdateEntryNumberData(4);
    }

    // Update is called once per frame
    void Update () {

        SoundControl();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //데이터의 저장부분 필요
            Setting.SaveSettingData();
            Application.Quit();
        }
        
	}
  

    void StartFunc()
    {
        GetComponent<AudioSource>().clip = btn;
        GetComponent<AudioSource>().Play();
        DataManage.entrySlotNum = ServerInterface.Instance.userState.GetEntryNumber(); // 임시방편 고쳐야함
        for (int i = 0; i < DataManage.entrySlotNum; ++i)
        {
        DataManage.entryName[i] = ServerInterface.Instance.userState.GetEntry(i);
        }
        DataManage.SourceInit(); // 임시방편 고쳐야함 <- 이걸 고칠때 SourceInit을 public static void 에서 void 로 바꿔야함
        //Application.LoadLevelAsync("Lobby");
        SceneManager.LoadSceneAsync("Lobby");
        
    }

    void KakaoFunc()
    {
         //androidManager.onClick();
        print("kakao!!!!");
    }

    void PlayTxt1Sound()
    {

        GetComponent<AudioSource>().clip = stemp_txt1;
        GetComponent<AudioSource>().Play();

    }

    void PlayTxt2Sound()
    {
        GetComponent<AudioSource>().clip = stemp_txt2;
        GetComponent<AudioSource>().Play();

    }

    void PlayPlanetSound()
    {
        GetComponent<AudioSource>().clip = stemp_planet;
        GetComponent<AudioSource>().Play();
    }

    void PlayBgm()
    {
        GetComponent<AudioSource>().clip = bgm;
        GetComponent<AudioSource>().Play();
    }

    void SoundControl()
    {
        if (Setting.bgmOX)
        {
            ForBgm.GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            ForBgm.GetComponent<AudioSource>().volume = 0;
        }
        if (Setting.soundOX)
        {
            GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            GetComponent<AudioSource>().volume = 0;
        }
    }
}
