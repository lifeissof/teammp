﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GradeupManage : MonoBehaviour {

    public List<GameObject> Monsters = new List<GameObject>(15);

    public tk2dSpriteAnimation[] GradeUpCharacterLib = new tk2dSpriteAnimation[15];

    public List<GameObject> BeforeCharacterStars = new List<GameObject>();
    public List<GameObject> AfterCharacterStars = new List<GameObject>();

    public GameObject GradeUpSetting;
    public tk2dSprite BeforeCharacterImage;
    public tk2dSprite AfterCharacterImage;
    public tk2dSprite MonsterAttributeImage;

    public tk2dSpriteAnimator BeforeCharacterAnim;
    public tk2dSpriteAnimator AfterCharacterAnim;
    
    public tk2dTextMesh Lv;
    public tk2dTextMesh BeforeMaxLv;
    public tk2dTextMesh AfterMaxLv;

    public tk2dTextMesh BeforeCharacterHp;
    public tk2dTextMesh BeforeCharacterSpeed;
    public tk2dTextMesh BeforeCharacterGround;
    public tk2dTextMesh BeforeCharacterAir;

    public tk2dTextMesh AfterCharacterHp;
    public tk2dTextMesh AfterCharacterSpeed;
    public tk2dTextMesh AfterCharacterGround;
    public tk2dTextMesh AfterCharacterAir;

    public tk2dTextMesh HaveMonsterGradeMaterial;
    public tk2dTextMesh StandardMonsterGradeMaterial;
    public tk2dTextMesh NeedUserGoldText;

    int increasePercent = 10;


    Ray ray;
    RaycastHit hit;

    public string selectNum;

    public GameObject HiveSoundObject;
    public AudioClip btn_Select;
    public AudioClip btn_GradeUpSound;
    public AudioClip btn_NotMaterial;

    int AfterHpValue;
    int AfterSpeedValue;
    int AfterGroundValue;
    int AfterAirValue;

    int haveGradeMaterialNum;
    int NeedGradeMaterialNum;
    int NeedGradeGold;


    private int BeforeStarCount;
    private int AfterStarCount;




    IEnumerator waitSeconds()
    {
        yield return new WaitForSeconds(0.5f);
    }


    // Use this for initialization
    void Start () {

       // DataManage._srcUpgrade[0].SetEnergy(20);        //몬스터 승급 업그레이드를 위해 10개 추가해봄  <- 서버에서 받아오도록 변경함 16 07 27

	
	}
	
	// Update is called once per frame
	void Update () {

        GradeupCharacter();
	}

    public void GradeupCharacter()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "CharacterGrade")  ////
            {
                HiveSoundObject.GetComponent<AudioSource>().clip = btn_Select;
                HiveSoundObject.GetComponent<AudioSource>().Play();

                selectNum = hit.collider.name.Substring(9, 2);
                if (DataManage._monster[System.Convert.ToInt32(selectNum)].monsterExist == true)
                {


                    BeforeCharacterImage.spriteId = BeforeCharacterImage.GetSpriteIdByName(string.Format("character{0}", selectNum));
                    BeforeCharacterAnim.Library = GradeUpCharacterLib[System.Convert.ToInt32(selectNum)];
                    BeforeCharacterAnim.Play(hit.collider.name.Substring(0, 11));
                    BeforeCharacterAnim.enabled = true;

                    AfterCharacterImage.spriteId = BeforeCharacterImage.GetSpriteIdByName(string.Format("character{0}", selectNum));
                    AfterCharacterAnim.Library = GradeUpCharacterLib[System.Convert.ToInt32(selectNum)];
                    AfterCharacterAnim.Play(hit.collider.name.Substring(0, 11));
                    AfterCharacterAnim.enabled = true;


                    Lv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level.ToString();      //현재 레벨

                    GradeUpData(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar);
                    GradeUpSetting.SetActive(true);


                }
                else
                {
                    print("해당 몬스터가 없음");
                    //원래는 팝업창을 띄워야 한다.
                    //몬스터가 없을 경우
                }
                //check.transform.position = hit.collider.transform.position + new Vector3(50, 50, -2);

            }
        }
    }

    void GradeUpData(int grade)
    {

        int increaseHp = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp / increasePercent;
        int increaseSpeed = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed / increasePercent;
        int increaseAG = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround / increasePercent;
        int increaseAA = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir / increasePercent;


        switch (grade)
        {
            case 1:
                BeforeStarCount = grade;
                AfterStarCount = BeforeStarCount + 1;

                
                switch (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute.GetHashCode())
                {
                    case 0:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("fireTypeIcon");
                        break;

                    case 1:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("waterTypeIcon");
                        break;

                    case 2:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("woodTypeIcon");
                        break;

                }
                haveGradeMaterialNum = DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].GetEnergy().GetHashCode();

                NeedGradeMaterialNum = DataManage._monster[System.Convert.ToInt32(selectNum)].GetUpgradeSource().GetValue(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute).GetHashCode() * BeforeStarCount;
                NeedGradeGold = 20000 * BeforeStarCount;

                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " +  (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv + 10);
                //BeforeMaxLv.text = "Max "+ 10.ToString();       //몬스터의 맥스레벨
                //AfterMaxLv.text = "MaxLv  " + 20.ToString();

            

                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (BeforeStarCount > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();


                AfterHpValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp + increaseHp;
                AfterSpeedValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed + increaseSpeed;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround + increaseAG;
                }
                else AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir + increaseAA;
                }
                else AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;
               
                AfterCharacterHp.text = AfterHpValue.ToString();
                AfterCharacterSpeed.text = AfterSpeedValue.ToString();
                AfterCharacterGround.text = AfterGroundValue.ToString();
                AfterCharacterAir.text = AfterAirValue.ToString();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                NeedUserGoldText.text = NeedGradeGold.ToString();
                break;

            case 2:


                BeforeStarCount = grade;
                AfterStarCount = BeforeStarCount + 1;


                switch (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute.GetHashCode())
                {
                    case 0:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("fireTypeIcon");
                        break;

                    case 1:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("waterTypeIcon");
                        break;

                    case 2:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("woodTypeIcon");
                        break;

                }
                haveGradeMaterialNum = DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].GetEnergy().GetHashCode();

                NeedGradeMaterialNum = DataManage._monster[System.Convert.ToInt32(selectNum)].GetUpgradeSource().GetValue(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute).GetHashCode() * BeforeStarCount;
                NeedGradeGold = 20000 * BeforeStarCount;

                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " + (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv + 10);
                //BeforeMaxLv.text = "Max "+ 20.ToString();       //몬스터의 맥스레벨
                //AfterMaxLv.text = "MaxLv  " + 30.ToString();



                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (BeforeStarCount > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();


                AfterHpValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp + increaseHp;
                AfterSpeedValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed + increaseSpeed;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround + increaseAG;
                }
                else AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir + increaseAA;
                }
                else AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;

                AfterCharacterHp.text = AfterHpValue.ToString();
                AfterCharacterSpeed.text = AfterSpeedValue.ToString();
                AfterCharacterGround.text = AfterGroundValue.ToString();
                AfterCharacterAir.text = AfterAirValue.ToString();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                NeedUserGoldText.text = NeedGradeGold.ToString();
                break;

            case 3:

                BeforeStarCount = grade;
                AfterStarCount = BeforeStarCount + 1;

                switch (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute.GetHashCode())
                {
                    case 0:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("fireTypeIcon");
                        break;

                    case 1:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("waterTypeIcon");
                        break;

                    case 2:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("woodTypeIcon");
                        break;

                }
                haveGradeMaterialNum = DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].GetEnergy().GetHashCode();
                NeedGradeMaterialNum = DataManage._monster[System.Convert.ToInt32(selectNum)].GetUpgradeSource().GetValue(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute).GetHashCode() * BeforeStarCount;
                NeedGradeGold = 20000 * BeforeStarCount;

                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " + (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv + 10);
                //BeforeMaxLv.text = "Max "+ 30.ToString();       //몬스터의 맥스레벨
                //AfterMaxLv.text = "MaxLv  " + 40.ToString();



                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (BeforeStarCount > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();


                AfterHpValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp + increaseHp;
                AfterSpeedValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed + increaseSpeed;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround + increaseAG;
                }
                else AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir + increaseAA;
                }
                else AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;

                AfterCharacterHp.text = AfterHpValue.ToString();
                AfterCharacterSpeed.text = AfterSpeedValue.ToString();
                AfterCharacterGround.text = AfterGroundValue.ToString();
                AfterCharacterAir.text = AfterAirValue.ToString();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                NeedUserGoldText.text = NeedGradeGold.ToString();
                break;

            case 4:

                BeforeStarCount = grade;
                AfterStarCount = BeforeStarCount + 1;

                switch (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute.GetHashCode())
                {
                    case 0:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("fireTypeIcon");
                        break;

                    case 1:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("waterTypeIcon");
                        break;

                    case 2:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("woodTypeIcon");
                        break;

                }
                haveGradeMaterialNum = DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].GetEnergy().GetHashCode();

                NeedGradeMaterialNum = DataManage._monster[System.Convert.ToInt32(selectNum)].GetUpgradeSource().GetValue(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute).GetHashCode() * BeforeStarCount;
                NeedGradeGold = 20000 * BeforeStarCount;

                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " + (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv + 10);
                //BeforeMaxLv.text = "Max "+ 40.ToString();       //몬스터의 맥스레벨
                //AfterMaxLv.text = "MaxLv  " + 50.ToString();



                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (BeforeStarCount > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();


                AfterHpValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp + increaseHp;
                AfterSpeedValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed + increaseSpeed;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround + increaseAG;
                }
                else AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir + increaseAA;
                }
                else AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;

                AfterCharacterHp.text = AfterHpValue.ToString();
                AfterCharacterSpeed.text = AfterSpeedValue.ToString();
                AfterCharacterGround.text = AfterGroundValue.ToString();
                AfterCharacterAir.text = AfterAirValue.ToString();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                NeedUserGoldText.text = NeedGradeGold.ToString();
                break;

            case 5:

                BeforeStarCount = grade;
                AfterStarCount = BeforeStarCount + 1;

                switch (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute.GetHashCode())
                {
                    case 0:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("fireTypeIcon");
                        break;

                    case 1:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("waterTypeIcon");
                        break;

                    case 2:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("woodTypeIcon");
                        break;

                }
                haveGradeMaterialNum = DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].GetEnergy().GetHashCode();

                NeedGradeMaterialNum = DataManage._monster[System.Convert.ToInt32(selectNum)].GetUpgradeSource().GetValue(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute).GetHashCode() * BeforeStarCount;
                NeedGradeGold = 20000 * BeforeStarCount;

                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " + (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv + 10);
                //BeforeMaxLv.text = "Max "+ 50.ToString();       //몬스터의 맥스레벨
                //AfterMaxLv.text = "MaxLv  " + 60.ToString();



                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (BeforeStarCount > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();


                AfterHpValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp + increaseHp;
                AfterSpeedValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed + increaseSpeed;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround + increaseAG;
                }
                else AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir + increaseAA;
                }
                else AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;

                AfterCharacterHp.text = AfterHpValue.ToString();
                AfterCharacterSpeed.text = AfterSpeedValue.ToString();
                AfterCharacterGround.text = AfterGroundValue.ToString();
                AfterCharacterAir.text = AfterAirValue.ToString();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                NeedUserGoldText.text = NeedGradeGold.ToString();
                break;

            case 6:

                BeforeStarCount = grade;
                AfterStarCount = BeforeStarCount;

                switch (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute.GetHashCode())
                {
                    case 0:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("fireTypeIcon");
                        break;

                    case 1:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("waterTypeIcon");
                        break;

                    case 2:
                        MonsterAttributeImage.spriteId = MonsterAttributeImage.GetSpriteIdByName("woodTypeIcon");
                        break;

                }
                haveGradeMaterialNum = 0;
                NeedGradeMaterialNum = 0;
                NeedGradeGold = 0;

                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                //BeforeMaxLv.text = "Max "+ 60.ToString();       //몬스터의 맥스레벨
                //AfterMaxLv.text = "MaxLv  " + 60.ToString();



                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (BeforeStarCount > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();


                AfterHpValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp ;
                AfterSpeedValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed ;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;
                }
                else AfterGroundValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;
                }
                else AfterAirValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir;
               
                AfterCharacterHp.text = AfterHpValue.ToString();
                AfterCharacterSpeed.text = AfterSpeedValue.ToString();
                AfterCharacterGround.text = AfterGroundValue.ToString();
                AfterCharacterAir.text = AfterAirValue.ToString();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                NeedUserGoldText.text = NeedGradeGold.ToString();
                break;
        }

    }


    public void Back()
    {
        GradeUpSetting.SetActive(false);

        HiveSoundObject.GetComponent<AudioSource>().clip = btn_Select;
        HiveSoundObject.GetComponent<AudioSource>().Play();
        //가지고 있는 경험치 아이템 갱신 코드 추가

        //예시 갱신화
        //ServerInterface.Instance.userState.SetNormalMaterial(normalMaterialNum);
        //ServerInterface.Instance.userState.SetRareMaterial(rareMaterialNum);
        for (int i = 0; i < 15; ++i)
        {
            Monsters[i].SendMessage("initInfo");
        }



    }

    public void GradeUp()
    {
        if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level == ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv)            //캐릭터의 레벨이 Max레벨과 같을 경우
        {
            if (haveGradeMaterialNum >= NeedGradeMaterialNum && ServerInterface.Instance.userState.GetGold() >= NeedGradeGold) //승급 재료와 골드가 모두 필요기준개수보다 높을 경우
            {
                HiveSoundObject.GetComponent<AudioSource>().clip = btn_GradeUpSound;
                HiveSoundObject.GetComponent<AudioSource>().Play();

                //이펙트 코루틴 함수 추가.

                //DataManage._monster[System.Convert.ToInt32(selectNum)].grade += 1;
                //DataManage._monster[System.Convert.ToInt32(selectNum)].maxLevel += 10;
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar += 1;
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv += 10;

                //서버연동 부분 능력치 값을 저장해야 한다.
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp = AfterHpValue;
                //print(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp);
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed = AfterSpeedValue;
                //print(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed);
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround = AfterGroundValue;
                //print(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround);
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir = AfterAirValue;
                //print(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir);

                //골드와 가지고 있는 재료의 개수를 감소시킨다.
                ServerInterface.Instance.userState.SetGold(ServerInterface.Instance.userState.GetGold() - NeedGradeGold);
                DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].SetEnergy(haveGradeMaterialNum - NeedGradeMaterialNum);


                // 서버에 데이터를 통신하는 과정
                ServerInterface.Instance.SetMonsterAbility(System.Convert.ToInt32(selectNum),
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed,
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir,
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level,
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv,
                        ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar,
                        ServerInterface.Instance.userState.GetGold());

                waitSeconds();
                
                // 업그레이드 후 화면 최신화 시작

                BeforeCharacterHp.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                BeforeCharacterSpeed.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                BeforeCharacterGround.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                BeforeCharacterAir.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();

                AfterCharacterHp.text = (AfterHpValue + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar * 10).ToString();
                AfterCharacterSpeed.text = (AfterSpeedValue + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar * 10).ToString();
                if(AfterGroundValue != 0)
                {
                    AfterCharacterGround.text = (AfterGroundValue + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar * 10).ToString();
                }
                else AfterCharacterGround.text = AfterGroundValue.ToString();

                if (AfterAirValue != 0)
                {
                    AfterCharacterAir.text = (AfterAirValue + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar * 10).ToString();
                }
                else AfterCharacterAir.text = AfterAirValue.ToString();

                haveGradeMaterialNum = DataManage._srcUpgrade[ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute].GetEnergy().GetHashCode();

                HaveMonsterGradeMaterial.text = haveGradeMaterialNum.ToString();
                StandardMonsterGradeMaterial.text = NeedGradeMaterialNum.ToString();
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar < 6)
                {
                    NeedUserGoldText.text = (NeedGradeGold + 20000).ToString();
                }
                else NeedUserGoldText.text = 0.ToString();



                BeforeMaxLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv;
                AfterMaxLv.text = "Lv  " + (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv + 10);

                if(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar < 6)
                {
                    AfterStarCount = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar + 1;
                }
                
                for (int i = 0; i < (int)Star.MAX; ++i)
                {
                    if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar > i) BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else BeforeCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");

                    if (AfterStarCount > i) AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                    else AfterCharacterStars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                }
                


                // 업그레이드 후 화면 최신화 끝
            }
            else
            {
                HiveSoundObject.GetComponent<AudioSource>().clip = btn_NotMaterial;
                HiveSoundObject.GetComponent<AudioSource>().Play();
                print("재료가 충분하지 않습니다");
                //팝업창 띄우기
            }
        }

        else
        {
            HiveSoundObject.GetComponent<AudioSource>().clip = btn_NotMaterial;
            HiveSoundObject.GetComponent<AudioSource>().Play();
            print("최대 레벨에 도달하지 못했습니다.");
            //팝업창 띄우기
        }

        
    }
    
}
