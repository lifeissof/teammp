﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class NewHiveUpgrade : MonoBehaviour
{


    public List<GameObject> typeBg = new List<GameObject>();
    public List<GameObject> InfoList = new List<GameObject>();

    public GameObject UpgradeSetting;
    public tk2dSprite bigCharacter;
   


    public List<GameObject> stars = new List<GameObject>();
    public Vector3[] CharacterPos = new Vector3[15]; // X값 중앙으로 맞추기 위한 리스트
    public tk2dTextMesh monsterLevel;
    private int starCount;
    private int TypeValue; //속성값

    public tk2dTextMesh monsterName;
    public tk2dTextMesh monsterExplanation;

    public tk2dTextMesh monsterHpDataText;
    public tk2dTextMesh monsterSpeedDataText;
    public tk2dTextMesh monsterGroundAttackDataText;
    public tk2dTextMesh monsterAirAttackDataText;
    //public tk2dTextMesh levelText;
    //public tk2dTextMesh expText;


    public GameObject InfoMonster;
    public GameObject HiveSoundObject;
    public GameObject HiveBgmObject;
    public AudioClip bgm;
    public AudioClip btn_Select;

    string selectName;
    public string selectNum;
    //public Gauge statGauge;
    public InfoGauge MonsterStatGauge;

    Ray ray;
    RaycastHit hit;



    // Use this for initialization

    // Update is called once per frame

    void Start()
    {
        HiveBgmObject.GetComponent<AudioSource>().clip = bgm;
        HiveBgmObject.GetComponent<AudioSource>().loop = true;
        HiveBgmObject.GetComponent<AudioSource>().Play();
    }
    void Update()
    {
        SoundControl();
        SelectCharacter();

    }


    public void SelectCharacter()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "CharacterInfo")
            {
                
                 HiveSoundObject.GetComponent<AudioSource>().clip = btn_Select;
                 HiveSoundObject.GetComponent<AudioSource>().Play();
                
                selectNum = hit.collider.name.Substring(9, 2);
                int spriteID;

                if (DataManage._monster[System.Convert.ToInt32(selectNum)].monsterExist == true)
                {


                   
                    print(selectNum);
                    int temp = System.Convert.ToInt32(selectNum);
                    InfoMonster = Instantiate(InfoList[temp], new Vector3(0, 0, 0), InfoList[temp].transform.rotation) as GameObject;
                    InfoMonster.GetComponent<tk2dSprite>().SortingOrder = 9;      
                    if (temp == 7 || temp == 8 || temp == 14)
                    {
                        InfoMonster.transform.localScale = new Vector3(1, 1, 1);
                        InfoMonster.transform.position = new Vector3(CharacterPos[temp].x, CharacterPos[temp].y, CharacterPos[temp].z);
                    }
                    else
                    {
                        InfoMonster.transform.localScale = new Vector3(2, 2, 1);
                        InfoMonster.transform.position = new Vector3(CharacterPos[temp].x, CharacterPos[temp].y, CharacterPos[temp].z);
                    }

                    bigCharacter.spriteId = bigCharacter.GetSpriteIdByName(string.Format("character{0}", selectNum));
                    selectName = string.Format("monster{0}", System.Convert.ToInt32(selectNum));
                    starCount = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar;
                    TypeValue = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute;

                    typeBg[TypeValue].SetActive(true);      //속성 이미지 표시

                    for (int i = 0; i < (int)Star.MAX; ++i)
                    {
                        if (starCount > i) stars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
                        else stars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
                    }

                    //SetDot(selectNum);
                    //currentGaugeScale = DataManage._monster[System.Convert.ToInt32(selectNum)].exp / ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp;
                    //gaugeBar.scale = new Vector3(currentGaugeScale, 1, 1);
                    //monsterExplanation.text = DataManage._monster[System.Convert.ToInt32(selectNum)].monsterInfo;
                    MonsterStatGauge.SetStat(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed);

                    //monsterHpDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].Hp.ToString();
                    monsterHpDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                    //monsterSpeedDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].speed.ToString();
                    monsterSpeedDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                    //monsterGroundAttackDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                    monsterGroundAttackDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                    //monsterAirAttackDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].attackAir.ToString();
                    monsterAirAttackDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();

                    monsterLevel.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level;

                }
                else
                {
                    tk2dSprite sprite = bigCharacter.gameObject.GetComponent<tk2dSprite>();

                    //   bigCharacter.Library = bigCharacterLib[System.Convert.ToInt32(selectNum)];
                    //   bigCharacter.Play(hit.collider.name);
                    //  bigCharacter.enabled = false;
                    //SetDot(selectNum);
                    spriteID = sprite.GetSpriteIdByName(string.Format("character{0}_h", selectNum));
                    sprite.spriteId = spriteID;
                    selectName = "Hidden";

                    MonsterStatGauge.SetStat(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed);

                    //monsterHpDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].Hp.ToString();
                    monsterHpDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp.ToString();
                    //monsterSpeedDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].speed.ToString();
                    monsterSpeedDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed.ToString();
                    //monsterGroundAttackDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                    monsterGroundAttackDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround.ToString();
                    //monsterAirAttackDataText.text = DataManage.monsterLevelInfo[System.Convert.ToInt32(selectNum)].attackAir.ToString();
                    monsterAirAttackDataText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir.ToString();
                }
                //check.transform.position = hit.collider.transform.position + new Vector3(50, 50, -2);
                monsterName.text = DataManage._monster[System.Convert.ToInt32(selectNum)].monsterName.ToUpper();
                monsterExplanation.text = DataManage._monster[System.Convert.ToInt32(selectNum)].monsterInfo;
                UpgradeSetting.SetActive(true);
            }
        }
    }


    void SoundControl()
    {

        if (Setting.bgmOX)
        {
            HiveBgmObject.GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            HiveBgmObject.GetComponent<AudioSource>().volume = 0;
        }

        if (Setting.soundOX)
        {
            HiveSoundObject.GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            HiveSoundObject.GetComponent<AudioSource>().volume = 0;
        }
    }



    public void Back()
    {

        
           HiveSoundObject.GetComponent<AudioSource>().clip = btn_Select;
           HiveSoundObject.GetComponent<AudioSource>().Play();



        Destroy(InfoMonster);
        typeBg[TypeValue].SetActive(false);
        UpgradeSetting.SetActive(false);
    }

    void GoToLobby()
    {
        //Application.LoadLevel("Lobby");
        SceneManager.LoadScene("Lobby");

    }


   
}
