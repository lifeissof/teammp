﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class NewHive : MonoBehaviour {

    public List<GameObject> typeBg = new List<GameObject>();
    public List<GameObject> gachaList = new List<GameObject>();
    public List<GameObject> stars = new List<GameObject>();


    public List<GameObject> characterSelectBar = new List<GameObject>();
    public GameObject currBar;
    public GameObject basisLeft;
    public GameObject basisRight;
    public GameObject leftButton;
    public GameObject rightButton;


    public GameObject HaveMaterialObject;

    public GameObject fireImage;
    public GameObject waterImage;
    public GameObject woodImage;
    public GameObject fireText;
    public GameObject waterText;
    public GameObject woodText;
    public tk2dTextMesh haveFireMaterialNum;
    public tk2dTextMesh haveWaterMaterialNum;
    public tk2dTextMesh haveWoodMaterialNum;

    bool isSliding;
    bool isPlayReady;

    // 가챠시 이미 있는 몬스터일 경우 증가하는 속성 재료의 값
    int addSourceNum = 10; 


    //몬스터의 뽑기 부분
    public GameObject gachaMonster;
    public GameObject stat;
    public GameObject typeIcon;
    public GameObject overlapObject;
    public GameObject overlapTypeicon;
    public GameObject overlapText;
    public tk2dTextMesh overlapNum;
    public GameObject btnNormal;
    public GameObject btnRare;
    public GameObject fractal;
    public GameObject friendshipPoint;        //서버에서 사용자의 크리스탈과 우정포인트 사용시 주석 해제
    public GameObject standardPoint;
    public tk2dTextMesh monsterName;
    public GameObject ColliderObject;
    public Gauge statGauge;

    public Vector3[] CharacterPos = new Vector3[15]; // X값 중앙으로 맞추기 위한 리스트
    public Vector3[] LevelCharacterPos = new Vector3[15]; // X값 중앙으로 맞추기 위한 리스트
    private int grade;
    private int num;
    private int starCount;
    private int randBg;

    private bool isRare;
    private bool normal, rare;
    private bool normalGacha, rareGacha;
    private bool[] starAlpha = new bool[9] { false, false, false, false, false, false, false, false, false };
    private bool gachaState = false;        //재 뽑기를 위한 bool 변수이다.

	// Use this for initialization
	void Start () {

        isSliding = false;
        isPlayReady = false;

        
        

        /////////////////////////////////////////////////////////////////////////////
        //뽑기부분
        friendshipPoint.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFriendshipPoint()); // 우정포인트 정보 서버에서 가져오기
        fractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFractal());

        if (ServerInterface.Instance.userState.GetFriendshipPoint() >= (int)Point.NORMAL)
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnNomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnNomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnNomal_s";
            normal = true;
        }
        else
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnD_Nomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnD_Nomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnD_Nomal_n";

            normal = false;
        }

        if (ServerInterface.Instance.userState.GetFractal() >= (int)Point.RARE)
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnRare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnRare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnRare_s";

            rare = true;
        }
        else
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnD_Rare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnD_Rare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnD_Rare_n";

            rare = false;
        }
        stat.SetActive(false);

        //////////////////////////////////////////////////////////////////////////////
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Lobby");
        }

        sideButton();
	}

    IEnumerator gacha()
    {
        //GodEyeL.Play("godEye_bottomL");
        //GodEyeR.Play("godEye_bottomR");
        yield return new WaitForSeconds(3.0f);

        setGrade(isRare);     // 확률적으로 등급 결정
        setCharacter(); // 결정된 등급 내에서 랜덤몬스터 선택

        if(DataManage._monster[num].monsterExist == true)
        {
            setName();      // 몬스터 이름
            statGauge.SetStat(ServerInterface.Instance.monsterState_L[num].Hp,
                              ServerInterface.Instance.monsterState_L[num].attackAir,
                              ServerInterface.Instance.monsterState_L[num].attackGround,
                              ServerInterface.Instance.monsterState_L[num].speed);      // 몬스터 능력치

            randBg = ServerInterface.Instance.monsterState_L[num].attribute;

            typeBg[randBg].SetActive(true);
            typeBg[randBg].transform.position = new Vector3(-100, 400, 0);
            tk2dSprite typeId = typeIcon.GetComponent<tk2dSprite>();
            switch (randBg)
            {
                case 0:
                    typeId.spriteId = typeId.GetSpriteIdByName("fireTypeIcon");
                    break;
                case 1:
                    typeId.spriteId = typeId.GetSpriteIdByName("waterTypeIcon");
                    break;
                case 2:
                    typeId.spriteId = typeId.GetSpriteIdByName("woodTypeIcon");
                    break;
            }

            gachaMonster = Instantiate(gachaList[num], standardPoint.transform.position, gachaList[num].transform.rotation) as GameObject;
            if (num == 7 || num == 8 || num == 10 || num == 11 || num == 13 || num == 14)
            {
                gachaMonster.transform.DOScale(new Vector3(1, 1, 1), 1).SetEase(Ease.OutBounce);
                gachaMonster.transform.position = new Vector3(CharacterPos[num].x, CharacterPos[num].y, CharacterPos[num].z);
                typeBg[randBg].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear);
            }
            else
            {
                gachaMonster.transform.DOScale(new Vector3(2, 2, 1), 1).SetEase(Ease.OutBounce);
                gachaMonster.transform.position = new Vector3(CharacterPos[num].x, CharacterPos[num].y, CharacterPos[num].z);
                typeBg[randBg].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear);
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            yield return new WaitForSeconds(2.0f);

            stat.SetActive(false);
            typeIcon.SetActive(false);
            Destroy(gachaMonster);
            typeBg[randBg].SetActive(false);

            tk2dSprite typeId2 = overlapTypeicon.GetComponent<tk2dSprite>();

            switch (randBg)
            {
                case 0:
                    typeId2.spriteId = typeId.GetSpriteIdByName("fireTypeIcon");
                    overlapNum.GetComponent<tk2dTextMesh>().color = new Color( 255f, 132f / 255f, 0f);

                    break;
                case 1:
                    typeId2.spriteId = typeId.GetSpriteIdByName("waterTypeIcon");
                    overlapNum.GetComponent<tk2dTextMesh>().color = new Color(46f / 255f, 174f / 255f, 209f / 255f);
                    break;
                case 2:
                    typeId2.spriteId = typeId.GetSpriteIdByName("woodTypeIcon");
                    overlapNum.GetComponent<tk2dTextMesh>().color = new Color(195f / 255f, 226f / 255f, 93f / 255f);
                    break;
            }


            haveFireMaterialNum.text = "x " + DataManage._srcUpgrade[0].GetEnergy().GetHashCode();
            haveWaterMaterialNum.text = "x " + DataManage._srcUpgrade[1].GetEnergy().GetHashCode();
            haveWoodMaterialNum.text = "x " + DataManage._srcUpgrade[2].GetEnergy().GetHashCode();

            overlapObject.SetActive(true);
            HaveMaterialObject.SetActive(true);
            overlapTypeicon.transform.DOScale(new Vector3(25f, 25f, 25f), 0.2f);
            overlapNum.transform.DOScale(new Vector3(300.0f, 300.0f, 300.0f), 0.2f);

            yield return new WaitForSeconds(0.5f);
            //overlapTypeicon.transform.DOLocalMove(HaveMaterialObject.transform.position, 1.0f, false);

            switch (randBg)
            {
                case 0:
                    overlapTypeicon.transform.DOMove(fireImage.transform.position, 1.5f, false).SetEase(Ease.Linear);
                    overlapText.transform.DOMove(fireText.transform.position, 1.5f, false).SetEase(Ease.Linear);
                    overlapTypeicon.transform.DOScale(new Vector3(10f, 10f, 10f), 2.0f);
                    overlapTypeicon.GetComponent<tk2dSprite>().DOFade(0, 2.5f);
                    overlapNum.transform.DOScale(new Vector3(200.0f, 200.0f, 200.0f), 2.0f);
                    overlapNum.GetComponent<tk2dTextMesh>().DOFade(0, 3.0f);
                    yield return new WaitForSeconds(1.0f);
                    //서버에 10개 불 속성 재료 10개 추가
                    ServerInterface.Instance.AlreadyGacha(num, ServerInterface.Instance.monsterState_L[num].attribute, addSourceNum);
                    // DataManage._srcUpgrade[0].AddEnergy(10);    //로컬에서 10개 추가 (불) 서버에 저장하므로 주석 처리
                    yield return new WaitForSeconds(0.5f);
                    haveFireMaterialNum.text = "x " + DataManage._srcUpgrade[0].GetEnergy().GetHashCode();
                    break;

                case 1:
                    overlapTypeicon.transform.DOMove(waterImage.transform.position, 1.5f, false).SetEase(Ease.Linear);
                    overlapText.transform.DOMove(waterText.transform.position, 1.5f, false).SetEase(Ease.Linear);
                    overlapTypeicon.transform.DOScale(new Vector3(10f, 10f, 10f), 2.0f);
                    overlapTypeicon.GetComponent<tk2dSprite>().DOFade(0, 2.5f);
                    overlapNum.transform.DOScale(new Vector3(200.0f, 200.0f, 200.0f), 2.0f);
                    overlapNum.GetComponent<tk2dTextMesh>().DOFade(0, 3.0f);
                    yield return new WaitForSeconds(1.0f);
                    //서버에 10개 불 속성 재료 10개 추가
                    ServerInterface.Instance.AlreadyGacha(num, ServerInterface.Instance.monsterState_L[num].attribute, addSourceNum);
                    //DataManage._srcUpgrade[1].AddEnergy(10);    //로컬에서 10개 추가 (불) 서버에 저장하므로 주석 처리
                    yield return new WaitForSeconds(0.5f);
                    haveWaterMaterialNum.text = "x " + DataManage._srcUpgrade[1].GetEnergy().GetHashCode();
                    break;

                case 2:
                    overlapTypeicon.transform.DOMove(woodImage.transform.position, 1.5f, false).SetEase(Ease.Linear);
                    overlapText.transform.DOMove(woodText.transform.position, 1.5f, false).SetEase(Ease.Linear);
                    overlapTypeicon.transform.DOScale(new Vector3(10f, 10f, 10f), 2.0f);
                    overlapTypeicon.GetComponent<tk2dSprite>().DOFade(0, 2.5f);
                    overlapNum.transform.DOScale(new Vector3(200.0f, 200.0f, 200.0f), 2.0f);
                    overlapNum.GetComponent<tk2dTextMesh>().DOFade(0, 3.0f);
                    yield return new WaitForSeconds(1.0f);
                    //서버에 10개 불 속성 재료 10개 추가
                    ServerInterface.Instance.AlreadyGacha(num, ServerInterface.Instance.monsterState_L[num].attribute, addSourceNum);
                    //DataManage._srcUpgrade[2].AddEnergy(10);    //로컬에서 10개 추가 (불) 서버에 저장하므로 주석 처리
                    yield return new WaitForSeconds(0.5f);
                    haveWoodMaterialNum.text = "x " + DataManage._srcUpgrade[2].GetEnergy().GetHashCode();
                    break;
            }

            //overlapTypeicon.transform.DOScale(new Vector3(10f, 10f, 10f), 0.5f);
            //overlapTypeicon.GetComponent<tk2dSprite>().DOFade(0, 0.5f);
            //overlapNum.transform.DOScale(new Vector3(200.0f, 200.0f, 200.0f), 0.5f);
            //overlapNum.GetComponent<tk2dTextMesh>().DOFade(0, 0.5f);
            HaveMaterialObject.SetActive(false);
            overlapObject.SetActive(false);
   


        }
        else
        {
            setName();      // 몬스터 이름
            statGauge.SetStat(ServerInterface.Instance.monsterState_L[num].Hp,
                              ServerInterface.Instance.monsterState_L[num].attackAir,
                              ServerInterface.Instance.monsterState_L[num].attackGround,
                              ServerInterface.Instance.monsterState_L[num].speed);      // 몬스터 능력치


            ServerInterface.Instance.GachaMonster(num);
            //StartCoroutine("twinkleStar");

            randBg = ServerInterface.Instance.monsterState_L[num].attribute; // 속성을 서버에서 받아서 사용함 16 07 26
            //randBg = DataManage._monster[num].monsterAttribute; // 속성은 서버 데이터 받아서 사용함 일단 임시적으로 0불 1물 2풀
            typeBg[randBg].SetActive(true);
            typeBg[randBg].transform.position = new Vector3(-100, 400, 0);
            tk2dSprite typeId = typeIcon.GetComponent<tk2dSprite>();
            switch (randBg)
            {
                case 0:
                    typeId.spriteId = typeId.GetSpriteIdByName("fireTypeIcon");
                    break;
                case 1:
                    typeId.spriteId = typeId.GetSpriteIdByName("waterTypeIcon");
                    break;
                case 2:
                    typeId.spriteId = typeId.GetSpriteIdByName("woodTypeIcon");
                    break;
            }
            //Vector3 tempVector = new Vector3((gachaList[num].transform.position.x + posX[num]) * NScreen.ScaleNum, ServerInterface.Instance.monsterState_P[num].posY * NScreen.ScaleNum, gachaList[num].transform.position.z);
            //Vector3 tempVector = new Vector3(200, 200, 200);
            gachaMonster = Instantiate(gachaList[num], standardPoint.transform.position, gachaList[num].transform.rotation) as GameObject;
            print(gachaList[num]);
            if (num == 7 || num == 8 || num == 10 || num == 11 || num == 13 || num == 14)
            {
                gachaMonster.transform.DOScale(new Vector3(1, 1, 1), 1).SetEase(Ease.OutBounce).SetDelay(1.0f);
                gachaMonster.transform.position = new Vector3(CharacterPos[num].x, CharacterPos[num].y, CharacterPos[num].z);
                typeBg[randBg].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear).SetDelay(1.0f);
            }
            else
            {
                gachaMonster.transform.DOScale(new Vector3(2, 2, 1), 1).SetEase(Ease.OutBounce).SetDelay(1.0f);
                gachaMonster.transform.position = new Vector3(CharacterPos[num].x, CharacterPos[num].y, CharacterPos[num].z);
                typeBg[randBg].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear).SetDelay(1.0f);
            }
            
            
        }



        if (normalGacha)
        {
            //DataManage.friendshipPoint -= (int)Point.NORMAL;
            //ServerInterface.Instance.userState.SetFriendshipPoint(ServerInterface.Instance.userState.GetFriendshipPoint() - (int)Point.NORMAL);
            //ServerInterface.Instance.userState.GetFriendshipPoint() -= (int)Point.NORMAL;
            //ServerInterface.Instance.UpdateHive("friendshipPoint");

            int _friendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();
            //ServerInterface.Instance.userState.GetFriendshipPoint() = _friendshipPoint;
            //int _friendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();

            friendshipPoint.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(_friendshipPoint);

            //if (Tutorial.stepNumber == 4) ServerInterface.Instance.UpdateFriendshipPointData(_friendshipPoint, 5);
            //else if (Tutorial.stepNumber == -1)
            //{
            //    ServerInterface.Instance.UpdateFriendshipPointData(_friendshipPoint, -1);
            //}

        }
        if (rareGacha)
        {
            // DataManage.fractal -= (int)Point.RARE;
            //ServerInterface.Instance.userState.SetFractal(ServerInterface.Instance.userState.GetFractal() - (int)Point.RARE);
            //ServerInterface.Instance.userState.GetFractal() -= (int)Point.RARE;
            //ServerInterface.Instance.UpdateHive("fractal");
            int _fractal = ServerInterface.Instance.userState.GetFractal();
            //int _fractal = ServerInterface.Instance.userState.GetFractal();
            //ServerInterface.Instance.userState.GetFractal() = _fractal;
            fractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(_fractal);

            //if (Tutorial.stepNumber == 6) ServerInterface.Instance.UpdateFractalData(_fractal, 7);
            //else if (Tutorial.stepNumber == -1)
            //{
            //    ServerInterface.Instance.UpdateFractalData(_fractal, -1);
            //}
        }

        yield return new WaitForSeconds(1.0f);
        ButtonInit();
        gachaState = true;

        

        //setGrade(isRare);     // 확률적으로 등급 결정
        //setCharacter(); // 결정된 등급 내에서 랜덤몬스터 선택
        //setName();      // 몬스터 이름
        //statGauge.SetStat(ServerInterface.Instance.monsterState_L[num].Hp,        
        //                  ServerInterface.Instance.monsterState_L[num].attackAir,
        //                  ServerInterface.Instance.monsterState_L[num].attackGround,
        //                  ServerInterface.Instance.monsterState_L[num].speed);      // 몬스터 능력치


        //ServerInterface.Instance.GachaMonster(num);
        ////StartCoroutine("twinkleStar");

        //randBg = ServerInterface.Instance.monsterState_L[num].attribute; // 속성을 서버에서 받아서 사용함 16 07 26
        ////randBg = DataManage._monster[num].monsterAttribute; // 속성은 서버 데이터 받아서 사용함 일단 임시적으로 0불 1물 2풀
        //typeBg[randBg].SetActive(true);
        //typeBg[randBg].transform.position = new Vector3(-100, 400, 0);
        //tk2dSprite typeId = typeIcon.GetComponent<tk2dSprite>();
        //switch (randBg)
        //{
        //    case 0:
        //        typeId.spriteId = typeId.GetSpriteIdByName("fireTypeIcon");
        //        break;
        //    case 1:
        //        typeId.spriteId = typeId.GetSpriteIdByName("waterTypeIcon");
        //        break;
        //    case 2:
        //        typeId.spriteId = typeId.GetSpriteIdByName("woodTypeIcon");
        //        break;
        //}
        ////Vector3 tempVector = new Vector3((gachaList[num].transform.position.x + posX[num]) * NScreen.ScaleNum, ServerInterface.Instance.monsterState_P[num].posY * NScreen.ScaleNum, gachaList[num].transform.position.z);
        ////Vector3 tempVector = new Vector3(200, 200, 200);
        //gachaMonster = Instantiate(gachaList[num], standardPoint.transform.position, gachaList[num].transform.rotation) as GameObject;
        //if (num == 7 || num == 8 || num == 10 || num == 11 || num == 13 || num == 14)
        //{
        //    gachaMonster.transform.DOScale(new Vector3(1, 1, 1), 1).SetEase(Ease.OutBounce).SetDelay(1.0f);
        //    gachaMonster.transform.position = new Vector3(CharacterPos[num].x, CharacterPos[num].y, CharacterPos[num].z);
        //    typeBg[randBg].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear).SetDelay(1.0f);
        //}
        //else
        //{
        //    gachaMonster.transform.DOScale(new Vector3(2, 2, 1), 1).SetEase(Ease.OutBounce).SetDelay(1.0f);
        //    gachaMonster.transform.position = new Vector3(CharacterPos[num].x, CharacterPos[num].y, CharacterPos[num].z);
        //    typeBg[randBg].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear).SetDelay(1.0f);
        //}
        ////Destroy(gachaMonster, 10.0f);           //뽑은 후 10초 후 보여주지않음.


        ////GodEyeL.Play("godEye_topL");
        ////GodEyeR.Play("godEye_topR");




        //if (normalGacha)
        //{
        //    //DataManage.friendshipPoint -= (int)Point.NORMAL;
        //    //ServerInterface.Instance.userState.SetFriendshipPoint(ServerInterface.Instance.userState.GetFriendshipPoint() - (int)Point.NORMAL);
        //    //ServerInterface.Instance.userState.GetFriendshipPoint() -= (int)Point.NORMAL;
        //    //ServerInterface.Instance.UpdateHive("friendshipPoint");

        //    int _friendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();
        //    //ServerInterface.Instance.userState.GetFriendshipPoint() = _friendshipPoint;
        //    //int _friendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();

        //    friendshipPoint.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(_friendshipPoint);

        //    //if (Tutorial.stepNumber == 4) ServerInterface.Instance.UpdateFriendshipPointData(_friendshipPoint, 5);
        //    //else if (Tutorial.stepNumber == -1)
        //    //{
        //    //    ServerInterface.Instance.UpdateFriendshipPointData(_friendshipPoint, -1);
        //    //}

        //}
        //if (rareGacha)
        //{
        //    // DataManage.fractal -= (int)Point.RARE;
        //    //ServerInterface.Instance.userState.SetFractal(ServerInterface.Instance.userState.GetFractal() - (int)Point.RARE);
        //    //ServerInterface.Instance.userState.GetFractal() -= (int)Point.RARE;
        //    //ServerInterface.Instance.UpdateHive("fractal");
        //    int _fractal = ServerInterface.Instance.userState.GetFractal();
        //    //int _fractal = ServerInterface.Instance.userState.GetFractal();
        //    //ServerInterface.Instance.userState.GetFractal() = _fractal;
        //    fractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(_fractal);

        //    //if (Tutorial.stepNumber == 6) ServerInterface.Instance.UpdateFractalData(_fractal, 7);
        //    //else if (Tutorial.stepNumber == -1)
        //    //{
        //    //    ServerInterface.Instance.UpdateFractalData(_fractal, -1);
        //    //}
        //}

        //gachaState = true;
    }


    void ButtonInit()
    {

        if (ServerInterface.Instance.userState.GetFriendshipPoint() >= (int)Point.NORMAL)
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnNomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnNomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnNomal_s";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnNormal.GetComponent<Collider>().enabled = true;
            //    btnNormal.GetComponent<ButtonSystem>().enabled = true;
            //}
            normal = true;
        }
        else
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnD_Nomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnD_Nomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnD_Nomal_n";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnNormal.GetComponent<Collider>().enabled = false;
            //    btnNormal.GetComponent<ButtonSystem>().enabled = false;
            //}
            normal = false;
        }

        if (ServerInterface.Instance.userState.GetFractal() >= (int)Point.RARE)
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnRare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnRare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnRare_s";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnRare.GetComponent<Collider>().enabled = true;
            //    btnRare.GetComponent<ButtonSystem>().enabled = true;
            //}
            rare = true;
        }
        else
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnD_Rare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnD_Rare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnD_Rare_n";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnRare.GetComponent<Collider>().enabled = false;
            //    btnRare.GetComponent<ButtonSystem>().enabled = false;
            //}
            rare = false;
        }

        btnRare.gameObject.SetActive(true);
        btnNormal.gameObject.SetActive(true);
        //if (Tutorial.stepNumber == 4 || Tutorial.stepNumber == 6) OnShowStat();
    }
    void showStat()
    {
        // 서버에서 해당 몬스터의 별 갯수 정보를 불러옴 이걸 각각 스텟창이랑 기본 화면에 적용하는게 필요함
        starCount = ServerInterface.Instance.monsterState_L[num].gradeStar;

        for (int i = 0; i < (int)Star.MAX; ++i)
        {
            if (starCount > i) stars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
            else stars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
        }


        stat.SetActive(true);
        typeIcon.SetActive(true);


        if (ServerInterface.Instance.userState.GetFriendshipPoint() >= (int)Point.NORMAL)
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnNomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnNomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnNomal_s";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnNormal.GetComponent<Collider>().enabled = true;
            //    btnNormal.GetComponent<ButtonSystem>().enabled = true;
            //}
            normal = true;
        }
        else
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnD_Nomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnD_Nomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnD_Nomal_n";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnNormal.GetComponent<Collider>().enabled = false;
            //    btnNormal.GetComponent<ButtonSystem>().enabled = false;
            //}
            normal = false;
        }

        if (ServerInterface.Instance.userState.GetFractal() >= (int)Point.RARE)
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnRare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnRare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnRare_s";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnRare.GetComponent<Collider>().enabled = true;
            //    btnRare.GetComponent<ButtonSystem>().enabled = true;
            //}
            rare = true;
        }
        else
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnD_Rare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnD_Rare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnD_Rare_n";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnRare.GetComponent<Collider>().enabled = false;
            //    btnRare.GetComponent<ButtonSystem>().enabled = false;
            //}
            rare = false;
        }

       
        //if (Tutorial.stepNumber == 4 || Tutorial.stepNumber == 6) OnShowStat();


    }

    void setGrade(bool _isRare)
    {
        //if (Tutorial.stepNumber == 4)
        //{
        //    grade = 1;
        //}
        //else
        //{
        int rand = Random.Range(0, 101);
        if (_isRare)
        {
            if (rand <= 5)
                grade = 5;
            else if (rand <= 35)
                grade = 4;
            else
                grade = 3;
        }
        else
        {
            if (rand <= 5)
                grade = 3;
            else if (rand <= 35)
                grade = 2;
            else
                grade = 1;
            //}
        }
    }

    void setName()
    {

        monsterName.text = DataManage._monster[num].monsterName.ToUpper();
        monsterName.enabled = false;
    }

    void setCharacter()
    {
        if (grade == 5)
        {
            num = Random.Range(13, 15);
            starCount = 6;
        }
        if (grade == 4)
        {
            num = Random.Range(10, 13);
            starCount = 4;
        }
        if (grade == 3)
        {
            num = Random.Range(6, 10);
            starCount = 3;
        }
        if (grade == 2)
        {
            num = Random.Range(3, 6);
            starCount = 2;
        }
        if (grade == 1)
        {
            //if (Tutorial.stepNumber == 4) num = 1;
            num = Random.Range(0, 3);
            starCount = 1;
        }



        //DataManage._monster[num].monsterExist = true;
        //ServerInterface.Instance.UpdateMonsterData(num, 1, 0);

        ////임시코드 모든 몬스터 보유하기
        //if (Tutorial.stepNumber == 6)
        //{
        //    for (int i = 2; i < 15; i++)
        //    {
        //        DataManage._monster[i].level = 1;
        //    }
        //    ServerInterface.Instance.MonsterDataForTurorial();
        //}
        /////
    }

    void selectNormal()
    {
        
            if (normal == true)
            {
               
                if (gachaState == true) EraseGhacha();
                btnNormal.gameObject.SetActive(false);
                btnRare.gameObject.SetActive(false);

                normalGacha = true;
                rareGacha = false;
                isRare = false;
                ServerInterface.Instance.UpdateHive("friendshipPoint");
                StartCoroutine("gacha");
                
                //if (Tutorial.stepNumber == 4) OnNormalTouch();
            }
        
        
    }

    void selectRare()
    {
        
            if (rare == true)
            {
                if (gachaState == true) EraseGhacha();
                btnNormal.gameObject.SetActive(false);
                btnRare.gameObject.SetActive(false);

                normalGacha = false;
                rareGacha = true;
                isRare = true;
                ServerInterface.Instance.UpdateHive("fractal");
                StartCoroutine("gacha");
                //if (Tutorial.stepNumber == 6) OnRareTouch();
            }
        
    }

    void EraseGhacha()
    {


        Destroy(gachaMonster);
        typeBg[randBg].SetActive(false);
        stat.SetActive(false);
        typeIcon.SetActive(false);
        
       

        overlapTypeicon.transform.localPosition = new Vector3(0, 0, 0);
        overlapTypeicon.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        overlapTypeicon.GetComponent<tk2dSprite>().color = new Color(1, 1, 1 ,1);

        overlapNum.transform.localPosition = new Vector3(78, -70, 0);
        overlapNum.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        overlapNum.GetComponent<tk2dTextMesh>().color = new Color(1, 1, 1, 1);
        monsterName.enabled = true;
        gachaState = false;
    }

    void hideStarEffect()
    {
        StartCoroutine("waitSeconds");

        //foreach (GameObject star in starEffect)
        //{
        //    star.gameObject.SetActive(false);
        //}
    }

    IEnumerator waitSeconds()
    {
        yield return new WaitForSeconds(1.5f);
    }

    void GoToLobby()
    {
        //Application.LoadLevel("Lobby");
        SceneManager.LoadScene("Lobby");

    }

    void btnTouchLeft()
    {
        if (isSliding == false)
        {
            isSliding = true;

            //GetComponent<AudioSource>().clip = btn_arrow;
            //GetComponent<AudioSource>().Play();


            if (currBar == characterSelectBar[0])
            {
                currBar.transform.DOMoveX(-835, 0.5f, false);
                characterSelectBar[1].transform.DOMoveX(-35, 0.5f, false);
                characterSelectBar[2].transform.DOMoveX(765, 0.5f, false);
                characterSelectBar[3].transform.DOMoveX(1565, 0.5f, false);

                currBar = characterSelectBar[1];
            }

            else if (currBar == characterSelectBar[1])
            {

                currBar.transform.DOMoveX(-835, 0.5f, false);
                characterSelectBar[0].transform.DOMoveX(-1635, 0.5f, false);
                characterSelectBar[2].transform.DOMoveX(-35, 0.5f, false);
                characterSelectBar[3].transform.DOMoveX(765, 0.5f, false);

                currBar = characterSelectBar[2];
            }

            else if (currBar == characterSelectBar[2])
            {

                currBar.transform.DOMoveX(-835, 0.5f, false);
                characterSelectBar[0].transform.DOMoveX(basisLeft.transform.position.x, 0.5f, false);
                characterSelectBar[1].transform.DOMoveX(-1635, 0.5f, false);
                characterSelectBar[3].transform.DOMoveX(-35, 0.5f, false);

                currBar = characterSelectBar[3];
            }

            else if (currBar == characterSelectBar[3])
            {


                isSliding = false;
                currBar = characterSelectBar[3];
            }
            slidingFlag();
        }

    }

    void btnTouchRight()
    {
        if (isSliding == false)
        {
            isSliding = true;

            //GetComponent<AudioSource>().clip = btn_arrow;
            //GetComponent<AudioSource>().Play();
            if (currBar == characterSelectBar[3])
            {
                currBar.transform.DOMoveX(765, 0.5f, false);
                characterSelectBar[0].transform.DOMoveX(-1635, 0.5f, false);
                characterSelectBar[1].transform.DOMoveX(-835, 0.5f, false);
                characterSelectBar[2].transform.DOMoveX(-35, 0.5f, false);

                currBar = characterSelectBar[2];

            }


            else if (currBar == characterSelectBar[2])
            {
                currBar.transform.DOMoveX(765, 0.5f, false);
                characterSelectBar[0].transform.DOMoveX(-835, 0.5f, false);
                characterSelectBar[1].transform.DOMoveX(-35, 0.5f, false);
                characterSelectBar[3].transform.DOMoveX(1565, 0.5f, false);

                currBar = characterSelectBar[1];

            }
            else if (currBar == characterSelectBar[1])
            {
                currBar.transform.DOMoveX(765, 0.5f, false);
                characterSelectBar[0].transform.DOMoveX(-35, 0.5f, false);
                characterSelectBar[2].transform.DOMoveX(1565, 0.5f, false);
                characterSelectBar[3].transform.DOMoveX(basisRight.transform.position.x, 0.5f, false);

                currBar = characterSelectBar[0];

            }
            else if (currBar == characterSelectBar[0])
            {
                isSliding = false;
                currBar = characterSelectBar[0];
            }
            slidingFlag();
        }

    }
    void sideButton()
    {
        if (currBar == characterSelectBar[0])
        {
            leftButton.SetActive(false);
        }
        else if (currBar == characterSelectBar[3])
        {
            rightButton.SetActive(false);
        }
        else
        {
            rightButton.SetActive(true);
            leftButton.SetActive(true);
        }
    }

    void slidingFlag()
    {
        isSliding = false;
    }

   
}
