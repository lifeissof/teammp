﻿using UnityEngine;
using System.Collections;

public class UpgradeBtn : MonoBehaviour {

    Ray ray;
    RaycastHit hit;
    public string Function;


    public GameObject Target;
    private string clickCharacterNum;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == this.gameObject)
            {
                Target.SendMessage(Function);
            }

        }
    }
}
