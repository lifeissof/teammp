﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.SceneManagement;


enum Point
{
    NORMAL = 20,
    RARE = 20
}

enum Star
{
    MAX = 6
}
public class ChamberManage : MonoBehaviour {


    public tk2dSpriteAnimator GodEyeL;
    public tk2dSpriteAnimator GodEyeR;

    public List<GameObject> typeBg = new List<GameObject>();
    public List<GameObject> gachaList = new List<GameObject>();
    public List<GameObject> stars = new List<GameObject>();
    public List<GameObject> starEffect = new List<GameObject>();

    public GameObject gachaMonster;
    public GameObject stat;
    public GameObject typeIcon;
    public GameObject btnNormal;
    public GameObject btnRare;
    public GameObject fractal;
    public GameObject friendshipPoint;        //서버에서 사용자의 크리스탈과 우정포인트 사용시 주석 해제
    public GameObject standardPoint;
    public tk2dTextMesh monsterName;
    //public Gauge statGauge;       //스탯 서버에서 받아서 처리

    //private int UserTestFractalPoint = ServerInterface.Instance.userState.GetFractal();
    //private int UserTestFriendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();

    public Vector3[] CharacterPos  = new Vector3[15]; // X값 중앙으로 맞추기 위한 리스트
    private int grade;
    private int gachaMonsternum;
    private int starCount;
    private int MonsterAttributeNum;

    private bool isRare;
    private bool normal, rare;
    private bool normalGacha, rareGacha;
    private bool[] starAlpha = new bool[9] { false, false, false, false, false, false, false, false, false };
    private bool gachaState = false;        //재 뽑기를 위한 bool 변수이다.



	// Use this for initialization
	void Start () {

     


        foreach (GameObject star in starEffect)
        {
            star.gameObject.SetActive(false);
        }

        
        friendshipPoint.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFriendshipPoint()); // 우정포인트 정보 서버에서 가져오기
        fractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFractal());

        if (ServerInterface.Instance.userState.GetFriendshipPoint() >= (int)Point.NORMAL)
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnNomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnNomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnNomal_s";
            normal = true;
        }
        else
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnD_Nomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnD_Nomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnD_Nomal_n";
         
            normal = false;
        }

        if (ServerInterface.Instance.userState.GetFractal() >= (int)Point.RARE)
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnRare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnRare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnRare_s";
        
            rare = true;
        }
        else
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnD_Rare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnD_Rare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnD_Rare_n";
   
            rare = false;
        }
        stat.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Lobby");
        }
	
	}

    IEnumerator twinkleStar()       //별 이펙트
    {
        for (int i = 0; i < starEffect.Count; ++i)
        {
            if (starAlpha[i] == false)
            {
                starEffect[i].GetComponent<tk2dSprite>().color = new Color(1, 1, 1, (float)Random.Range(0, 100) / 255);
                starAlpha[i] = true;
            }
            else
            {
                starEffect[i].GetComponent<tk2dSprite>().color = new Color(1, 1, 1, (float)Random.Range(150, 255) / 255);
                starAlpha[i] = false;
            }

            yield return new WaitForSeconds(0.008f);
        }

        yield return new WaitForSeconds(0.06f);

        if (starEffect[0].gameObject.activeSelf == true)
        {
            StartCoroutine("twinkleStar");
        }
    }


    //void createMonster()
    //{
    //    if (Ball.GetComponent<tk2dSpriteAnimator>().CurrentClip.name != "ball_s")
    //    {
    //        Destroy(gachaMonster);
    //        typeBg[randBg].SetActive(false);
    //        typeBg[randBg].transform.position = new Vector3(0, 30, 0);
    //        stat.SetActive(false);
    //        for (int i = 0; i < 7; ++i)
    //        {
    //            stars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
    //        }
    //        StartCoroutine("gacha");
    //    }
    //}

    IEnumerator gacha()
    {
   
       
        GodEyeL.Play("godEye_bottomL");
        GodEyeR.Play("godEye_bottomR");

        yield return new WaitForSeconds(3.0f);

        setGrade(isRare);     // 확률적으로 등급 결정
        setCharacter(); // 결정된 등급 내에서 랜덤몬스터 선택
        setName();      // 몬스터 이름
        //statGauge.SetStat(ServerInterface.Instance.monsterState_L[num].Hp,        서버부분 제외
        //                  ServerInterface.Instance.monsterState_L[num].attackAir,
        //                  ServerInterface.Instance.monsterState_L[num].attackGround,
        //                  ServerInterface.Instance.monsterState_L[num].speed);      // 몬스터 능력치

        foreach (GameObject star in starEffect)
        {
            star.gameObject.SetActive(true);
        }

        StartCoroutine("twinkleStar");



        MonsterAttributeNum = DataManage._monster[gachaMonsternum].monsterAttribute;// 속성은 서버 데이터 받아서 사용함 일단 임시적으로 0불 1물 2풀
        typeBg[MonsterAttributeNum].SetActive(true);
        typeBg[MonsterAttributeNum].transform.position = new Vector3(-100, 30, 0);
        tk2dSprite typeId = typeIcon.GetComponent<tk2dSprite>();
        switch (MonsterAttributeNum)
        {
            case 0:
                typeId.spriteId = typeId.GetSpriteIdByName("fireTypeIcon");
                break;
            case 1:
                typeId.spriteId = typeId.GetSpriteIdByName("waterTypeIcon");
                break;
            case 2:
                typeId.spriteId = typeId.GetSpriteIdByName("woodTypeIcon");
                break;
        }
        print(gachaMonsternum);
        print(MonsterAttributeNum);
        //Vector3 tempVector = new Vector3((gachaList[num].transform.position.x + posX[num]) * NScreen.ScaleNum, ServerInterface.Instance.monsterState_P[num].posY * NScreen.ScaleNum, gachaList[num].transform.position.z);
        //Vector3 tempVector = new Vector3(200, 200, 200);
        gachaMonster = Instantiate(gachaList[gachaMonsternum], standardPoint.transform.position, gachaList[gachaMonsternum].transform.rotation) as GameObject;
        if (gachaMonsternum == 7 || gachaMonsternum == 8 || gachaMonsternum == 10 || gachaMonsternum == 11 || gachaMonsternum == 13 || gachaMonsternum == 14)
        {
            gachaMonster.transform.DOScale(new Vector3(1, 1, 1), 1).SetEase(Ease.OutBounce).SetDelay(1.0f);
            gachaMonster.transform.position = new Vector3(CharacterPos[gachaMonsternum].x, CharacterPos[gachaMonsternum].y, CharacterPos[gachaMonsternum].z);
            typeBg[MonsterAttributeNum].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear).SetDelay(1.0f);
        }
        else
        {
            gachaMonster.transform.DOScale(new Vector3(2, 2, 1), 1).SetEase(Ease.OutBounce).SetDelay(1.0f);
            gachaMonster.transform.position = new Vector3(CharacterPos[gachaMonsternum].x, CharacterPos[gachaMonsternum].y, CharacterPos[gachaMonsternum].z);
            typeBg[MonsterAttributeNum].transform.DOScale(new Vector3(1, 1, 1), 1).OnStart(hideStarEffect).OnComplete(showStat).SetEase(Ease.Linear).SetDelay(1.0f); 
        }
        //Destroy(gachaMonster, 10.0f);           //뽑은 후 10초 후 보여주지않음.


        GodEyeL.Play("godEye_topL");
        GodEyeR.Play("godEye_topR");

      
     

        if (normalGacha)
        {
            //DataManage.friendshipPoint -= (int)Point.NORMAL;
            //ServerInterface.Instance.userState.SetFriendshipPoint(ServerInterface.Instance.userState.GetFriendshipPoint() - (int)Point.NORMAL);
            //ServerInterface.Instance.userState.GetFriendshipPoint() -= (int)Point.NORMAL;
            //ServerInterface.Instance.UpdateHive("friendshipPoint");
            
            int _friendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();
            //ServerInterface.Instance.userState.GetFriendshipPoint() = _friendshipPoint;
            //int _friendshipPoint = ServerInterface.Instance.userState.GetFriendshipPoint();

            friendshipPoint.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(_friendshipPoint); 
            
            //if (Tutorial.stepNumber == 4) ServerInterface.Instance.UpdateFriendshipPointData(_friendshipPoint, 5);
            //else if (Tutorial.stepNumber == -1)
            //{
            //    ServerInterface.Instance.UpdateFriendshipPointData(_friendshipPoint, -1);
            //}

        }
        if (rareGacha)
        {
            // DataManage.fractal -= (int)Point.RARE;
            //ServerInterface.Instance.userState.SetFractal(ServerInterface.Instance.userState.GetFractal() - (int)Point.RARE);
            //ServerInterface.Instance.userState.GetFractal() -= (int)Point.RARE;
            //ServerInterface.Instance.UpdateHive("fractal");
            int _fractal = ServerInterface.Instance.userState.GetFractal();
            //int _fractal = ServerInterface.Instance.userState.GetFractal();
            //ServerInterface.Instance.userState.GetFractal() = _fractal;
            fractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(_fractal);

            //if (Tutorial.stepNumber == 6) ServerInterface.Instance.UpdateFractalData(_fractal, 7);
            //else if (Tutorial.stepNumber == -1)
            //{
            //    ServerInterface.Instance.UpdateFractalData(_fractal, -1);
            //}
        }

        gachaState = true;
    }


    void showStat()
    {
        for (int i = 0; i < (int)Star.MAX; ++i) 
        {
            if (starCount > i) stars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
            else stars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
        }


        stat.SetActive(true);
        typeIcon.SetActive(true);
       

        if (ServerInterface.Instance.userState.GetFriendshipPoint()>= (int)Point.NORMAL)
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnNomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnNomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnNomal_s";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnNormal.GetComponent<Collider>().enabled = true;
            //    btnNormal.GetComponent<ButtonSystem>().enabled = true;
            //}
            normal = true;
        }
        else
        {
            btnNormal.GetComponent<tk2dSprite>().SetSprite("btnD_Nomal_n");
            btnNormal.GetComponent<ButtonSystem>().UpSprite = "btnD_Nomal_n";
            btnNormal.GetComponent<ButtonSystem>().DownSprite = "btnD_Nomal_n";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnNormal.GetComponent<Collider>().enabled = false;
            //    btnNormal.GetComponent<ButtonSystem>().enabled = false;
            //}
            normal = false;
        }

        if (ServerInterface.Instance.userState.GetFractal() >= (int)Point.RARE)
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnRare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnRare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnRare_s";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnRare.GetComponent<Collider>().enabled = true;
            //    btnRare.GetComponent<ButtonSystem>().enabled = true;
            //}
            rare = true;
        }
        else
        {
            btnRare.GetComponent<tk2dSprite>().SetSprite("btnD_Rare_n");
            btnRare.GetComponent<ButtonSystem>().UpSprite = "btnD_Rare_n";
            btnRare.GetComponent<ButtonSystem>().DownSprite = "btnD_Rare_n";
            //if (Tutorial.stepNumber == -1)
            //{
            //    btnRare.GetComponent<Collider>().enabled = false;
            //    btnRare.GetComponent<ButtonSystem>().enabled = false;
            //}
            rare = false;
        }

        btnRare.gameObject.SetActive(true);
        btnNormal.gameObject.SetActive(true);
        //if (Tutorial.stepNumber == 4 || Tutorial.stepNumber == 6) OnShowStat();


    }

    void setGrade(bool _isRare)
    {
        //if (Tutorial.stepNumber == 4)
        //{
        //    grade = 1;
        //}
        //else
        //{
            int rand = Random.Range(0, 101);
            if (_isRare)
            {
                if (rand <= 5)
                    grade = 5;
                else if (rand <= 35)
                    grade = 4;
                else
                    grade = 3;
            }
            else
            {
                if (rand <= 5)
                    grade = 3;
                else if (rand <= 35)
                    grade = 2;
                else
                    grade = 1;
            //}
        }
    }

    void setName()
    {

        monsterName.text = DataManage._monster[gachaMonsternum].monsterName.ToUpper();
        monsterName.enabled = false;
    }

    void setCharacter()
    {
        if (grade == 5)
        {
            gachaMonsternum = Random.Range(13, 15);
            starCount = 6;
        }
        if (grade == 4)
        {
            gachaMonsternum = Random.Range(10, 13);
            starCount = 4;
        }
        if (grade == 3)
        {
            gachaMonsternum = Random.Range(6, 10);
            starCount = 3;
        }
        if (grade == 2)
        {
            gachaMonsternum = Random.Range(3, 6);
            starCount = 2;
        }
        if (grade == 1)
        {
            //if (Tutorial.stepNumber == 4) num = 1;
            gachaMonsternum = Random.Range(0, 3);
            starCount = 1;
        }


      
        //DataManage._monster[num].monsterExist = true;
        //ServerInterface.Instance.UpdateMonsterData(num, 1, 0);

        ////임시코드 모든 몬스터 보유하기
        //if (Tutorial.stepNumber == 6)
        //{
        //    for (int i = 2; i < 15; i++)
        //    {
        //        DataManage._monster[i].level = 1;
        //    }
        //    ServerInterface.Instance.MonsterDataForTurorial();
        //}
        /////
    }

    void selectNormal()
    {
        if (normal == true)
        {
            if (gachaState == true) EraseGhacha();
            btnNormal.gameObject.SetActive(false);
            btnRare.gameObject.SetActive(false);

            normalGacha = true;
            rareGacha = false;
            isRare = false;
            //ServerInterface.Instance.UpdateHive("friendshipPoint");
            StartCoroutine("gacha");
            //if (Tutorial.stepNumber == 4) OnNormalTouch();
        }
    }

    void selectRare()
    {
        if (rare == true)
        {
            if (gachaState == true) EraseGhacha();
            btnNormal.gameObject.SetActive(false);
            btnRare.gameObject.SetActive(false);
        
            normalGacha = false;
            rareGacha = true;
            isRare = true;
           // ServerInterface.Instance.UpdateHive("fractal");
            StartCoroutine("gacha");
            //if (Tutorial.stepNumber == 6) OnRareTouch();
        }
    }

    void EraseGhacha()
    {
        Destroy(gachaMonster);
        typeBg[MonsterAttributeNum].SetActive(false);
        stat.SetActive(false);
        typeIcon.SetActive(false);
        monsterName.enabled = true;
        gachaState = false;
    }

    void hideStarEffect()
    {
        StartCoroutine("waitSeconds");

        foreach (GameObject star in starEffect)
        {
            star.gameObject.SetActive(false);
        }
    }

    IEnumerator waitSeconds()
    {
        yield return new WaitForSeconds(1.5f);
    }

    void GoToLobby()
    {
        //Application.LoadLevel("Lobby");
        SceneManager.LoadScene("Lobby");

    }
}
