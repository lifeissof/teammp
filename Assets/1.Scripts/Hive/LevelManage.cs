﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManage : MonoBehaviour {


    public List<GameObject> Monsters = new List<GameObject>(15);
    public NewHive NewHiveObject = new NewHive();
    
    public GameObject LevelUpSetting;
    public GameObject LevelUpSelectCharacter;
    public GameObject InstantiatePoint;
   

    public tk2dTextMesh monsterName;
    public tk2dTextMesh monsterLevel;
    public tk2dTextMesh monsterStandardLevel;
    public tk2dSlicedSprite monsterExpGauge;


    //public GameObject NormalMaterial;
    //public GameObject RareMaterial;
    //public tk2dSlicedSprite gaugeBar;


    private static float currentExp = 0;//나중에 DB에서 불러와야함.
    private static float currentGaugeScale = 0;
    private static int levelForText = 0;
    private static float needExpForText = 0;

    private int num;
    private int normalMaterialNum;
    private int rareMaterialNum;
    private int normalMaterialUpValue = 50;
    private int rareMaterialUpValue = 100;


    /*
    레벨업 시 증가 퍼센트
    */
    //private const int increasePercent = 10;

    /*
    레벨업 시 증가 경험치
    */
    private const int increaseExp = 100;

    public tk2dTextMesh normalMaterialText;
    public tk2dTextMesh rareMaterialText;
    public tk2dTextMesh monsterExpText;
    public tk2dTextMesh monsterneedExpText;

    //public tk2dTextMesh levelText;
    //public tk2dTextMesh expText;

    string selectName;
    public string selectNum;
    public Gauge statGauge;

    public GameObject HiveSoundObject;
    public AudioClip btn_Select;
    public AudioClip btn_NotExist;
    public AudioClip btn_MaterialSelect;
    public AudioClip btn_LevelUpSound;

    Ray ray;
    RaycastHit hit;



	// Use this for initialization
	void Start () {

        normalMaterialNum = ServerInterface.Instance.userState.GetNormalMaterial(); // 우정포인트 정보 서버에서 가져오기
        rareMaterialNum = ServerInterface.Instance.userState.GetRareMaterial();

        normalMaterialText.text = ServerInterface.Instance.userState.GetNormalMaterial().ToString();
        rareMaterialText.text = ServerInterface.Instance.userState.GetRareMaterial().ToString();
       
	}
	
	// Update is called once per frame
	void Update () {

        LevelSelectCharacter();
        
	
	}


    public void LevelSelectCharacter()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit) && hit.collider.tag == "CharacterLevel")  ////
            {
                HiveSoundObject.GetComponent<AudioSource>().clip = btn_Select;
                HiveSoundObject.GetComponent<AudioSource>().Play();


                selectNum = hit.collider.name.Substring(13, 2);
                

                if (DataManage._monster[System.Convert.ToInt32(selectNum)].monsterExist == true)
                {
                    LevelUpSetting.SetActive(true);

                    LevelUpSelectCharacter = Instantiate(NewHiveObject.gachaList[System.Convert.ToInt32(selectNum)], InstantiatePoint.transform.position , NewHiveObject.gachaList[System.Convert.ToInt32(selectNum)].transform.rotation) as GameObject;
                    //LevelUpSelectCharacter.transform.parent = this.gameObject.transform.GetChild(0);
                    LevelUpSelectCharacter.transform.parent = GameObject.Find("LevelSetting").transform;
                    LevelUpSelectCharacter.GetComponent<tk2dSprite>().SortingOrder = 12;
                    LevelUpSelectCharacter.GetComponent<tk2dSprite>().scale = new Vector3(150, 150, 1);



                    statGauge.SetStat(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed);


                    monsterName.text = DataManage._monster[System.Convert.ToInt32(selectNum)].monsterName.ToUpper();
                    monsterLevel.text = System.Convert.ToString(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level);
                    monsterStandardLevel.text = System.Convert.ToString(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv);

                    normalMaterialText.text = normalMaterialNum.ToString();
                    rareMaterialText.text = rareMaterialNum.ToString();
                    monsterExpText.text = System.Convert.ToString(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp);
                    monsterneedExpText.text = System.Convert.ToString(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp);

                    monsterExpGauge.dimensions = new Vector2((float)(380.0f * ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp / ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp), 35);

                    

                    

                }
                else
                {
                    HiveSoundObject.GetComponent<AudioSource>().clip = btn_NotExist;
                    HiveSoundObject.GetComponent<AudioSource>().Play();

                    print("해당 몬스터가 없음");
                    //원래는 팝업창을 띄워야 한다.
                   //몬스터가 없을 경우
                }
                //check.transform.position = hit.collider.transform.position + new Vector3(50, 50, -2);
               
            }
        }
    }

    void ClickNormal()
    {

        float gaugePercent = 0.0f;
        if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level < ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv)
        {
            print(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp);
            if (normalMaterialNum > 0)
            {
                HiveSoundObject.GetComponent<AudioSource>().clip = btn_MaterialSelect;
                HiveSoundObject.GetComponent<AudioSource>().Play();
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp += normalMaterialUpValue;
                normalMaterialNum--;
                ServerInterface.Instance.userState.SetNormalMaterial(normalMaterialNum);
                gaugePercent = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp / ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp;
                monsterExpGauge.dimensions = new Vector2((float)(380.0f * gaugePercent), 35);
            }

              //레벨 업 할 경우
            if(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp >= ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp)
            {
                //int increaseHp = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp / increasePercent;
                //int increaseSpeed = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed / increasePercent;
                //int increaseAG = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround / increasePercent;
                //int increaseAA = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir / increasePercent;


                HiveSoundObject.GetComponent<AudioSource>().clip = btn_LevelUpSound;
                HiveSoundObject.GetComponent<AudioSource>().Play();

                //이펙트 함수 추가

                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level++;
                // 현재 경험치를 0으로 초기화
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp = 0;
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp += increaseExp;



                gaugePercent = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp / ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp;
                monsterExpGauge.dimensions = new Vector2((float)(380.0f * gaugePercent), 35);
                print(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp);
                /*
                 몬스터 능력치를 증가시킨다.
                 현재 능력치의 10%를 증가시켜 보고 나중에 정확한 수치는 수정할 예정

                   - 16 08 12 : 레벨업은 자주 있는건데 10%를 증가시키니까 한도끝도 없이 증가하는 경우가 발생해서 일단은 레벨업당 5씩 증가시켜볼 예정
                 */
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp += 5;
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed += 1;
                if(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround += 5;
                }
                else ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround += 0;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir += 5;
                }
                else ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir += 0;

                ServerInterface.Instance.IncreaseMonsterAbility(System.Convert.ToInt32(selectNum),
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed,
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir
                    );


                statGauge.SetStat(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed);


            }
            // 여기에 서버와의 통신 추가
            ServerInterface.Instance.GetMonsterLevelData(System.Convert.ToInt32(selectNum));

        }
        else
        {
            //최대 레벨
            monsterExpGauge.dimensions = new Vector2((float)(380.0f), 35);
            
            //레벨업을 할수 없게 해야한다.
        }
        normalMaterialText.text = normalMaterialNum.ToString();
        monsterLevel.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level.ToString();
        monsterExpText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp.ToString();
        monsterneedExpText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp.ToString();
    }

    void ClickRare()
    {

        float gaugePercent = 0.0f;

        if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level < ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv)
        {

            if (rareMaterialNum > 0)
            {

                HiveSoundObject.GetComponent<AudioSource>().clip = btn_MaterialSelect;
                HiveSoundObject.GetComponent<AudioSource>().Play();

                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp += rareMaterialUpValue;
                rareMaterialNum--;
                ServerInterface.Instance.userState.SetRareMaterial(rareMaterialNum);
                gaugePercent = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp / ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp;
                monsterExpGauge.dimensions = new Vector2((float)(380.0f * gaugePercent), 35);
            }

            //레벨 업 할 경우
            if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp >= ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp)
            {

                

                HiveSoundObject.GetComponent<AudioSource>().clip = btn_LevelUpSound;
                HiveSoundObject.GetComponent<AudioSource>().Play();

                //이펙트 함수 추가

                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level++;
                // 현재 경험치를 0으로 초기화
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp = 0;
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp += increaseExp;


                gaugePercent = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp / ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp;
                monsterExpGauge.dimensions = new Vector2((float)(380.0f * gaugePercent), 35);

                /*
                몬스터 능력치를 증가시킨다.
                현재 능력치의 10%를 증가시켜 보고 나중에 정확한 수치는 수정할 예정
                
                   - 16 08 12 : 레벨업은 자주 있는건데 10%를 증가시키니까 한도끝도 없이 증가하는 경우가 발생해서 일단은 레벨업당 5씩 증가시켜볼 예정
                 */
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp += 5;
                ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed += 1;
                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround != 0)
                {
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround += 5;
                }
                else ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround += 0;

                if (ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir != 0)
                {
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir += 5;
                }
                else ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir += 0;

                ServerInterface.Instance.IncreaseMonsterAbility(System.Convert.ToInt32(selectNum),
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed,
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                    ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir
                    );

                statGauge.SetStat(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Hp,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackAir,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attackGround,
                                      ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].speed);

            }

            // 여기에 서버와의 통신 추가
            ServerInterface.Instance.GetMonsterLevelData(System.Convert.ToInt32(selectNum));

        }
        else
        {
            //최대 레벨
            monsterExpGauge.dimensions = new Vector2((float)(380.0f), 35);

            //레벨업을 할수 없게 해야한다.
        }

        rareMaterialText.text = rareMaterialNum.ToString();
        monsterLevel.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level.ToString();
        monsterExpText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].currentExp.ToString();
        monsterneedExpText.text = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp.ToString();
    }


    //void LevelUp()
    //{
    //    if (DataManage._monster[System.Convert.ToInt32(selectNum)].level < ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].maxLv)
    //    {
    //        if(DataManage._monster[System.Convert.ToInt32(selectNum)].exp >= ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp)
    //        {
    //            DataManage._monster[System.Convert.ToInt32(selectNum)].level++;
    //            ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].needExp *= 2;

    //        }
    //    }
    //}



    public void Back()
    {

        HiveSoundObject.GetComponent<AudioSource>().clip = btn_Select;
        HiveSoundObject.GetComponent<AudioSource>().Play();

        LevelUpSetting.SetActive(false);
        //가지고 있는 경험치 아이템 갱신 코드 추가

        //예시 갱신화
        ServerInterface.Instance.userState.SetNormalMaterial(normalMaterialNum);
        ServerInterface.Instance.userState.SetRareMaterial(rareMaterialNum);

        for(int i = 0; i < 15; ++i)
        {
            Monsters[i].SendMessage("initInfo");
        }
      
        
        Destroy(LevelUpSelectCharacter);
    }

}


