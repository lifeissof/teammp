﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.EventSystems;

public class PlanetMonster : AutoUnit {


    float groundY;

    bool meeting = false;
    bool isReadytoMove = false;
    //public tk2dSpriteAnimation[] monsterLib = new tk2dSpriteAnimation[15];
    public delegate void MyEventHandler(Unit target, int damage);
    public event MyEventHandler Dot;

    int monsterSpeed;
    int monsterHp;
  

    public delegate void ScrollHandler();
    public static event ScrollHandler AllyDie;
	// Use this for initialization

    void Awake()
    {
        this.gameObject.transform.DOScale(new Vector3(1.0f, 1.0f, 1.0f), 1).SetEase(Ease.OutQuad).OnComplete(moveCheck).SetTarget(this.gameObject);
        groundY = this.gameObject.transform.position.y;
        number = System.Convert.ToInt32(this.gameObject.name.Substring(7, 2));

        //state_P = DataManage.monsterPublicInfo[number];
        //state_L = DataManage.monsterLevelInfo[number];      //일단 서버에서 받아오지않기때문에 로컬 내에서 처리하도록 함

        state_P = ServerInterface.Instance.monsterState_P[number];      
        state_L = ServerInterface.Instance.monsterState_L[number];

        isAttackPlaying = false;

        mySkill = CreateSkill(this.gameObject.name);
        dotTime = 0f;
        
    }

	void Start () {


        instanceHpBar = Instantiate(hpBar, this.gameObject.transform.position + (hpBarPosition), this.gameObject.transform.rotation) as GameObject;
        instanceHpBar.transform.localScale = (instanceHpBar.transform.localScale / 10);
        instanceHpBar.transform.parent = this.gameObject.transform;
        isHpBarExist = true;

        _lst_Ally.Add(this.gameObject);
        //state_L.Hp = 100;
        //state_L.MaxHp = state_L.Hp; 
        //state_L.Level = 10;
        state = (int)unitState.WALK;  
        
        
	}
	
	// Update is called once per frame
	void Update () {

        if (isHpBarExist == true)
        {
            instanceHpBar.transform.GetChild(0).GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(((float)state_L.Hp / (float)state_L.MaxHp) * 64, 13);
        }
        
        StateMachine();

	}

    void UnitColorDown()
    {
        if (this.gameObject != null)
        {
            this.gameObject.GetComponent<tk2dSprite>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 1);
            instanceHpBar.gameObject.SetActive(false);
            instanceHpBar.GetComponent<tk2dSprite>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 1);
            instanceHpBar.transform.GetChild(0).gameObject.GetComponent<tk2dSlicedSprite>().color = new Color(100f / 255f, 100f / 255f, 100f / 255f, 1);
        }
    }

    //스킬 추가

    public override Skill CreateSkill(string name)
    {
        switch (name)
        {
            case "Monster00(Clone)":
                return new Slime();
            case "Monster01(Clone)":
                return new KickMan();
            case "Monster02(Clone)":
                return new FlyingStone();
            case "Monster03(Clone)":
                return new GroundPiranha();
            case "Monster04(Clone)":
                return new EyeBlue();
            case "Monster05(Clone)":
               return new RunningTree();
            case "Monster06(Clone)":
               return new JumpingRedball();
            case "Monster07(Clone)":
               return new MonsterPlant();
            case "Monster08(Clone)":

               return new Boxer();

            case "Monster09(Clone)":
               return new AcidCrown();

            case "Monster10(Clone)":
               return new Boom();

            case "Monster11(Clone)":
               return new WhiteWolverine();

            case "Monster12(Clone)":
               return new BumpBird();

            case "Monster13(Clone)":
               return new RedDragon();

            case "Monster14(Clone)":
               return new Zero();
           
            default:
                return null;
        }
    }


    void moveCheck()
    {
        isReadytoMove = true;
    }

    public override void setTarget()
    {
        if (_lst_Enemy.Count > 0)
        {
    
            for (int i = 0; i < _lst_Enemy.Count; ++i)
            {
                if (_lst_Enemy[i].transform.position.x >= this.gameObject.transform.position.x)
                {
                 
                    Unit enemySize = _lst_Enemy[i].GetComponent<Unit>();
                    if ((enemySize.state_P.unitType == (int)type.AIR && state_L.attackAir != 0) || (enemySize.state_P.unitType == (int)type.GROUND && state_L.attackGround != 0))
                    {
                   
                        CurrDistance = (_lst_Enemy[i].transform.position.x - (enemySize.UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (UnitSize * 0.5f));
                        if (CurrDistance >= 0 && CurrDistance < ShortDistance)
                        {
                            Target = _lst_Enemy[i];
                            ShortDistance = (Target.transform.position.x - (enemySize.UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (UnitSize * 0.5f));
                        }
                    }
                    else if (_lst_Enemy[i].gameObject.tag == "spaceBattleship")
                    {
                        CurrDistance = (_lst_Enemy[i].transform.position.x - (enemySize.UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (UnitSize * 0.5f));
                        if (CurrDistance >= 0 && CurrDistance < ShortDistance)
                        {
                            Target = _lst_Enemy[i];
                            ShortDistance = (Target.transform.position.x - (enemySize.UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (UnitSize * 0.5f));
                        }
                    }
                    else
                    {/*
						CurrDistance = (_lst_Enemy[i].transform.position.x - (enemySize.UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (UnitSize * 0.5f));
						if (CurrDistance <= state_L.attackRange && CurrDistance >= 0)
						{
                           // StartCoroutine("walk");//StartCoroutine("Meeting");
						}
                      */
                    }
                }
            }
            if (Target != null)
            {
                Unit enemy = Target.GetComponent<Unit>();
                ShortDistance = (Target.transform.position.x - (enemy.UnitSize * 0.5f)) - (this.gameObject.transform.position.x + (UnitSize * 0.5f));
            }
        }
    }
    public override void walk()
    {

        //if (state_P.unitType == 1)
        //{

        //}
        
        animation.Play(string.Format("monster{0}_walk", number));

        if (meeting == false)
        {
            if (isReadytoMove)
            {
                if (this.gameObject.transform.localPosition.x < 0)
                {
                    //this.gameObject.transform.position += (Vector3.right.normalized * 30.0f) * Time.deltaTime;      //10.0f 는 몬스터의 스피드 값 서버에서 받아와야 한다.
                    this.gameObject.transform.position += (Vector3.right.normalized * state_L.speed) * Time.deltaTime;      //10.0f 는 몬스터의 스피드 값 서버에서 받아와야 한다.
                }
                else
                {
                    //this.gameObject.transform.position += (Vector3.right.normalized * 30.0f) * Time.deltaTime;
                    this.gameObject.transform.position += (Vector3.right.normalized * state_L.speed) * Time.deltaTime;      //10.0f 는 몬스터의 스피드 값 서버에서 받아와야 한다.
                }
            }
        }

        if (number == 6)
        {
            
        }
    }

    public override IEnumerator WalkTime()
    {
        yield return new WaitForSeconds(state_L.movingTime);
        if (state == (int)unitState.WALK)
        {
            state = (int)unitState.SEE;
            StartCoroutine("WalkDelay");
        }
    }
    public override IEnumerator WalkDelay()
    {
        yield return new WaitForSeconds(state_L.movingDelay);
        if (state == (int)unitState.SEE)
        {
            state = (int)unitState.WALK;
            StartCoroutine("WalkTime");
        }
    }

    public override IEnumerator meet()
    {
        meeting = true;
        yield return new WaitForSeconds(1.5f);
        meeting = false;
    }

 
    public override void see()
    {
        animation.Play(string.Format("monster{0}_see", number));
    }

    public override void idle()
    {
        animation.Play(string.Format("monster{0}_idle", number));
    }

    public override void attack()
    {
        if (Target != null)
        {
            Unit enemy = Target.GetComponent<Unit>();

            float delay;

            if (enemy.state_P.unitType == 0)
            {
                delay = state_L.attackGroundDelay;
            }
            else
            {
                delay = state_L.attackAirDelay;
            }

            if (Atk_Timer == 0 && isAttackPlaying == false)
            {
                Attacking(enemy);
            }

            Atk_Timer += Time.deltaTime;

            if (Atk_Timer >= delay)
            {
                Atk_Timer = 0;
            } 
            if ((Target.transform.position.x - (enemy.UnitSize * 0.5f)) < (this.gameObject.transform.position.x + (UnitSize * 0.5f)))
            {
                Target = null;
            }

            if (null != Target && 0 >= enemy.state_L.Hp)
            {
                Target = null;
            }

        }
        else
            state = (int)unitState.IDLE;
    }

    void Attacking(Unit enemy)
    {
        isAttackPlaying = true;
        if ((Target.transform.position.x - (enemy.UnitSize * 0.5f)) >= (this.gameObject.transform.position.x + (UnitSize * 0.5f)) && enemy != null)
        {
            UseSkill(enemy, enemy.state_P.unitType, 1);
        }
        else
        {
            isAttackPlaying = false;
            state = (int)unitState.IDLE;
        }
    }
    public override IEnumerator die()
    {

        Destroy(instanceHpBar);
        isHpBarExist = false;
        EmptyHP = true;
        _lst_Ally.Remove(this.gameObject);
        while (animation.IsPlaying(string.Format("monster{0}_die", number)) == false)
        {
            animation.Play(string.Format("monster{0}_die", number));
            yield return true;
        }
        if (state_P.unitType == 1 && animation.IsPlaying(string.Format("monster{0}_crash", number)))
        {
            float tweenTime = animation.CurrentClip.frames.Length / animation.CurrentClip.fps;
            this.gameObject.transform.DOMoveY(-state_P.posY * 0.5f , tweenTime , false).SetEase(Ease.InQuart);
        }
        else if (number == 6 && animation.IsPlaying(string.Format("monster{0}_crash", number)))
        {
            float tweenTime = animation.CurrentClip.frames.Length / animation.CurrentClip.fps;
            this.gameObject.transform.DOMoveY(groundY * 0.5f, tweenTime, false).SetEase(Ease.InQuart);
        }

        yield return new WaitForSeconds(animation.CurrentClip.frames.Length / animation.CurrentClip.fps);
        Destroy(this.gameObject);
      
        AllyDie();
    }

    void Push(int dis)
    {
        if (Target != null && Target.tag != "spaceBattleship" && Target.tag != "hiveSource")
        {
            Target.transform.DOMoveX(dis, 0.5f, false).SetEase(Ease.OutBack);
        }
    }

    public override void splash(int num)
    {
        float dis = 0;
        int HitNum = 0;
        Unit targetunit = Target.GetComponent<Unit>();
        for (int i = 0; i < _lst_Enemy.Count; ++i)
        {
            if (_lst_Enemy[i] != Target)
            {
                Unit allyunit = _lst_Ally[i].GetComponent<Unit>();
                dis = Mathf.Abs((Target.transform.position.x + (targetunit.UnitSize * 0.5f)) - (_lst_Enemy[i].transform.position.x + (allyunit.UnitSize * 0.5f)));
                if (dis < 10)
                {
                    HitNum++;
                    if (allyunit.state_P.unitType == (int)type.AIR)
                        allyunit.state_L.Hp -= state_L.attackAir;
                    else
                        allyunit.state_L.Hp -= state_L.attackGround;

                }
            }
            if (HitNum >= num)
                break;
        }
    }


    public override void Ending()
    {
        state_L.Hp = 0;
        //Destroy(instanceHpBar);
        //Destroy(this.gameObject);
    }

    public override void DotDamage(Unit target)
    {
        Dot += new MyEventHandler(target.DotDamaged);
        if (target != null && target.state_L.Hp > 0) Dot(target, 10);
        
    }

}
