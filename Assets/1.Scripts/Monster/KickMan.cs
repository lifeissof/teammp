﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class KickMan : Skill {

    int groundDamage;


    public override IEnumerator Skill1(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
            self.animation.Play(string.Format("monster{0}_atk", self.number));
        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        if (enemy != null && enemy.gameObject.tag == "spaceBattleship")
        {
            enemy.gameObject.GetComponent<SpaceBattleShip>().Damaged();
        }

        if (enemy.state_L.Hp - self.state_L.attackGround >= 0)
            enemy.state_L.Hp -= self.state_L.attackGround;
        else
            enemy.state_L.Hp = 0;

        //if (enemy.gameObject != null && enemy.gameObject.tag != "spaceBattleship" )
        //{
        //    enemy.gameObject.transform.DOMoveX(enemy.state_L.attackPush, 0.5f, false).SetEase(Ease.OutBack);
        //}
        self.isAttackPlaying = false;
        self.state = (int)unitState.IDLE;
    }
}
