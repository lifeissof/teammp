﻿using UnityEngine;
using System.Collections;

public class RunningTree : Skill{

    int groundDamage;
    int airDamage;
    Vector3 prevPosition;

    public override int ConfirmAttackType(int number)
    {
        switch (number)
        {
            case 0:
                return 0;
            case 1:
                return 1;
            default:
                return 0;
            /*   case 3:
                   return
               case 4:
                   return
              */
        }
    }

    public override IEnumerator Skill1(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
        {
            self.animation.Play(string.Format("monster{0}_atk", self.number));
            prevPosition = self.transform.position;
        }
        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            self.transform.position += new Vector3(0.5f, 0, 0);
            yield return true;
        }
        self.transform.position = prevPosition;

        if (enemy != null && enemy.gameObject.tag == "spaceBattleship")
        {
            enemy.gameObject.GetComponent<SpaceBattleShip>().Damaged();
        }

        if (enemy.state_L.Hp - self.state_L.attackGround >= 0)
            enemy.state_L.Hp -= self.state_L.attackGround;
        else
            enemy.state_L.Hp = 0;

        //if (enemy.gameObject != null && enemy.gameObject.tag != "spaceBattleship" && enemy.gameObject.tag != "hiveSource")
        //{
        //    iTween.MoveBy(enemy.gameObject, iTween.Hash(
        //        "x", enemy.state_L.attackPush,
        //        "easetype", iTween.EaseType.easeOutBack,
        //        "time", 0.5f
        //        ));
        //}

        self.state_L.Hp = 0;
        self.isAttackPlaying = false;
        self.state = (int)unitState.DIE;
    }

    public override IEnumerator Skill2(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
            self.animation.Play(string.Format("monster{0}_atk", self.number));
        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            prevPosition = self.transform.position;
            self.transform.position += new Vector3(0.5f, 0, 0);
            yield return true;
        }

        if (enemy != null && enemy.gameObject.tag == "spaceBattleship")
        {
            enemy.gameObject.GetComponent<SpaceBattleShip>().Damaged();
        }

        if (enemy.state_L.Hp - self.state_L.attackAir >= 0)
            enemy.state_L.Hp -= self.state_L.attackAir;
        else
            enemy.state_L.Hp = 0;

        //if (enemy.gameObject != null && enemy.gameObject.tag != "spaceBattleship" && enemy.gameObject.tag != "hiveSource")
        //{
        //    //iTween.MoveBy(enemy.gameObject, iTween.Hash(
        //    //    "x", enemy.state_L.attackPush,
        //    //    "easetype", iTween.EaseType.easeOutBack,
        //    //    "time", 0.5f
        //    //    ));
        //}
        self.isAttackPlaying = false;
        self.state = (int)unitState.IDLE;
    }
}
