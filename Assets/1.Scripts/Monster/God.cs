﻿using UnityEngine;
using System.Collections;

public class God : Unit {

    //public GameObject objectForBgm;
    public GameObject damagedEffect;

    bool isPlaying = false;
    public static bool playOnce = true;

	// Use this for initialization
	void Start () {

        state_L.MaxHp = 1000;
        state_L.Hp = 1000;
        _lst_Ally.Add(this.gameObject);

	
	}
	
	// Update is called once per frame
	void Update () {

        if (state_L.Hp >= 60)
        {
            animation.Play("god_normal");
        }
        else if (state_L.Hp >= 30)
        {
            animation.Play("god_beaten0");
        }
        else
        {
            animation.Play("god_beaten1");
        }

        if (state_L.Hp <= 0 && isPlaying == false)
        {
            isPlaying = true;
            die();
        }
	
	}

    public void die()
    {
        BattleScroll Obj = GameObject.Find("Manager").GetComponent<BattleScroll>();
        _lst_Ally.Remove(this.gameObject);

        Obj.SendMessage("Ending");
        damagedEffect.SetActive(false); 
        //Destroy(instanceHpBar);
    }

    IEnumerator waitSeconds()
    {
        yield return new WaitForSeconds(4f);
    }

    public IEnumerator Damaged()
    {
        if (damagedEffect.activeSelf == false)
        {
            damagedEffect.SetActive(true);
        }
        while (damagedEffect.GetComponent<tk2dSpriteAnimator>().IsPlaying("god_attackEffect") == false)
        {
            damagedEffect.GetComponent<tk2dSpriteAnimator>().Play("god_attackEffect");
            yield return true;
        }

        while (damagedEffect.GetComponent<tk2dSpriteAnimator>().CurrentFrame != damagedEffect.GetComponent<tk2dSpriteAnimator>().CurrentClip.frames.Length)
        {
            yield return true;
        }
        damagedEffect.SetActive(false);
        damagedEffect.GetComponent<tk2dSpriteAnimator>().SetFrame(0);
    }
}
