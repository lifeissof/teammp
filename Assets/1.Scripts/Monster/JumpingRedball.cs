﻿using UnityEngine;
using System.Collections;

public class JumpingRedball : Skill{

    public override IEnumerator Skill1(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
            self.animation.Play(string.Format("monster{0}_atk", self.number));
        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        if (enemy != null && enemy.gameObject.tag == "spaceBattleship")
        {
            enemy.gameObject.GetComponent<SpaceBattleShip>().Damaged();
        }

        if (enemy.state_L.Hp - self.state_L.attackGround >= 0)
            enemy.state_L.Hp -= self.state_L.attackGround;
        else
            enemy.state_L.Hp = 0;

        //if (enemy.gameObject != null && enemy.gameObject.tag != "spaceBattleship" && enemy.gameObject.tag != "hiveSource")
        //{
        //    iTween.MoveBy(enemy.gameObject, iTween.Hash(
        //        "x", enemy.state_L.attackPush,
        //        "easetype", iTween.EaseType.easeOutBack,
        //        "time", 0.5f
        //        ));
        //}
        self.isAttackPlaying = false;
        self.state = (int)unitState.IDLE;
    }
}
