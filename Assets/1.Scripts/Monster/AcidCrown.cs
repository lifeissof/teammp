﻿using UnityEngine;
using System.Collections;

public class AcidCrown : Skill {

    int groundDamage;

    /*
    public override int ConfirmAttackType(int number)
    {
        switch (number)
        {
            case 1:
                return 1;
            case 2:
                return 1;
            default:
                return 1;
            /*   case 3:
                   return
               case 4:
                   return
             * 
        }
    }*/

    public override IEnumerator Skill1(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
        {
            self.animation.Play(string.Format("monster{0}_atk", self.number));
        }
        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame <= 5/*("monster9_attack_0016"*/)
        {
            yield return true;
        }

        if (enemy != null && enemy.gameObject.tag == "spaceBattleship")
        {
            enemy.gameObject.GetComponent<SpaceBattleShip>().Damaged();
        }

        if (enemy.state_L.Hp - self.state_L.attackGround >= 0)
            enemy.state_L.Hp -= self.state_L.attackGround;
        else
            enemy.state_L.Hp = 0;

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        self.isAttackPlaying = false;
        self.state = (int)unitState.IDLE;
    }
}
