﻿using UnityEngine;
using System.Collections;

public class Zero : Skill {

    int groundDamage;
    //public override int ConfirmAttackType(int number)
    //{
    //    switch(number)
    //    {
    //        case 1:
    //            return 0;
    //        case 2:
    //            return 0;
    //        default:
    //            return 0;
    //     /*   case 3:
    //            return
    //        case 4:
    //            return
    //      * */
    //    }
    //}

    public override IEnumerator Skill1(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
            self.animation.Play(string.Format("monster{0}_atk", self.number));

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame <= 22)
        {
            yield return true;
        }

        Damage(enemy, self, "ground");

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame <= 37)
        {
            yield return true;
        }

        Damage(enemy, self, "ground");

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        self.isAttackPlaying = false;
        self.state = (int)unitState.IDLE;
    }
}
