﻿using UnityEngine;
using System.Collections;

public class Boxer : Skill {

    int groundDamage;


    public override IEnumerator Skill1(Unit enemy, Unit self)
    {
        if (false == self.animation.IsPlaying(string.Format("monster{0}_atk", self.number)))
            self.animation.Play(string.Format("monster{0}_atk", self.number));

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame <= 6)
        {
            yield return true;
        }

        Damage(enemy, self, "ground");

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame <= 13)
        {
            yield return true;
        }

        Damage(enemy, self, "ground");

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame <= 20)
        {
            yield return true;
        }

        Damage(enemy, self, "ground");

        while (self.animation.CurrentClip.name == string.Format("monster{0}_atk", self.number) && self.animation.CurrentFrame != self.animation.CurrentClip.frames.Length)
        {
            yield return true;
        }

        self.isAttackPlaying = false;
        self.state = (int)unitState.IDLE;
    }
}
