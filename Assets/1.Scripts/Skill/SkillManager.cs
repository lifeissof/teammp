﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class SkillManager : MonoBehaviour {


    public GameObject SoundObject;
    public GameObject ExplainSkill;
    public GameObject UserFractal;
    public GameObject UserFriendShip;
    public GameObject UserGold;


    //public AudioClip bgm;
    //public AudioClip btn_play;
    //public AudioClip btn_arrow;
    //public AudioClip btn;



    bool isSliding;
    bool isPlayReady;

    public static bool isLoad = false;


    // Use this for initialization
    void Start()
    {


        UserFriendShip.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFriendshipPoint()); // 우정포인트 정보 서버에서 가져오기
        UserFractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFractal());
        UserGold.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetGold());


        isSliding = false;
        isPlayReady = false;
        isLoad = false;

        //SoundObject.GetComponent<AudioSource>().clip = bgm;
        //SoundObject.GetComponent<AudioSource>().loop = true;
        //SoundObject.GetComponent<AudioSource>().Play();


    }

    void Update()
    {

    }

    // Update is called once per frame

    void PlayFunc()
    {
        StopCoroutine("Play");
        StartCoroutine("Play");

    }
    

    IEnumerator Play()
    {

        if (isPlayReady == false)
        {
            //popUpSmall.SetActive(true);       몬스터를 넣지 않았을 경우.    팝업창을 띄운다.
            yield return new WaitForSeconds(0.3f);
            //popUpSmall.SetActive(false);
        }


        if (isLoad == false)
        {
            if (DataManage.GameCount > 0)
            {
                //GetComponent<AudioSource>().clip = btn_play;
                //GetComponent<AudioSource>().Play();
                //Application.LoadLevel("Stage");
                SceneManager.LoadScene("Stage");
            }
        }
    }

}
