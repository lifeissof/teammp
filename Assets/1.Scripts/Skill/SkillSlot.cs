﻿using UnityEngine;
using System.Collections;

public class SkillSlot : MonoBehaviour {

    Ray ray;
    RaycastHit hit;

    public GameObject[] skillentryLock = new GameObject[2];          //엔트리를 잠구는 목록 2개
    public GameObject[] skillentryslot = new GameObject[5];          //총 슬롯 5개
    public tk2dSpriteAnimation[] skillentryLib = new tk2dSpriteAnimation[15];    //현재 리소스가 있는 캐릭터 15개의 배열

    public AudioClip btn_register_ok;
    public AudioClip btn_register_cancel;
    public AudioClip btn_register_fail;


    void OnEnable()
    {
        //OnEnable()은 게임오브젝트가 활성화되면 실행할 함수를 호출하는 기능이다.
        EntrySlotSetting();           //엔트리 슬롯을 몇개 열것인지 -> 캐릭터를 몇개를 사용할것인지.
        SlotSetting();
        //FriendSlotSetting();

    }

    void OnDisable()
    {
        //OnDisable()은게임오브젝트가 비활성화되면 실행할 함수를 코딩

    }

    void EntrySlotSetting()
    {
        print(DataManage.SkillSlotNum);
        switch (DataManage.SkillSlotNum)
        {
            case 3:
                {
                    skillentryslot[3].SetActive(false);
                    skillentryslot[4].SetActive(false);
                    //entryslot[5].SetActive(true);
                    skillentryLock[0].SetActive(true);
                    skillentryLock[1].SetActive(true);
                    break;
                }
            case 4:
                {
                    skillentryslot[3].SetActive(true);
                    skillentryslot[4].SetActive(false);
                    //entryslot[5].SetActive(true);
                    skillentryslot[0].SetActive(false);
                    skillentryslot[1].SetActive(true);
                    break;
                }
            case 5:
                {
                    skillentryslot[3].SetActive(true);
                    skillentryslot[4].SetActive(true);
                    //entryslot[5].SetActive(true);
                    skillentryslot[0].SetActive(false);
                    skillentryslot[1].SetActive(false);
                    break;
                }
            default:
                {
                    skillentryslot[3].SetActive(false);
                    skillentryslot[4].SetActive(false);
                    //entryslot[5].SetActive(true);
                    skillentryslot[0].SetActive(true);
                    skillentryslot[1].SetActive(true);
                    break;
                }
        }
    }

    void SlotSetting()
    {

        for (int i = 0; i < DataManage.SkillSlotNum; ++i)
        {
            tk2dSpriteAnimator anim = skillentryslot[i].GetComponent<tk2dSpriteAnimator>();

            if (DataManage._skillSlot[i].SkillName != "None")
            {
                skillentryslot[i].name = string.Format("Character{0}", DataManage._skillSlot[i].SkillName);
                anim.enabled = true;
                anim.Library = skillentryLib[System.Convert.ToInt32(DataManage._skillSlot[i].SkillName)];
                anim.Play(skillentryslot[i].name);
            }
            else
            {
                skillentryslot[i].name = "None";
                tk2dSprite sprite = skillentryslot[i].GetComponent<tk2dSprite>();
                anim.Library = skillentryLib[0];
                anim.Play("Character00");
                anim.enabled = false;
                int ID_x = sprite.GetSpriteIdByName("character00_h");
                sprite.spriteId = ID_x;
            }
        }
        ServerInterface.Instance.SetEntryMonster();     //이 부분 서버 추가.
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.tag.Substring(0, 10) == "SkillEntry")
            {
                GetComponent<AudioSource>().clip = btn_register_cancel;
                GetComponent<AudioSource>().Play();


                int hitNum = System.Convert.ToInt32(hit.collider.tag.Substring(10, 1));
                //DataManage._entrySlot[hitNum] = DataManage.monsterNone;
                DataManage._skillSlot[hitNum] = DataManage.skillNone;
                //this.SendMessage("SlotSetting");
                SlotSetting();
            }
        }

        else if (Physics.Raycast(ray, out hit) && hit.collider.name.Substring(0, 4) == "Skil")
        {



            int num = System.Convert.ToInt32(hit.collider.name.Substring(5, 2));
            if (DataManage._skill[num].SkillExist == true)       //캐릭터가 있는지 없는지 판별하는 조건문
            {

                bool entry_ok = true;
                string hitName = hit.collider.name;

                for (int i = 0; i < DataManage.SkillSlotNum; ++i)
                {
                    if (skillentryslot[i].name == hitName)
                    {
                        entry_ok = false;
                        break;
                    }
                }
                if (entry_ok == true)
                {
                    int entryNum = 0;
                    for (int i = 0; i < DataManage.SkillSlotNum; ++i)
                    {
                        if (DataManage._skillSlot[i].SkillName == "None")
                        {
                            GetComponent<AudioSource>().clip = btn_register_ok;
                            GetComponent<AudioSource>().Play();

                            //if (i == 0)리더 시스템은 나중에.
                            //{
                            //    leader = System.Convert.ToInt32(hitName.Substring(9, 2));
                            //}


                            DataManage._skillSlot[i] = DataManage._skill[System.Convert.ToInt32(hitName.Substring(9, 2))];

                            //this.SendMessage("SlotSetting");
                            SlotSetting();
                            break;
                        }
                        else
                            entryNum++;
                    }
                    if (entryNum == DataManage.SkillSlotNum)//엔트리가 꽉참
                    {

                    }
                }
                else    //엔트리에 이미 있을때
                {

                }
            }
            else    // 없는 유닛 등록시
            {
            }
        }
    }
}
