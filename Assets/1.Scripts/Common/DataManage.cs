﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


enum SOURCE_UPGRADE
{
    FIRE,
    WATER,
    PlANT,
    TOTAL
}

enum SOURCE_LEVEL
{
    LEVEL,
    TOTAL
}

enum SOURCE_GAME
{
    MONSTER,
    //SKILL,
    TOTAL
}

enum SOURCE_ACT
{
    ACT,
    TOTAL
}

enum SOURCE_SKILL
{ 
    SKILL,
    TOTAL
}

enum OwnMonster
{
    NUMBER = 1
}

public class UserSkill
{
   public int[] needSkillSource;         //나중에 스킬부분은 클래스로 나누어야 한다.
   public string SkillName;
   public string SKillInfo;
   public string SkillNumber;
   public bool SkillExist;

   public int level;
   public int maxLevel;
   public float exp;
   public float needExp;

   public UserSkill()        //생성자
   {
       level = 1;
       maxLevel = 20;
       exp = 0;
       needExp = 100;
   }


   public void SetSkillSource(int Skill)
   {
       needSkillSource = new int[(int)SOURCE_SKILL.TOTAL];
       needSkillSource[(int)SOURCE_SKILL.SKILL] = Skill;
   }

   public int[] GetSkillSource()
   {
       return needSkillSource;
   }

   public bool CheckSkillSource(List<CSource> haveSource)
   {
       int ok = 0;
       for (int i = 0; i < (int)SOURCE_SKILL.TOTAL; ++i)
       {
           //if (haveSource[i].GetEnergyBig() >= needGameSource[i])
           //{
           //    ok++;
           //}
           if (haveSource[i].GetEnergy() >= needSkillSource[i])
           {
               ok++;
           }
       }
       if (ok == (int)SOURCE_SKILL.TOTAL)
           return true;

       else return false;
   }
}

public class Monster
{
    public int[] needUpgradeSource; //업그레이드에 필요한 소스들의 수
    public int[] needGameSource; //게임에 필요한 소스들의 수이다.  
    public int[] needActSource; //행동력에 필요한 값
   
    //public int[] needLevelSource;
    //public int[] needSkillSource;

    public string monsterName;
    public string monsterInfo;
    public string monsterNumber;
    public bool monsterExist;           //몬스터의 유무를 판별하는 bool 변수 
    public int monsterAttribute;        //몬스터의 속성값 속성을 기준으로 등급업을 시킨다.  0 : 불    1 : 물    2 : 풀

    public int grade;
    public int level;
    public int maxLevel;
    public float exp;
    public float needExp;
    


    public Monster()        //생성자
    {
        level = 1;
        exp = 0;
        maxLevel = 10;
        needExp = 200;
    }

    public void SetUpgradeSource(int FIRE, int WATER, int PlANT)  //등급 업그레이드 위한 소스들의 값을 넣어주는 Set함수
    {
        needUpgradeSource = new int[(int)SOURCE_UPGRADE.TOTAL];
        needUpgradeSource[(int)SOURCE_UPGRADE.FIRE] = FIRE;
        needUpgradeSource[(int)SOURCE_UPGRADE.WATER] = WATER;
        needUpgradeSource[(int)SOURCE_UPGRADE.PlANT] = PlANT;

    }

    public int[] GetUpgradeSource()
    {
        return needUpgradeSource;
    }



    //public void SetSkillSource(int Skill)
    //{
    //    needSkillSource = new int[(int)SOURCE_SKILL.TOTAL];
    //    needSkillSource[(int)SOURCE_SKILL.SKILL] = Skill;
    //}

    //public int[] GetSkillSource()
    //{
    //    return needSkillSource;
    //}


   

    public void SetGameSource(int Monster /* , int Skill*/)
    {
        needGameSource = new int[(int)SOURCE_GAME.TOTAL];
        needGameSource[(int)SOURCE_GAME.MONSTER] = Monster;
        //needGameSource[(int)SOURCE_GAME.SKILL] = Skill;
    }


    public bool CheckGameSource(List<CSource> haveSource)
    {
        int ok = 0;
        for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
        {
            //if (haveSource[i].GetEnergyBig() >= needGameSource[i])
            //{
            //    ok++;
            //}
            if (haveSource[i].GetEnergy() >= needGameSource[i])
            {
                ok++;
            }
        }
        if (ok == (int)SOURCE_GAME.TOTAL)
            return true;

        else return false;
    }

    //public bool CheckSkillSource(List<CSource> haveSource)
    //{
    //    int ok = 0;
    //    for (int i = 0; i < (int)SOURCE_SKILL.TOTAL; ++i)
    //    {
    //        //if (haveSource[i].GetEnergyBig() >= needGameSource[i])
    //        //{
    //        //    ok++;
    //        //}
    //        if (haveSource[i].GetEnergy() >= needGameSource[i])
    //        {
    //            ok++;
    //        }
    //    }
    //    if (ok == (int)SOURCE_SKILL.TOTAL)
    //        return true;

    //    else return false;
    //}


    //public bool CheckHiveSource(CSource[] haveSource)
    //{
    //    int ok = 0;

    //    for (int i = 0; i < (int)SOURCE_UPGRADE.TOTAL; ++i)
    //    {
    //        if (haveSource[i].GetEnergyBig() >= needHiveSource[i])
    //        {
    //            ok++;
    //        }
    //    }
    //    if (ok == (int)SOURCE_UPGRADE.TOTAL)
    //        return true;
    //    else
    //        return false;
    //}


    public int[] GetGameSource()
    {
        return needGameSource;
    }

    public void SetActSource(int Act /* , int Skill*/)
    {
        needActSource = new int[(int)SOURCE_ACT.TOTAL];
        needActSource[(int)SOURCE_ACT.ACT] = Act;
    }

    public int[] GetActSource()
    {
        return needActSource;
    }

    public bool CheckActSource(List<CSource> haveSource)
    {
        int ok = 0;
        for (int i = 0; i < (int)SOURCE_ACT.TOTAL; ++i)
        {

            if (haveSource[i].GetEnergy() >= needGameSource[i])
            {
                ok++;
            }
        }
        if (ok == (int)SOURCE_ACT.TOTAL)
            return true;

        else return false;
    }

}
public class CSource       //게임에 필요한 소스에 대한 객체.
{
    private int price;
    //private int energyBig;
    //private int energySmall;
    private int energy;
    

    //private bool isLock;       //게임을 해나가면서 사용할 수 있도록 풀어줄 생각

    public CSource()        //게임 소스 생성자
    {
        //price = 100;
        //energyBig = 0;
        //energySmall = 0;

        price = 100;
        energy = 0;
    }

    //public bool IsLock()
    //{
    //    return isLock;
    //}
    //public void SetLock(bool b)
    //{
    //    isLock = b;
    //}
    public int GetPrice()
    {
        return price;
    }
    public void SetEnergy(int val)
    {
        //energyBig = val;
          energy = val;
    }
    public int GetEnergy()
    {
        //return energyBig;
        return energy;
    }
    //public void SetEnergySmall(int val)
    //{
    //    energySmall = val;
    //}
    //public int GetEnergySmall()
    //{
    //    return energySmall;
    //}

    public void AddEnergy(int val)
    {
       SetEnergy(GetEnergy() + val);
    }
    //public void AddEnergySmall(int val)
    //{
    //    int num = GetEnergySmall() + val;
    //    if (num < 6)
    //    {
    //        SetEnergySmall(num);
    //    }
    //    else
    //    {
    //        AddEnergyBig(1);
    //        SetEnergySmall(num - 6);
    //    }
    //}
}

public class DataManage : MonoBehaviour
{

    public static List<CSource> _srcUpgrade = new List<CSource>();      //객체 생성
    public static List<CSource> _srcGame = new List<CSource>();         //객체 생성
    public static List<CSource> _srcSkill = new List<CSource>();   //객체 생성
    public static List<CSource> _srcAct = new List<CSource>();

    public static List<Monster> _entrySlot = new List<Monster>(); //몬스터를 넣고 게임을 할 슬롯
    public static Monster _friendEntrySlot; // 친구 엔트리 슬롯
    public static List<UserSkill> _skillSlot = new List<UserSkill>();
    public static Monster[] _monster = new Monster[45];          //총 몬스터 45종 현재는 15종이 있음
    public static UserSkill[] _skill = new UserSkill[5];             //현재 스킬 5종 

    public static Enemy[] _enemy = new Enemy[5];                //총 몬스터 5종 현재는 3종 존재

    public static Monster monsterNone;                          //몬스터가 없을경우의 변수
    public static UserSkill skillNone;


    public static PublicState[] monsterPublicInfo = new PublicState[15];           //몬스터의 자세한 정보
    public static PublicState[] enemyPublicInfo = new PublicState[15];
    public static LevelState[] monsterLevelInfo = new LevelState[15];              //몬스터의 레벨에 따른 자세한 정보
    public static LevelState[] enemyLevelInfo = new LevelState[15];

    //16 03 23 현재 오류 -> 몬스터의 레벨업에 필요한 스텟정보를 서버를 통해 받아와야 하지만 바로 서버를 구현할 수 없기 때문에 로컬내에서 값을 정하여 해결해야한다.
    

    public static int[] entryName = new int[5];                 //5개의 엔트리를 배열로 관리하겠음.
    public static int[] SkillentryName = new int[5];
    public static int entrySlotNum;                             //엔트리 슬롯의 숫자.
    public static int SkillSlotNum = 5;
    public static int GameCount = 5;
    //public static UserData userState = new UserData();
    //public static UserStageInfo StageInfo = new UserStageInfo();


    //Enemy 객체 생성해야함.


    void Awake()  // start()함수가 실행되기 전에 실행
    {
        ServerInterface.Instance.OnTakeData += InitData;

    }

    void InitData()
    {
        MonsterInit();
        EnemyInit();
        UserSkillInit();
        //entrySlotNum = 4;
        //print("entry" + ServerInterface.Instance.userState.GetEntryNumber());
        entrySlotNum = ServerInterface.Instance.userState.GetEntryNumber();


        //SkillSlotNum = ServerInterface.Instance.userState.GetSkillNumber();  //16 07 14

        for (int i = 0; i < entrySlotNum; ++i)
        {
            entryName[i] = ServerInterface.Instance.userState.GetEntry(i);
        }


        //16 07 14
        //for (int i = 0; i < entrySlotNum; ++i)
        //{
        //    SkillentryName[i] = ServerInterface.Instance.userState.GetSkillEntry(i);
        //}

        SourceInit();

        //for (int i = 0; i < (int)OwnMonster.NUMBER; i++) // 각각의 몬스터 필요정보
        //{

        //}

        //for (int i = 0; i < ServerInterface.Instance.userState.ownMonster.Count; ++i) //전체 몬스터 갯수
        //{
        //    if (System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["level"].ToString()) >= 1)
        //    {
        //        ServerInterface.Instance.GetSingleMonster(System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["level"].ToString()),  // 몬스터 이름과 레벨을 통해 몬스터 정보 받아오기
        //            string.Format("mon{0:00}", System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["monNumber"].ToString())));
        //        _monster[System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["monNumber"].ToString())].level = System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["level"].ToString()); // 보유 몬스터 레벨 갱신
        //        _monster[System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["monNumber"].ToString())].exp = System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["exp"].ToString()); // 보유 몬스터 경험치랑 갱신

        //        for (int j = 1; j < _monster[System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["monNumber"].ToString())].level; j++) // 몬스터 레벨의 따른 경험치량정해지면 거기서 받아와야함. 이건 임시..
        //        {
        //            _monster[System.Int32.Parse(ServerInterface.Instance.userState.ownMonster[i]["monNumber"].ToString())].needExp *= 2;
        //        }
        //    }
        //}

    }
    void UserSkillInit()
    {

        skillNone = new UserSkill();
        skillNone.SkillName = "None";
        skillNone.SKillInfo = "";
        skillNone.SkillNumber = "-1";
        skillNone.SkillExist = false;
        skillNone.SetSkillSource(0);

        _skill[0] = new UserSkill();
        _skill[0].SkillName = "Thunder Storm";
        _skill[0].SKillInfo = "강력한 번개 폭풍으로 적들에게 피해를 입힌다.";
        _skill[0].SkillNumber = "00";
        _skill[0].SkillExist = true;
        _skill[0].SetSkillSource(90);

        _skill[1] = new UserSkill();
        _skill[1].SkillName = "stim pack";
        _skill[1].SKillInfo = "몬스터들의 이동속도와 공격속도를 일시적으로 상승시킨다.";
        _skill[1].SkillNumber = "01";
        _skill[1].SkillExist = true;
        _skill[1].SetSkillSource(60);

        _skill[2] = new UserSkill();
        _skill[2].SkillName = "all mighty";
        _skill[2].SKillInfo = "아군을 일시적으로 무적상태로 만든다.";
        _skill[2].SkillNumber = "02";
        _skill[2].SkillExist = true;
        _skill[2].SetSkillSource(80);

        _skill[3] = new UserSkill();
        _skill[3].SkillName = "time stop";
        _skill[3].SKillInfo = "적들의 공격을 일시적으로 멈춘다.";
        _skill[3].SkillNumber = "03";
        _skill[3].SkillExist = true;
        _skill[3].SetSkillSource(70);

        _skill[4] = new UserSkill();
        _skill[4].SkillName = "Double Product";
        _skill[4].SKillInfo = "몬스터를 일시적으로 2배씩 소환한다..";
        _skill[4].SkillNumber = "04";
        _skill[4].SkillExist = true;
        _skill[4].SetSkillSource(60);




    }






    void MonsterInit()
    {

        monsterNone = new Monster();
        monsterNone.monsterName = "None";
        monsterNone.monsterInfo = "";
        monsterNone.monsterNumber = "-1";
        monsterNone.monsterExist = false;
        monsterNone.SetUpgradeSource(0 ,0 , 0);
        monsterNone.SetGameSource(0);

        _monster[0] = new Monster();
        _monster[0].monsterName = "pink stone";
        //_monster[0].monsterInfo = "인류는 이 친숙하고 귀엽게 생긴 몬스터를 슬라임이라고 이름 지었다. 인류는 책이나 게임을 통해서 너무나도 친숙하게 봐왔던 슬라임과 모습이 비슷했기 때문이다. 하지만 겉모습과 다르게 슬라임은 말랑말랑하지 않았다. 이들의 머리는 마치 커다란 바위와 같고, 왠만한 강철은 다 뭉개버릴 정도로 강력한 박치기를 한다.";
        _monster[0].monsterInfo = "슬라임이라는 겉모습과 다르게 머리는 커다란 바위와 같고 강력한 박치기를 한다.";
        _monster[0].monsterNumber = "00";
        _monster[0].monsterExist = true;
        _monster[0].monsterAttribute = 0;       //불속성
        _monster[0].SetUpgradeSource(10, 0, 0);
        _monster[0].SetGameSource(20);
        
        monsterPublicInfo[0].name = _monster[0].monsterName;
        monsterLevelInfo[0].Hp = 100;
        monsterLevelInfo[0].speed = 44;
        monsterLevelInfo[0].attackGround = 20;
        monsterLevelInfo[0].attackAir = 0;
        //PlayerPrefs.SetInt("monster0_level", 0);



        _monster[1] = new Monster();
        _monster[1].monsterName = "kick man";
        //_monster[1].monsterInfo = "인류는 처음 이 몬스터를 봤을 때 당황할 수 밖에 없었다. 마치 태권도나 가라데를 하는 듯한 현란한 발차기를 했기 때문이다. 멀리서 그냥 보기에는 멋있지만 막상 이 몬스터 앞에 서게 되면 그 자리를 피하고 싶을 것이다.";
        _monster[1].monsterInfo = "강철같은 다리의 힘으로 현란한 발차기를 하는 몬스터이다.";
        _monster[1].monsterNumber = "01";
        _monster[1].monsterExist = true;
        _monster[1].monsterAttribute = 1;       //물속성
        _monster[1].SetUpgradeSource(0, 10, 0);
        _monster[1].SetGameSource(25);
        monsterLevelInfo[1].Hp = 100;
        monsterLevelInfo[1].speed = 44;
        monsterLevelInfo[1].attackGround = 20;
        monsterLevelInfo[1].attackAir = 0;



        _monster[2] = new Monster();
        _monster[2].monsterName = "flying stone";
        //_monster[2].monsterInfo = "생체 구조상 슬라임에서 진화한 것으로 보인다. 이들은 공중의 전투기나 헬기를 향해 돌진하여 들이 받는다. 비행 조종사들에게 공포의 대상이며 공격시 굉음을 낸다.";
        _monster[2].monsterInfo = "비행능력을 가진 슬라임으로 진화한 것으로 보인다. 이들은 공중의 전투기나 헬기를 향해 돌진하여 들이 받는다.";
        _monster[2].monsterNumber = "02";
        _monster[2].monsterExist = true;
        _monster[2].monsterAttribute = 0;       //불속성
        _monster[2].SetUpgradeSource(10, 0, 0);
        _monster[2].SetGameSource(30);
        monsterLevelInfo[2].Hp = 100;
        monsterLevelInfo[2].speed = 44;
        monsterLevelInfo[2].attackGround = 20;
        monsterLevelInfo[2].attackAir = 0;


        _monster[3] = new Monster();
        _monster[3].monsterName = "ground piranha";
        //_monster[3].monsterInfo = "뿔달린 소악마로 불리는 이 노란 몬스터는 매우 강한 턱과 이빨을 가졌다. 마치 무엇이든 다 절단할 수 있는 절단기를 연상시킨다. 이들이 턱에서 내는 소리는 특유의 리듬감을 가지고 있으며 다른 몬스터들에게 더 빠르게 움직이게 하는 어떤 효과가 있는 것 같다.";
        _monster[3].monsterInfo = "뿔달린 소악마로 불리는 이 노란 몬스터는 매우 강한 턱과 이빨을 가졌다. 무엇이든 다 절단할 수 있다";
        _monster[3].monsterNumber = "03";
        _monster[3].monsterExist = true;
        _monster[3].monsterAttribute = 2;       //풀속성
        _monster[3].SetUpgradeSource(0, 0, 10);
        _monster[3].SetGameSource(40);
        monsterLevelInfo[3].Hp = 100;
        monsterLevelInfo[3].speed = 44;
        monsterLevelInfo[3].attackGround = 20;
        monsterLevelInfo[3].attackAir = 0;


        _monster[4] = new Monster();
        _monster[4].monsterName = "eye blue";
        //_monster[4].monsterInfo = "아이블루는 인류에게 매우 큰 연구거리이자 공포의 대상이다. 그들은 전자기파를 발생시키며, 전기적인 공격을 자유자제로 할 수 있다. 원거리에서 전자빔을 쏘는데 마치 인류의 강력한 무기인 양자포를 연상시킨다.";
        _monster[4].monsterInfo = "아이블루는 전자기파를 발생시키며, 전기적인 공격을 자유자제로 할 수 있다";
        _monster[4].monsterNumber = "04";
        _monster[4].monsterExist = true;
        _monster[4].monsterAttribute = 0;       //불속성
        _monster[4].SetUpgradeSource(10, 0, 0);
        _monster[4].SetGameSource(40);
        monsterLevelInfo[4].Hp = 100;
        monsterLevelInfo[4].speed = 44;
        monsterLevelInfo[4].attackGround = 20;
        monsterLevelInfo[4].attackAir = 0;


        _monster[5] = new Monster();
        _monster[5].monsterName = "running tree";
        //_monster[5].monsterInfo = "멀리서 가만히 서 있는 모습을 보면 마치 크리스마스 트리를 연상시킨다. 평소에는 움직임이 없기 때문에 특이한 나무 같은 종류로 착각을 할 수 있지만 근처에 적이 나타나면 엄청난 속도로 달려들어 자폭 공격을 한다.";
        _monster[5].monsterInfo = "움직임이 없는 특이한 나무 로 착각을 할 수 있지만 적을 향해 달려들어 자폭 공격을 한다.";
        _monster[5].monsterNumber = "05";
        _monster[5].monsterExist = false;
        _monster[5].monsterAttribute = 2;       //풀속성
        _monster[5].SetUpgradeSource(0, 0, 10);
        _monster[5].SetGameSource(45);
        monsterLevelInfo[5].Hp = 100;
        monsterLevelInfo[5].speed = 44;
        monsterLevelInfo[5].attackGround = 20;
        monsterLevelInfo[5].attackAir = 0;


        _monster[6] = new Monster();
        _monster[6].monsterName = "jumping redball";
        //_monster[6].monsterInfo = "뒷면을 보면 농구공으로 착각을 할 수 있지만 이들을 무시무시한 이빨과 강력한 턱을 가지고 있는 몬스터이다. 적을 향해 점프를 해서 튀어 오는데 방심하면 심각한 타격을 받을 수 있느니 조심해야 한다.";
        _monster[6].monsterInfo = "농구공으로 착각을 할 수 있지만 무시무시한 이빨과 강력한 턱을 가지고 있는 몬스터이다";
        _monster[6].monsterNumber = "06";
        _monster[6].monsterExist = true;
        _monster[6].monsterAttribute = 0;       //불속성
        _monster[6].SetUpgradeSource(10, 0, 0);
        _monster[6].SetGameSource(45);
        monsterLevelInfo[6].Hp = 100;
        monsterLevelInfo[6].speed = 44;
        monsterLevelInfo[6].attackGround = 20;
        monsterLevelInfo[6].attackAir = 0;



        _monster[7] = new Monster();
        _monster[7].monsterName = "monster plant";
        //_monster[7].monsterInfo = "식인식물은 과연 존재했다. 이 식인 식물 몬스터는 심지어 걸어다닌다. 간혹 크리티컬 공격으로 적을 잡아먹는데 매우 치명적이다. 잡아먹은 적을 소화 시켜야 하기 때문에 바로 다시 잡아먹지는 못하지만 이 몬스터를 보면 가까이 가지 않는 것이 현명하다. 머리로 들이 받거나 공격도 치명적이니 주의해야 한다.";
        _monster[7].monsterInfo = "이 식인식물 몬스터는 걸어다니며 머리로 들이 받거나 치명적인 공격을 한다";
        _monster[7].monsterNumber = "07";
        _monster[7].monsterExist = true;
        _monster[7].monsterAttribute = 2;       //풀속성
        _monster[7].SetUpgradeSource(0, 0, 10);
        _monster[7].SetGameSource(60);
        monsterLevelInfo[7].Hp = 100;
        monsterLevelInfo[7].speed = 44;
        monsterLevelInfo[7].attackGround = 20;
        monsterLevelInfo[7].attackAir = 0;


        _monster[8] = new Monster();
        _monster[8].monsterName = "boxer";
        //_monster[8].monsterInfo = "발차기에 이어 복싱이다. 어떻게 된 영문인지 이 몬스터의 공격은 인류의 복싱 선수와 유사한 면이 많다. 손의 피부조직은 피부가 전혀 없고 뼈에 가깝다. 공격이 매우 빠르고 매서우니 굉장히 조심해야 한다.";
        _monster[8].monsterInfo = "인류의 복싱을 배운 이 몬스터는 강철같은 단단한 주먹으로 빠르고 매서운 공격을 한다.";
        _monster[8].monsterNumber = "08";
        _monster[8].monsterExist = true;
        _monster[8].monsterAttribute = 0;       //불속성
        _monster[8].SetUpgradeSource(10, 0, 0);
        _monster[8].SetGameSource(65);
        monsterLevelInfo[8].Hp = 100;
        monsterLevelInfo[8].speed = 44;
        monsterLevelInfo[8].attackGround = 20;
        monsterLevelInfo[8].attackAir = 0;


        _monster[9] = new Monster();
        _monster[9].monsterName = "acid crown";
        _monster[9].monsterInfo = "왕관과 같은 모양의 몬스터로 강한 농도의 산성 공격을 하여 적을 녹여버린다.";
        _monster[9].monsterNumber = "09";
        _monster[9].monsterExist = true;
        _monster[9].monsterAttribute = 1;       //물속성
        _monster[9].SetUpgradeSource(0, 10, 0);
        _monster[9].SetGameSource(40);
        monsterLevelInfo[9].Hp = 100;
        monsterLevelInfo[9].speed = 44;
        monsterLevelInfo[9].attackGround = 20;
        monsterLevelInfo[9].attackAir = 0;


        _monster[10] = new Monster();
        _monster[10].monsterName = "boom";
        _monster[10].monsterInfo = "우주의 폭발과 함께 태어난 몬스터로 단단한 몸체를 바탕으로 강력한 근접공격을 한다.";
        _monster[10].monsterNumber = "10";
        _monster[10].monsterExist = true;
        _monster[10].monsterAttribute = 2;       //풀속성
        _monster[10].SetUpgradeSource(0, 0, 10);
        _monster[10].SetGameSource(50);
        monsterLevelInfo[10].Hp = 100;
        monsterLevelInfo[10].speed = 44;
        monsterLevelInfo[10].attackGround = 20;
        monsterLevelInfo[10].attackAir = 0;


        _monster[11] = new Monster();
        _monster[11].monsterName = "white wolverine";
        //_monster[11].monsterInfo = "설인 같은 이미지를 가진 몬스터의 이름은 인류가 좋아하는 고전 만화 울버린에서 가져왔다. 선량해 보이는 모습과 달리 매우 잔인한 공격을 하는데 그 공격은 어느 몬스터의 공격보다도 강력하다.";
        _monster[11].monsterInfo = "설인 같은 이미지를 가진 몬스터로 선량해 보이는 모습과 달리 날카로운 발톱으로 공격한다.";
        _monster[11].monsterNumber = "11";
        _monster[11].monsterExist = false;
        _monster[11].monsterAttribute = 2;       //풀속성
        _monster[11].SetUpgradeSource(0, 0, 10);
        _monster[11].SetGameSource(50);
        monsterLevelInfo[11].Hp = 100;
        monsterLevelInfo[11].speed = 44;
        monsterLevelInfo[11].attackGround = 20;
        monsterLevelInfo[11].attackAir = 0;


        _monster[12] = new Monster();
        _monster[12].monsterName = "bump bird";
        //_monster[12].monsterInfo = "몬스터 중에서 가장 귀여운 모습을 하고 있으나 방심은 금물이다. 이들은 가미가제 특공대를 연상시키는 자살 폭탄 공격을 하는데 그 속도가 굉장히 빠르기 때문에 조심해야 한다.";
        _monster[12].monsterInfo = "하나의 몬스터는 셋으로 분열하였다 이들은 매우 강한 자폭공격을 하는 몬스터이다";
        _monster[12].monsterNumber = "12";
        _monster[12].monsterExist = true;
        _monster[12].monsterAttribute = 1;       //물속성
        _monster[12].SetUpgradeSource(0, 10, 0);
        _monster[12].SetGameSource(50);
        monsterLevelInfo[12].Hp = 100;
        monsterLevelInfo[12].speed = 44;
        monsterLevelInfo[12].attackGround = 20;
        monsterLevelInfo[12].attackAir = 0;


        _monster[13] = new Monster();
        _monster[13].monsterName = "red dragon";
        //_monster[13].monsterInfo = "공룡을 연상시키는 레드드래곤은 강력한 화염공격과 꼬리 공격기 특징이다. 이 몬스터는 다른 몬스터들의 운동 능력을 증진시키는 호르몬을 내뿜는다. 이 호르몬은 공중을 통해 전파되며 인체에 해롭지는 않지만 매우 역겨운 냄새가 난다.";
        _monster[13].monsterInfo = "백악기 공룡의 염색체를 바탕으로 만들어진 몬스터이며 강력한 화염공격과 꼬리 공격기 특징이다";
        _monster[13].monsterNumber = "13";
        _monster[13].monsterExist = true;
        _monster[13].monsterAttribute = 1;       //물속성
        _monster[13].SetUpgradeSource(0, 10, 0);
        _monster[13].SetGameSource(70);
        monsterLevelInfo[13].Hp = 100;
        monsterLevelInfo[13].speed = 44;
        monsterLevelInfo[13].attackGround = 20;
        monsterLevelInfo[13].attackAir = 0;


        _monster[14] = new Monster();
        _monster[14].monsterName = "zero";
        //_monster[14].monsterInfo = "이 몬스터를 목격하고 보고한 공격부대는 모두 전멸을 당했다. 그래서 이 몬스터의 이름은 제로이다. 아직 그 능력에 대해서 모르는 것이 많다. 인류가 두려워하는 몬스터이다.";
        _monster[14].monsterInfo = "이 몬스터를 공격한 부대는 모두 전멸을 당했다. 그래서 이 몬스터의 이름은 제로이다 능력은 알려진 것이 없다";
        _monster[14].monsterNumber = "14";
        _monster[14].monsterExist = true;
        _monster[14].monsterAttribute = 0;       //불속성
        _monster[14].SetUpgradeSource(10, 0, 0);
        _monster[14].SetGameSource(80);
        monsterLevelInfo[14].Hp = 100;
        monsterLevelInfo[14].speed = 44;
        monsterLevelInfo[14].attackGround = 20;
        monsterLevelInfo[14].attackAir = 0;


      

            _monster[15] = monsterNone;
        _monster[16] = monsterNone;
        _monster[17] = monsterNone;
        _monster[18] = monsterNone;
        _monster[19] = monsterNone;
        _monster[20] = monsterNone;
        _monster[21] = monsterNone;
        _monster[22] = monsterNone;
        _monster[23] = monsterNone;
        _monster[24] = monsterNone;
        _monster[25] = monsterNone;
        _monster[26] = monsterNone;
        _monster[27] = monsterNone;
        _monster[28] = monsterNone;
        _monster[29] = monsterNone;
        _monster[30] = monsterNone;
        _monster[31] = monsterNone;
        _monster[32] = monsterNone;
        _monster[33] = monsterNone;
        _monster[34] = monsterNone;
        _monster[35] = monsterNone;
        _monster[36] = monsterNone;
        _monster[37] = monsterNone;
        _monster[38] = monsterNone;
        _monster[39] = monsterNone;
        _monster[40] = monsterNone;
        _monster[41] = monsterNone;
        _monster[42] = monsterNone;
        _monster[43] = monsterNone;
        _monster[44] = monsterNone;
    }

    void EnemyInit()
    {
        
        enemyPublicInfo[0].name = "BigFoot";
        enemyPublicInfo[0].posY = 30;
        enemyPublicInfo[0].unitType = 0;
        enemyPublicInfo[0].attackType = 0;

        enemyPublicInfo[1].name = "Dron";
        enemyPublicInfo[1].posY = 30;
        enemyPublicInfo[1].unitType = 0;
        enemyPublicInfo[1].attackType = 0;

        enemyPublicInfo[2].name = "Tank";
        enemyPublicInfo[2].posY = 30;
        enemyPublicInfo[2].unitType = 0;
        enemyPublicInfo[2].attackType = 0;


        //이후 공격에 관한것들을 초기화 하여 넣어주고 그것을 사용하면된다
        //ex)   enemyLevelInfo[2].Attack = "Tank";
    }


    public static void SourceInit()
    {
        for (int i = 0; i < (int)SOURCE_UPGRADE.TOTAL; ++i)         //업그레이드에 필요한 소스를 초기화 하는 부분이다.
        {
            _srcUpgrade.Add(new CSource());                         //업그레이드 재료 리스트에 CSource객체를 넣는다.
            //_srcUpgrade[i].SetEnergy(0);
        }

        //for (int i = 0; i < entrySlotNum; ++i)   // 임시방편!!!! public static void 를 void로 바꾸면 이 for를 지워야 한다.
        //{
        //    entryName[i] = ServerInterface.Instance.userState.GetEntry(i);
        //}

        for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)        //게임플레이에 필요한 소스를 초기화 하는 부분이다.
        {
            _srcGame.Add(new CSource());        //게임플레이 재료 리스트에 CSource객체를 넣는다.
            _srcGame[i].SetEnergy(0);
        }


        for (int i = 0; i < (int)SOURCE_SKILL.TOTAL; ++i)        //게임플레이에 필요한 소스를 초기화 하는 부분이다.
        {
            _srcSkill.Add(new CSource());        //게임플레이 재료 리스트에 CSource객체를 넣는다.
            _srcSkill[i].SetEnergy(0);
        }

        for (int i = 0; i < (int)SOURCE_ACT.TOTAL; ++i)        //게임플레이에 필요한 소스를 초기화 하는 부분이다.
        {
            _srcAct.Add(new CSource());        //게임플레이 재료 리스트에 CSource객체를 넣는다.
            _srcAct[i].SetEnergy(0);
        }
        
        

        for (int i = 0; i < entrySlotNum; ++i)
        {
            _entrySlot.Add(new Monster());
            if (entryName[i] == -1)
            {
                _entrySlot[i] = monsterNone;
            }
            else
            {
                _entrySlot[i] = _monster[entryName[i]];
            }
        }

        for (int i = 0; i < SkillSlotNum; ++i)
        {
            _skillSlot.Add(new UserSkill());
            if (SkillentryName[i] == -1)
            {
                _skillSlot[i] = skillNone;
            }
            else
            {
                _skillSlot[i] = _skill[SkillentryName[i]];
            }
        }
        _friendEntrySlot = monsterNone;
    }


    // Use this for initialization
    void Start()
    {
        InitData();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
