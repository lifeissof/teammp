﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;


enum NET_ORDER
{
    GET_TEST,
    GET_PUBLIC_MONSTER,         // 1
    GET_PUBLIC_ENEMY,           // 2
    GET_SINGLE_MONSTER,         // 3
    GET_SINGLE_ENEMY,           // 4
    GET_BATCH_LEVEL_MONSTER,    // 5
    GET_BATCH_LEVEL_ENEMY,      // 6
    GET_BATCH_JSON_MONSTER,     // 7
    GET_BATCH_JSON_ENEMY,       // 8
    JOIN_USER,                  // 9
    LOGIN_USER,                 // 10
    GET_USER_DATA,              // 11
    UPDATE_USER_DATA,           // 12
    DELETE_USER_DATA,           // 13
    MODIFY_USER_DATA,           // 14
    GET_RANK,                   // 15
    UPDATE_RANK,                // 16
    DELETE_TOKEN,               // 17
    ADD_USER_DATA,              // 18
    UPDATE_HIVE,         // 잠깐 만들어놓은 enum
    GET_MONSTER_EXIST,
    GET_STAGE_DATA,
    UPDATE_STAGE_DATA,
    GET_ENTRY_MONSTER,
    SET_ENTRY_MONSTER,
    GACHA_MONSTER,
    GET_FRIENDS_MONSTER,
    GET_MONSTER_LEVEL_DATA,
    SET_MONSTER_ABILITY,
    INCREASE_MONSTER_ABILITY,
    GET_GAME_REWARD,
    ALREADY_GACHA,
    TOTAL
}

public class ServerInterface : MonoBehaviour
{
    private static ServerInterface _ins = null; // 클래스의 싱글톤 객체

    private static bool _busy = false;
    private static string _result = "";

    public string _addr = "119.207.205.67"; // 내 아이피 주소
    public string _port = "8080"; // 일단 80인데 바뀔거
    public string _key = "MDAxMDMzODEzNDUwMTAyNzM5NDc4NzAxMjQ5NTE2NDU="; 
    public string _iv = "Mzk0Nzg3MDEyNDk1MTY0NQ==";
 
    public PublicState[] enemyState_P = new PublicState[45];
    public PublicState[] monsterState_P = new PublicState[45];
    public LevelState[] enemyState_L = new LevelState[45];
    public LevelState[] monsterState_L = new LevelState[45];
    public UserData userState = new UserData();   // 변수 이름에 대해 현중이와 얘기해야함
    
    //public UserStageInfo StageInfo = new UserStageInfo();  // <- 이건 위에 userState 안에 따로 생성중이라 주석함.

    public delegate void DataEvent();  // 이벤트 연결을 위한 델리게이터
    public event DataEvent OnTakeData; // 이벤트 핸들러

    public static ServerInterface Instance  // 싱글톤 객체 만들기
    {
        get
        {
            if (null == _ins)
                _ins = FindObjectOfType(typeof(ServerInterface)) as ServerInterface;

            return _ins;
        }
    }

    void Awake()
    {
        userState.bonus = 1999;
        userState.AllocateUserStage();
        userState.AllocateUserEntry();
        DontDestroyOnLoad(this);
    }

    void OnApplicationQuit()
    {
        _ins = null;
    }

    public bool IsBusy()
    {
        return _busy;
    }


    public IEnumerator SendData(int order, string strData) // 통신처리를 위한 코루틴
    {
        if (IsBusy())
        {
            print("Error: ServerInterface is very busy!!");
        }

        _busy = true;
        _result = "";
        
        string strUrl = "http://" + _addr + ":" + _port + "/" + order.ToString() + ".php"; // 웹서버 내에 있는 order에 대한 php코드가 각각 있어야할듯 싶음
        //string strUrl = "http://localhost/SEND_TEST.php";
        WWWForm form = new WWWForm();
        form.AddField("data", strData);
        WWW w = new WWW(strUrl, form);

        yield return w;

        if (w.error != null)
        {
            print("Connection Failed!"); // 통신에러 발생
            print(w.error);
        }
        else
        {
            print(w.text);
            _result = w.text;
            SendMessage("ResultParser", order);
        }
        _busy = false;
    }

    public void UpdateHive(string strHive)
    {
        if (strHive == "friendshipPoint")
        {
            string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"values\":\"friendshipPoint\"}";

            StartCoroutine(SendData((int)NET_ORDER.UPDATE_HIVE, strData));
        }

        else if (strHive == "fractal")
        {
            string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"values\":\"fractal\"}";

            StartCoroutine(SendData((int)NET_ORDER.UPDATE_HIVE, strData));
        }
    }

    public void GetMonsterExist()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\"}";


        StartCoroutine(SendData((int)NET_ORDER.GET_MONSTER_EXIST, strData));
    }

    public void GetPublicMonster()
    {
        StartCoroutine(SendData((int)NET_ORDER.GET_PUBLIC_MONSTER, ""));
    }

    public void GetPublicEnemy()
    {
        StartCoroutine(SendData((int)NET_ORDER.GET_PUBLIC_ENEMY, ""));
    }

    // 레벨과 몬스터 이름으로 정보 얻기 암호화 과정을 거침 // 친구 리더의 몬스터 정보를 얻어오기 위한 함수
    public void GetSingleMonster(int level, string strMonster)
    {
        string strData = "{\"level\":\"" + level.ToString() + "\",\"object\":\"" + strMonster + "\"}";
        strData = SecureManager.SimpleConverterE(strData);
        print(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        print(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        print(strData);

        StartCoroutine(SendData((int)NET_ORDER.GET_SINGLE_MONSTER, strData));
        print(level + " , " + strMonster + " :: " + strData);
    }

    // 레벨과 적 이름으로 정보 얻기 암호화 과정을 거침
    public void GetSingleEnemy(int level, string strEnemy)
    {
        string strData = "{\"level\":\"" + level.ToString() + "\",\"object\":\"" + strEnemy + "\"}";
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.GET_SINGLE_ENEMY, strData));
    }

    public void GetBatchByLevelMonster(int level)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"level\":\"" + level.ToString() + "\"}";
        //strData = SecureManager.SimpleConverterE(strData);
        //strData = SecureManager.SimpleEncrypt(strData);
        //strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.GET_BATCH_LEVEL_MONSTER, strData));
    }

    public void GetBatchByLevelEnemy(int level)
    {
        string strData = "{\"level\":\"" + level.ToString() + "\"}";
        //strData = SecureManager.SimpleConverterE(strData);
        //strData = SecureManager.SimpleEncrypt(strData);
        //strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.GET_BATCH_LEVEL_ENEMY, strData));
    }

    public void JoinUser(string id, string password)
    {
        string strData = "{\"uid\":\"" + id + "\",\"upass\":\"" + password + "\"}";
        print("JoinUser" + strData);
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        print("encrypt" + strData);
        StartCoroutine(SendData((int)NET_ORDER.JOIN_USER, strData));
    }

    public void LoginUser(string uid, string upass)
    {
        string strData = "{\"uid\":\"" + uid + "\",\"upass\":\"" + upass + "\"}";
        print("LoginUser" + strData);
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.LOGIN_USER, strData));
    }

    public void GetUserData()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"attributes\":[\"mac\",\"uid\",\"bonus\",\"curgold\",\"stage\",\"piece\",\"fractal\",\"friendshipPoint\",\"tutorial\",\"score\",\"monster\",\"userStage\",\"entry\",\"entryNumber\"]}"; // 유저 정보 불러들이기 piece 이 변수이름에 대해 고민
        //strData = SecureManager.SimpleConverterE(strData);
        //strData = SecureManager.SimpleEncrypt(strData);
        //strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.GET_USER_DATA, strData));
    }

    public void DeleteUserData(string id)
    {
        string strData = "{\"uid\":\"" + id + "\"}";
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.DELETE_USER_DATA, strData));
    }

    // 유저 데이터 초기화 함수
    public void InitUserData()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"piece\": \"0\", \"fractal\": \"20\", \"friendshipPoint\": \"20\", \"tutirial\": \"0\" }, \"type\":\"PUT\"}"; // DB 스키마 갱신
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void AddMonsterData(int monsterNumber, int level, int exp)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"monster\": [ {\"monNumber\": " + monsterNumber + ", \"level\" : " + level + ", \"exp\" : " + exp + "}, {\"monNumber\": " + monsterNumber + ", \"level\" : " + level + ", \"exp\" : " + exp + "}]}, \"type\":\"ADD\"}"; // DB 스키마 갱신
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void MonsterDataForTutorial()
    {
        string addData = "";
        for (int i = 3; i < 15; i++)
        {
            addData += ",{\"monNumber\": " + i + ", \"level\" : 1, \"exp\" : 0 }";
        }
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"monster\": [ {\"monNumber\": " + 2 + ", \"level\" : 1, \"exp\" :  0}" + addData + "]},\"type\":\"ADD\"}"; //DB 스키마 갱신

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void ModifyMonsterData(int monsterNumber, int level, int exp)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"monster\": [ {\"monNumber\": " + monsterNumber + ", \"level\" : " + level + ", \"exp\" : " + exp + "}]},\"key\":\"monNumber\"}"; // DB 스키마 갱신
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.MODIFY_USER_DATA, strData));
    }

    //public void UpdateMonsterData(int MonsterNumber, int level, int exp)
    //{
    //    string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"monster\": [ {\"monNumber\": " + MonsterNumber + ", \"level\" : " + level + ", \"exp\" : " + exp + "}]},\"type\":\"ADD\"}"; // DB 스키마 갱신
    //    strData = SecureManager.SimpleConverterE(strData);
    //    strData = SecureManager.SimpleEncrypt(strData);
    //    strData = SecureManager.AESEncrypt(strData, _key, _iv);

    //    StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    //}

    public void UpdateEntryNumberData(int entryNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"entryNumber\": " + entryNumber + "},\"type\":\"PUT\"}"; //DB 스키마 갱신
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void ModifyEntryData(int entryNumber, int[] monsterNumber)
    {
        string data = "{\"entryNumber\" : " + 0 + ", \"monNumber\": " + monsterNumber[0] + "}";
        string[] addData = new string[entryNumber];
        for (int i = 1; i < entryNumber; i++)
        {
            addData[i] = ", {\"entryNumber\" : " + i + ", \"monNumber\": " + monsterNumber[i] + "}";
            data += addData[i];
        }
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"entry\": [ " + data + " ]},\"key\":\"entryNumber\"}"; //DB 스키마 갱신

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.MODIFY_USER_DATA, strData));
    }

    public void UpdateEntryData(int entryNumber, int monsterNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\": [ {\"entryNumber\" : " + entryNumber + ", \"monNumber\": " + monsterNumber + " }]},\"type\":\"PUT\"}"; // DB 스키마 갱신

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void GetStageData()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\"}";

        StartCoroutine(SendData((int)NET_ORDER.GET_STAGE_DATA, strData));
    }

    public void AddStageData(int stageNumber, int starNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"userStage\": [ {\"stage\" : " + stageNumber + ",\"starNumber\" : " + starNumber + "}]},\"type\":\"ADD\"}"; //DB 스키마 갱신
        print("=========================================");
        print("ADD STAGE DATA");
        print("=========================================");
        //strData = SecureManager.SimpleConverterE(strData);
        //strData = SecureManager.SimpleEncrypt(strData);
        //strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_STAGE_DATA, strData));
        print(strData);
    }

    public void UpdateStageData(int stageNumber, int starNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"userStage\": [ {\"stage\" : " + stageNumber + ",\"starNumber\" : " + starNumber + "}]},\"type\":\"PUT\"}"; // DB 스키마 갱신
        print("=========================================");
        print("UPDATE STAGE DATA");
        print("=========================================");
        //strData = SecureManager.SimpleConverterE(strData);
        //strData = SecureManager.SimpleEncrypt(strData);
        //strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_STAGE_DATA, strData));
    }

    public void ModifyStageData(int stageNumber, int starNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"userStage\": [ {\"stage\" : " + stageNumber + ",\"starNumber\" : " + starNumber + "}]},\"type\":\"MODIFY\"}"; // DB 스키마 갱신
        print("=========================================");
        print("MODIFY STAGE DATA");
        print("=========================================");
        //strData = SecureManager.SimpleConverterE(strData);
        //strData = SecureManager.SimpleEncrypt(strData);
        //strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_STAGE_DATA, strData));
    }

    public void UpdateTutorialData(int tutorialStep)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"tutirial\": \"" + tutorialStep.ToString() + "\"}, \"type\":\"PUT\"}";
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void UpdateFriendshipPointData(int friendshipPoint, int tutorial)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"friendshipPoint\": \"" + friendshipPoint.ToString() + "\",\"tutorial\": \"" + tutorial.ToString() + "\"}, \"type\":\"PUT\"}"; // DB 스키마 갱신

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void UpdateFractalData(int fractal, int tutorial)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"fractal\": \"" + fractal.ToString() + "\",\"tutorial\": \"" + tutorial.ToString() + "\"}, \"type\":\"PUT\"}"; // DB 스키마 갱신

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void UpdateMaterialData(int pieceNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"piece\": \"" + pieceNumber.ToString() + "\"}, \"type\":\"PUT\"}"; // DB 스키마 갱신
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void UpdateUserDataForResult(int stageNumber, int pieceNumber, int score, int fractal, int tutorial)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\",\"values\":{\"stage\": \"" + stageNumber.ToString() + "\",\"piece\": \"" + pieceNumber.ToString() + "\",\"score\": \"" + score.ToString() + "\",\"fractal\" : \"" + fractal.ToString() + "\",\"tutorial\": \"" + tutorial.ToString() + "\"}, \"type\":\"PUT\"}"; // DB 스키마 갱신
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.UPDATE_USER_DATA, strData));
    }

    public void GetRank()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\"}";
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.GET_RANK, strData));
    }

    public void UpdateRank()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"values\":\"topScore=" + userState.bonus + "\"}";
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.UPDATE_RANK, strData));
    }

    public void DeleteToken()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\"}";
        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);

        StartCoroutine(SendData((int)NET_ORDER.DELETE_TOKEN, strData));
    }

    public void GetBatchByVariableMonster(string strVar)
    {
        string strData = "[{\"object\":\"";
        string strTarget = "";
        for (int i = 0, step = 0; i < strVar.Length; i++)
        {
            if (',' != strVar[i])
            {
                strTarget += strVar[i];
            }
            else
            {
                strData += strTarget;
                strTarget = "";

                if (0 == step++ % 2)
                {
                    strData += "\",\"level\":\"";
                }
                else
                {
                    strData += "\"}";
                    if (i + 1 < strVar.Length)
                    {
                        strData += ",{\"object\":\"";
                    }
                }
            }
        }
        strData += strTarget + "\"}]";

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.GET_BATCH_JSON_MONSTER, strData));
    }

    public void GetBatchByVariableEnemy(string strVar)
    {
        string strData = "[{\"object\":\"";
        string strTarget = "";
        for (int i = 0, step = 0; i < strVar.Length; i++)
        {
            if (',' != strVar[i])
            {
                strTarget += strVar[i];
            }
            else
            {
                strData += strTarget;
                strTarget = "";

                if (0 == step++ % 2)
                {
                    strData += "\",\"level\":\"";
                }
                else
                {
                    strData += "\"}";
                    if (i + 1 < strVar.Length)
                    {
                        strData += ",{\"object\":\"";
                    }
                }
            }
        }
        strData += strTarget + "\"}]";

        strData = SecureManager.SimpleConverterE(strData);
        strData = SecureManager.SimpleEncrypt(strData);
        strData = SecureManager.AESEncrypt(strData, _key, _iv);
        StartCoroutine(SendData((int)NET_ORDER.GET_BATCH_JSON_ENEMY, strData));
    }
    
    public void GetEntryMonster()
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\"}";

        StartCoroutine(SendData((int)NET_ORDER.GET_ENTRY_MONSTER, strData));
    }

    public void SetEntryMonster()
    {
        for (int i = 0; i < DataManage.entrySlotNum; i++)
        {
            userState.SetEntry(i, System.Convert.ToInt32(DataManage._entrySlot[i].monsterNumber));
        }
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"entry00\":\"" + userState.GetEntry(0) + "\", \"entry01\":\"" + userState.GetEntry(1) + "\",  \"entry02\":\"" + userState.GetEntry(2) + "\",  \"entry03\":\"" + userState.GetEntry(3) + "\",  \"entry04\":\"" + userState.GetEntry(4) + "\"}";

        StartCoroutine(SendData((int)NET_ORDER.SET_ENTRY_MONSTER, strData));
    }

    public void GachaMonster(int monsterNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"monsterNumber\":\"" + monsterNumber + "\"}";
        print("Gacha!!" + monsterNumber);
        StartCoroutine(SendData((int)NET_ORDER.GACHA_MONSTER, strData)); 
    }

    public void GetFriendsMonster(string _utoken)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"friendsUtoken\":\"" + _utoken + "\"}";

        StartCoroutine(SendData((int)NET_ORDER.GET_FRIENDS_MONSTER, strData));
    }

    public void GetMonsterLevelData(int _monsterNumber)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"monsterNumber\":\"" + _monsterNumber + "\", \"level\":\"" + monsterState_L[_monsterNumber].Level + "\", \"currentExp\":\"" + monsterState_L[_monsterNumber].currentExp + "\", \"needExp\":\"" + monsterState_L[_monsterNumber].needExp + "\" , \"normalMaterial\":\"" + userState.GetNormalMaterial() + "\", \"rareMaterial\":\"" + userState.GetRareMaterial() + "\" } ";

        StartCoroutine(SendData((int)NET_ORDER.GET_MONSTER_LEVEL_DATA, strData));
    }
    public void SetMonsterAbility(int _monsterNumber, int _hp, int _speed, int _attackGround, int _attackAir, int _level, int _maxLv, int _gradeStar, int _curGold)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"monsterNumber\":\"" + _monsterNumber + "\", \"hp\":\"" + _hp + "\", \"speed\":\"" + _speed + "\", \"attackGround\":\"" + _attackGround + "\", \"attackAir\":\"" + _attackAir + "\", \"level\":\"" + _level + "\", \"maxLv\":\"" + _maxLv + "\", \"gradeStar\":\"" + _gradeStar + "\", \"currentExp\":\"" + monsterState_L[System.Convert.ToInt32(_monsterNumber)].currentExp + "\", \"needExp\":\"" + monsterState_L[System.Convert.ToInt32(_monsterNumber)].needExp + "\", \"fireSource\":\"" + DataManage._srcUpgrade[0].GetEnergy() + "\", \"waterSource\":\"" + DataManage._srcUpgrade[1].GetEnergy() + "\", \"plantSource\":\"" + DataManage._srcUpgrade[2].GetEnergy() + "\", \"curGold\":\"" + _curGold + "\"} ";

        StartCoroutine(SendData((int)NET_ORDER.SET_MONSTER_ABILITY, strData));
    }

    public void IncreaseMonsterAbility(int _monsterNumber, int _hp, int _speed, int _AG, int _AA)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"monsterNumber\":\"" + _monsterNumber + "\", \"hp\":\"" + _hp + "\", \"speed\":\"" + _speed + "\", \"AG\":\"" + _AG + "\", \"AA\":\"" + _AA + "\" } ";

        StartCoroutine(SendData((int)NET_ORDER.INCREASE_MONSTER_ABILITY, strData));
    }

    public void GetGameReward(int _score, int _fireSource, int _waterSource, int _plantSource, int _normalMaterial, int _rareMaterial, int _money)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"score\":\"" + _score + "\", \"fireSource\":\"" + _fireSource + "\", \"waterSource\":\"" + _waterSource + "\", \"plantSource\":\"" + _plantSource + "\", \"normalMaterial\":\"" + _normalMaterial + "\", \"rareMaterial\":\"" + _rareMaterial + "\", \"money\":\"" + _money + "\" } ";

        StartCoroutine(SendData((int)NET_ORDER.GET_GAME_REWARD, strData));
    }
    public void AlreadyGacha(int _monsterNumber, int _attribute, int _addSourceNum)
    {
        string strData = "{\"utoken\":\"" + PlayerPrefs.GetString("utoken") + "\", \"monsterNumber\":\"" + _monsterNumber + "\", \"attribute\":\"" + _attribute + "\", \"addSourceNum\":\"" + _addSourceNum + "\" } ";

        StartCoroutine(SendData((int)NET_ORDER.ALREADY_GACHA, strData));
    }
    private void ResultParser(int order)
    {
        switch (order)
        {
            case (int)NET_ORDER.UPDATE_HIVE:
                ResultUpdateHive();
                break;
            case (int)NET_ORDER.GET_PUBLIC_MONSTER:
                ResultPublicMonster();
                break;
            case (int)NET_ORDER.GET_PUBLIC_ENEMY:
                ResultPublicEnemy();
                break;
            case (int)NET_ORDER.GET_SINGLE_MONSTER:
                ResultSingleMonster();
                break;
            case (int)NET_ORDER.GET_SINGLE_ENEMY:
                ResultSingleEnemy();
                break;
            case (int)NET_ORDER.GET_BATCH_LEVEL_MONSTER:
                ResultBatchByLevelMonster();
                break;
            case (int)NET_ORDER.GET_BATCH_LEVEL_ENEMY:
                ResultBatchByLevelEnemy();
                break;
            case (int)NET_ORDER.GET_BATCH_JSON_MONSTER:
                ResultBatchByVariableMonster();
                break;
            case (int)NET_ORDER.GET_BATCH_JSON_ENEMY:
                ResultBatchByVariableEnemy();
                break;
            case (int)NET_ORDER.JOIN_USER:
                ResultJoinUser();
                break;
            case (int)NET_ORDER.LOGIN_USER:
                ResultLoginUser();
                break;
            case (int)NET_ORDER.GET_USER_DATA:
                ResultGetUserData();
                break;
            case (int)NET_ORDER.UPDATE_USER_DATA:
                ResultUpdateUserData();
                break;
            case (int)NET_ORDER.DELETE_USER_DATA:
                ResultDeleteUserData();
                break;
            case (int)NET_ORDER.MODIFY_USER_DATA:
                ResultModifyUserData();
                break;
            case (int)NET_ORDER.GET_RANK:
                ResultGetRank();
                break;
            case (int)NET_ORDER.UPDATE_RANK:
                ResultUpdateRank();
                break;
            case (int)NET_ORDER.DELETE_TOKEN:
                ResultDeleteToken();
                break;
            case (int)NET_ORDER.ADD_USER_DATA:    //확인 2016.04.08
                ResultAddUserData();
                break;
            case (int)NET_ORDER.GET_MONSTER_EXIST:
                ResultGetMonsterExist();
                break;
            case (int)NET_ORDER.GET_STAGE_DATA:
                ResultGetStageData();
                break;
            case (int)NET_ORDER.UPDATE_STAGE_DATA:
                ResultUpdateStageData();
                break;
            case (int)NET_ORDER.GET_ENTRY_MONSTER:
                ResultGetEntryMonster();
                break;
            case (int)NET_ORDER.SET_ENTRY_MONSTER:
                ResultSetEntryMonster();
                break;
            case (int)NET_ORDER.GACHA_MONSTER:
                ResultGachaMonster();
                break;
            case (int)NET_ORDER.GET_FRIENDS_MONSTER:
                ResultGetFriendsMonster();
                break;
            case (int)NET_ORDER.GET_MONSTER_LEVEL_DATA:
                ResultGetMonsterLevelData();
                break;
            case (int)NET_ORDER.SET_MONSTER_ABILITY:
                ResultSetMonsterAbility();
                break;
            case (int)NET_ORDER.INCREASE_MONSTER_ABILITY:
                ResultIncreaseMonsterAbility();
                break;
            case (int)NET_ORDER.GET_GAME_REWARD:
                ResultGetGameReward();
                break;
            case (int)NET_ORDER.ALREADY_GACHA:
                ResultAlreadyGacha();
                break;
            default:
                print("Unexpected Result!");
                break;
        }
    }

    private void ResultUpdateHive()
    {
        string strData = _result;
        //print(strData);
        JsonData jData = JsonMapper.ToObject(strData);

        //print("fractal: " + jData["fractal"].ToString());
        //print("friendshipPoint: " + jData["friendshipPoint"].ToString());
        //print("----------------------");
        
        userState.SetFractal(System.Convert.ToInt32(jData["fractal"].ToString()));
        //print(userState.GetFractal());
        userState.SetFriendshipPoint(System.Convert.ToInt32(jData["friendshipPoint"].ToString()));
        //print(userState.GetFriendshipPoint());
        
    }

    private void ResultPublicMonster()
    {
        //print(_result);
        SecureManager.SimpleDecrypt(_result);
        print(_result);
        string strData = _result;
        //strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //print(strData);
        //strData = SecureManager.SimpleDecrypt(strData);
        //print(strData);
        //strData = SecureManager.SimpleConverterD(strData);
        //print(strData);
        
        JsonData jData = JsonMapper.ToObject(strData);
        //print(jData[1]["name"].ToString());
        //print(jData[2]["name"].ToString());
        //print(jData[3]["name"].ToString());
        for (int i = 0; i< jData.Count; i++)
        {
            int num = System.Convert.ToInt32(jData[i]["object"].ToString().Substring(3, 2));
            monsterState_P[num] = new PublicState();
            monsterState_P[num].name = jData[i]["name"].ToString();
            monsterState_P[num].unitType = System.Convert.ToInt32(jData[i]["unitType"].ToString());
            monsterState_P[num].attackType = System.Convert.ToInt32(jData[i]["attackType"].ToString());
            monsterState_P[num].posY = System.Convert.ToInt32(jData[i]["posY"].ToString());
            monsterState_P[num].elementType = System.Convert.ToInt32(jData[i]["elementType"].ToString());
            monsterState_P[num].resourceY = System.Convert.ToInt32(jData[i]["resourceY"].ToString());
            //print("--------------");
            //print("num: " + num);
            //print("name: " + monsterState_P[num].name);
            //print("unitType: " + monsterState_P[num].unitType);
            //print("attackType: " + monsterState_P[num].attackType);
            //print("posY: " + monsterState_P[num].posY);
            //print("elementType: " + monsterState_P[num].elementType);
            //print("resorceY: " + monsterState_P[num].resourceY);
        }
    }

    private void ResultPublicEnemy()
    {
        //SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        //strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //strData = SecureManager.SimpleDecrypt(strData);
        //strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
        for (int i = 0; i < jData.Count; i++)
        {
            int num = System.Convert.ToInt32(jData[i]["object"].ToString().Substring(5, 2));
            enemyState_P[num] = new PublicState();
            enemyState_P[num].name = jData[i]["name"].ToString();
            enemyState_P[num].unitType = System.Convert.ToInt32(jData[i]["unitType"].ToString());
            enemyState_P[num].attackType = System.Convert.ToInt32(jData[i]["attackType"].ToString());
            enemyState_P[num].posY = System.Convert.ToInt32(jData[i]["posY"].ToString());

            //print("name: " + enemyState_P[num].name);
            //print("unitType: " + enemyState_P[num].unitType);
            //print("attackType: " + enemyState_P[num].attackType);
            //print("posY: " + enemyState_P[num].posY);

        }
    }

    private void ResultSingleMonster()
    {
        SecureManager.SimpleDecrypt(_result);
        string strData = _result;
        //print("result : " + strData);
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);
        //print("result2 : " + strData);
        JsonData jData = JsonMapper.ToObject(strData);
        //print(jData["level"].ToString());
        int num = System.Convert.ToInt32(jData["object"].ToString().Substring(3, 2));
        //print("num : " + num);
        monsterState_L[num] = new LevelState();
        monsterState_L[num].Level = System.Convert.ToInt32(jData["level"].ToString());
        monsterState_L[num].armor = System.Convert.ToInt32(jData["armor"].ToString());
        monsterState_L[num].MaxHp = System.Convert.ToInt32(jData["hp"].ToString());
        monsterState_L[num].Hp = System.Convert.ToInt32(jData["hp"].ToString());
        monsterState_L[num].speed = System.Convert.ToInt32(jData["speed"].ToString());
        monsterState_L[num].attackRange = System.Convert.ToInt32(jData["attackRange"].ToString());
        monsterState_L[num].attackPoison = System.Convert.ToInt32(jData["attackPoison"].ToString());
        monsterState_L[num].attackPoisonPlus = System.Convert.ToInt32(jData["attackPoisonPlus"].ToString());
        monsterState_L[num].attackAir = System.Convert.ToInt32(jData["attackAir"].ToString());
        monsterState_L[num].attackAirDelay = System.Convert.ToSingle(jData["attackAirDelay"].ToString());
        monsterState_L[num].attackGround= System.Convert.ToInt32(jData["attackGround"].ToString());
        monsterState_L[num].attackGroundDelay = System.Convert.ToSingle(jData["attackGroundDelay"].ToString());
        monsterState_L[num].attackCritical = System.Convert.ToInt32(jData["attackCritical"].ToString());
        monsterState_L[num].attackPercent = System.Convert.ToInt32(jData["attackPercent"].ToString());
        monsterState_L[num].attackPush = System.Convert.ToInt32(jData["attackPush"].ToString());
        monsterState_L[num].movingDelay = System.Convert.ToInt32(jData["movingDelay"].ToString());
        monsterState_L[num].movingTime = System.Convert.ToInt32(jData["movingTime"].ToString());
    }

    private void ResultSingleEnemy()
    {
        //print(_result);
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
       //rint(strData);
        strData = SecureManager.SimpleDecrypt(strData);
        //print(strData);
        strData = SecureManager.SimpleConverterD(strData);
        //print(strData);

        JsonData jData = JsonMapper.ToObject(strData);

        int num = System.Convert.ToInt32(jData["object"].ToString().Substring(5, 2));
        enemyState_L[num] = new LevelState();
        enemyState_L[num].Level = System.Convert.ToInt32(jData["level"].ToString());
        enemyState_L[num].gentime = System.Convert.ToInt32(jData["gentime"].ToString());
        enemyState_L[num].armor = System.Convert.ToInt32(jData["armor"].ToString());
        enemyState_L[num].MaxHp = System.Convert.ToInt32(jData["hp"].ToString());
        enemyState_L[num].Hp = System.Convert.ToInt32(jData["hp"].ToString());
        enemyState_L[num].speed = System.Convert.ToInt32(jData["speed"].ToString());
        enemyState_L[num].attackRange = System.Convert.ToInt32(jData["attackRange"].ToString());
        enemyState_L[num].attackPoison = System.Convert.ToInt32(jData["attackPoison"].ToString());
        enemyState_L[num].attackPoisonPlus = System.Convert.ToInt32(jData["attackPoisonPlus"].ToString());
        enemyState_L[num].attackAir = System.Convert.ToInt32(jData["attackAir"].ToString());
        enemyState_L[num].attackAirDelay = System.Convert.ToSingle(jData["attackAirDelay"].ToString());
        enemyState_L[num].attackGround = System.Convert.ToInt32(jData["attackGround"].ToString());
        enemyState_L[num].attackGroundDelay = System.Convert.ToSingle(jData["attackGroundDelay"].ToString());
        enemyState_L[num].attackCritical = System.Convert.ToInt32(jData["attackCritical"].ToString());
        enemyState_L[num].attackPercent = System.Convert.ToInt32(jData["attackPercent"].ToString());
        enemyState_L[num].attackPush = System.Convert.ToInt32(jData["attackPush"].ToString());
        enemyState_L[num].movingDelay = System.Convert.ToInt32(jData["movingDelay"].ToString());
        enemyState_L[num].movingTime = System.Convert.ToInt32(jData["movingTime"].ToString());
        //print("--------------------------");
        //print(jData["object"] + " / " + jData["level"]);
        //print("armor: " + jData["armor"]);
        //print("hp: " + jData["hp"]);
        //print("speed: " + jData["speed"]);
        //print("attackRange: " + jData["attackRange"]);
        //print("attackPoison: " + jData["attackPoison"]);
        //print("attackPoisonPlus: " + jData["attackPoisonPlus"]);
        //print("attackAir: " + jData["attackAir"]);
        //print("attackAirDelay: " + jData["attackAirDelay"]);
        //print("attackGround: " + jData["attackGround"]);
        //print("attackGroundDelay: " + jData["attackGroundDelay"]);
        //print("attackCritical: " + jData["attackCritical"]);
        //print("attackPercent: " + jData["attackPercent"]);
        //print("attackPercent: " + jData["attackPercent"]);
        //print("movingDelay: " + jData["movingDelay"]);
        //print("movingTime: " + jData["movingTime"]);
    }

    private void ResultBatchByLevelMonster()
    {
        //SecureManager.SimpleDecrypt(_result);

        string strData = _result;

        //strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //strData = SecureManager.SimpleDecrypt(strData);
        //strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
        for (int i = 0; i < jData.Count; i++)
        {
            int num = System.Convert.ToInt32(jData[i]["object"].ToString().Substring(3, 2));
            monsterState_L[num] = new LevelState();
            monsterState_L[num].Level = System.Convert.ToInt32(jData[i]["level"].ToString());
            monsterState_L[num].maxLv = System.Convert.ToInt32(jData[i]["maxLv"].ToString());
            monsterState_L[num].gradeStar = System.Convert.ToInt32(jData[i]["gradeStar"].ToString()); // 16 07 23 몬스터의 별 갯수
            monsterState_L[num].currentExp = System.Convert.ToInt32(jData[i]["currentExp"].ToString()); // 16 07 22 몬스터의 경험치 받아오기 추가
            monsterState_L[num].needExp = System.Convert.ToInt32(jData[i]["needExp"].ToString());
            monsterState_L[num].armor = System.Convert.ToInt32(jData[i]["armor"].ToString());
            monsterState_L[num].MaxHp = System.Convert.ToInt32(jData[i]["Hp"].ToString());
            monsterState_L[num].Hp = System.Convert.ToInt32(jData[i]["Hp"].ToString());
            monsterState_L[num].speed = System.Convert.ToInt32(jData[i]["speed"].ToString());
            monsterState_L[num].attackRange = System.Convert.ToInt32(jData[i]["attackRange"].ToString());
            monsterState_L[num].attackPoison = System.Convert.ToInt32(jData[i]["attackPoison"].ToString());
            monsterState_L[num].attackPoisonPlus = System.Convert.ToInt32(jData[i]["attackPoisonPlus"].ToString());
            monsterState_L[num].attackAir = System.Convert.ToInt32(jData[i]["attackAir"].ToString());
            monsterState_L[num].attackAirDelay = System.Convert.ToSingle(jData[i]["attackAirDelay"].ToString());
            monsterState_L[num].attackGround = System.Convert.ToInt32(jData[i]["attackGround"].ToString());
            monsterState_L[num].attackGroundDelay = System.Convert.ToSingle(jData[i]["attackGroundDelay"].ToString());
            monsterState_L[num].attackCritical = System.Convert.ToInt32(jData[i]["attackCritical"].ToString());
            monsterState_L[num].attackPercent = System.Convert.ToInt32(jData[i]["attackPercent"].ToString());
            monsterState_L[num].attackPush = System.Convert.ToInt32(jData[i]["attackPush"].ToString());
            monsterState_L[num].movingDelay = System.Convert.ToInt32(jData[i]["movingDelay"].ToString());
            monsterState_L[num].movingTime = System.Convert.ToInt32(jData[i]["movingTime"].ToString());
            monsterState_L[num].attribute = System.Convert.ToInt32(jData[i]["attribute"].ToString());

            //print("--------------------------");
            //print(jData[i]["object"] + " / " + jData[i]["level"]);
            //print("armor: " + jData[i]["armor"]);
            //print("hp: " + jData[i]["Hp"]);
            //print("speed: " + jData[i]["speed"]);
            //print("attackrange: " + jData[i]["attackRange"]);
            //print("attackpoison: " + jData[i]["attackPoison"]);
            //print("attackpoisonplus: " + jData[i]["attackPoisonPlus"]);
            //print("attackair: " + jData[i]["attackAir"]);
            //print("attackairdelay: " + jData[i]["attackAirDelay"]);
            //print("attackground: " + jData[i]["attackGround"]);
            //print("attackgrounddelay: " + jData[i]["attackGroundDelay"]);
            //print("attackcritical: " + jData[i]["attackCritical"]);
            //print("attackpercent: " + jData[i]["attackPercent"]);
            //print("movingdelay: " + jData[i]["movingDelay"]);
            //print("movingtime: " + jData[i]["movingTime"]);
        }
    }

    private void ResultBatchByLevelEnemy()
    {
        //SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        //strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //strData = SecureManager.SimpleDecrypt(strData);
        //strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
        for (int i = 0; i < jData.Count; i++)
        {
            int num = System.Convert.ToInt32(jData[i]["object"].ToString().Substring(5, 2));
            enemyState_L[num] = new LevelState();
            enemyState_L[num].Level = System.Convert.ToInt32(jData[i]["level"].ToString());
            enemyState_L[num].gentime = System.Convert.ToInt32(jData[i]["gentime"].ToString());
            enemyState_L[num].armor = System.Convert.ToInt32(jData[i]["armor"].ToString());
            enemyState_L[num].MaxHp = System.Convert.ToInt32(jData[i]["Hp"].ToString());
            enemyState_L[num].Hp = System.Convert.ToInt32(jData[i]["Hp"].ToString());
            enemyState_L[num].speed = System.Convert.ToInt32(jData[i]["speed"].ToString());
            enemyState_L[num].attackRange = System.Convert.ToInt32(jData[i]["attackRange"].ToString());
            enemyState_L[num].attackPoison = System.Convert.ToInt32(jData[i]["attackPoison"].ToString());
            enemyState_L[num].attackPoisonPlus = System.Convert.ToInt32(jData[i]["attackPoisonPlus"].ToString());
            enemyState_L[num].attackAir = System.Convert.ToInt32(jData[i]["attackAir"].ToString());
            enemyState_L[num].attackAirDelay = System.Convert.ToSingle(jData[i]["attackAirDelay"].ToString());
            enemyState_L[num].attackGround = System.Convert.ToInt32(jData[i]["attackGround"].ToString());
            enemyState_L[num].attackGroundDelay = System.Convert.ToSingle(jData[i]["attackGroundDelay"].ToString());
            enemyState_L[num].attackCritical = System.Convert.ToInt32(jData[i]["attackCritical"].ToString());
            enemyState_L[num].attackPercent = System.Convert.ToInt32(jData[i]["attackPercent"].ToString());
            enemyState_L[num].attackPush = System.Convert.ToInt32(jData[i]["attackPush"].ToString());
            enemyState_L[num].movingDelay = System.Convert.ToInt32(jData[i]["movingDelay"].ToString());
            enemyState_L[num].movingTime = System.Convert.ToInt32(jData[i]["movingTime"].ToString());

            //print("--------------------------");
            //print(jData[i]["object"] + " / " + jData[i]["level"]);
            //print("Enemy armor: " + jData[i]["armor"]);
            //print("Enemy Hp: " + jData[i]["Hp"]);
            //print("Enemy speed: " + jData[i]["speed"]);
            //print("Enemy attackRange: " + jData[i]["attackRange"]);
            //print("Enemy attackPoison: " + jData[i]["attackPoison"]);
            //print("Enemy attackPoisonPlus: " + jData[i]["attackPoisonPlus"]);
            //print("Enemy attackAir: " + jData[i]["attackAir"]);
            //print("Enemy attackAirDelay: " + jData[i]["attackAirDelay"]);
            //print("Enemy attackGround: " + jData[i]["attackGround"]);
            //print("Enemy attackGroundDelay: " + jData[i]["attackGroundDelay"]);
            //print("Enemy attackCritical: " + jData[i]["attackCritical"]);
            //print("Enemy attackPercent: " + jData[i]["attackPercent"]);
            //print("Enemy attackPercent: " + jData[i]["attackPercent"]);
            //print("Enemy movingDelay: " + jData[i]["movingDelay"]);
            //print("Enemy movingTime: " + jData[i]["movingTime"]);
        }
    }

    private void ResultBatchByVariableMonster()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
        for(int i = 0; i < jData.Count; i++)
        {
            int num = System.Convert.ToInt32(jData[i]["object"].ToString().Substring(3, 2));
            monsterState_L[num] = new LevelState();
            monsterState_L[num].Level = System.Convert.ToInt32(jData[i]["level"].ToString());
            monsterState_L[num].armor = System.Convert.ToInt32(jData[i]["armor"].ToString());
            monsterState_L[num].MaxHp = System.Convert.ToInt32(jData[i]["hp"].ToString());
            monsterState_L[num].Hp = System.Convert.ToInt32(jData[i]["hp"].ToString());
            monsterState_L[num].speed = System.Convert.ToInt32(jData[i]["speed"].ToString());
            monsterState_L[num].attackRange = System.Convert.ToInt32(jData[i]["attackRange"].ToString());
            monsterState_L[num].attackPoison = System.Convert.ToInt32(jData[i]["attackPoison"].ToString());
            monsterState_L[num].attackPoisonPlus = System.Convert.ToInt32(jData[i]["attackPoisonPlus"].ToString());
            monsterState_L[num].attackAir = System.Convert.ToInt32(jData[i]["attackAir"].ToString());
            monsterState_L[num].attackAirDelay = System.Convert.ToSingle(jData[i]["attackAirDelay"].ToString());
            monsterState_L[num].attackGround = System.Convert.ToInt32(jData[i]["attackGround"].ToString());
            monsterState_L[num].attackGroundDelay = System.Convert.ToSingle(jData[i]["attackGroundDelay"].ToString());
            monsterState_L[num].attackCritical = System.Convert.ToInt32(jData[i]["attackCritical"].ToString());
            monsterState_L[num].attackPercent = System.Convert.ToInt32(jData[i]["attackPercent"].ToString());
            monsterState_L[num].attackPush = System.Convert.ToInt32(jData[i]["attackPush"].ToString());
            monsterState_L[num].movingDelay = System.Convert.ToInt32(jData[i]["movingDelay"].ToString());
            monsterState_L[num].movingTime = System.Convert.ToInt32(jData[i]["movingTime"].ToString());
        }
    }

    private void ResultBatchByVariableEnemy()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
        for (int i = 0; i < jData.Count; i++)
        {
            int num = System.Convert.ToInt32(jData[i]["object"].ToString().Substring(5, 2));
            enemyState_L[num] = new LevelState();
            enemyState_L[num].Level = System.Convert.ToInt32(jData[i]["level"].ToString());
            enemyState_L[num].gentime = System.Convert.ToInt32(jData[i]["gentime"].ToString());
            enemyState_L[num].armor = System.Convert.ToInt32(jData[i]["armor"].ToString());
            enemyState_L[num].MaxHp = System.Convert.ToInt32(jData[i]["hp"].ToString());
            enemyState_L[num].Hp = System.Convert.ToInt32(jData[i]["hp"].ToString());
            enemyState_L[num].speed = System.Convert.ToInt32(jData[i]["speed"].ToString());
            enemyState_L[num].attackRange = System.Convert.ToInt32(jData[i]["attackRange"].ToString());
            enemyState_L[num].attackPoison = System.Convert.ToInt32(jData[i]["attackPoison"].ToString());
            enemyState_L[num].attackPoisonPlus = System.Convert.ToInt32(jData[i]["attackPoisonPlus"].ToString());
            enemyState_L[num].attackAir = System.Convert.ToInt32(jData[i]["attackAir"].ToString());
            enemyState_L[num].attackAirDelay = System.Convert.ToSingle(jData[i]["attackAirDelay"].ToString());
            enemyState_L[num].attackGround = System.Convert.ToInt32(jData[i]["attackGround"].ToString());
            enemyState_L[num].attackGroundDelay = System.Convert.ToSingle(jData[i]["attackGroundDelay"].ToString());
            enemyState_L[num].attackCritical = System.Convert.ToInt32(jData[i]["attackCritical"].ToString());
            enemyState_L[num].attackPercent = System.Convert.ToInt32(jData[i]["attackPercent"].ToString());
            enemyState_L[num].attackPush = System.Convert.ToInt32(jData[i]["attackPush"].ToString());
            enemyState_L[num].movingDelay = System.Convert.ToInt32(jData[i]["movingDelay"].ToString());
            enemyState_L[num].movingTime = System.Convert.ToInt32(jData[i]["movingTime"].ToString());
        }
    }

    private void ResultJoinUser()
    {
        //print(_result);
        SecureManager.SimpleDecrypt(_result);
        //print(_result);
        string strData = _result;
        //print(strData);
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //print(strData);
        strData = SecureManager.SimpleDecrypt(strData);
        //print(strData);
        strData = SecureManager.SimpleConverterD(strData);
        //print(strData);
        JsonData jData = JsonMapper.ToObject(strData);

        userState.utoken = jData["utoken"].ToString();
        PlayerPrefs.SetString("utoken", userState.utoken);
        userState.uid = jData["uid"].ToString();
        PlayerPrefs.SetString("uid", userState.uid);
       
        //GetUserData();
    }

    private void ResultLoginUser()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);
        print(strData); // utoken을 리턴

        JsonData jData = JsonMapper.ToObject(strData);
        print("LoginSuccess");
    }

    private void ResultGetUserData()
    {
        //SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        //strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //strData = SecureManager.SimpleDecrypt(strData);
        //strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);

        userState.uid = jData[0]["uid"].ToString();
        userState.mac = jData[0]["mac"].ToString();
        userState.bonus = System.Convert.ToInt32(jData[0]["bonus"].ToString());
        userState.SetFractal(System.Convert.ToInt32(jData[0]["fractal"].ToString()));
        userState.SetFriendshipPoint(System.Convert.ToInt32(jData[0]["friendshipPoint"].ToString()));
        userState.SetEntryNumber(System.Convert.ToInt32(jData[0]["entryNumber"].ToString()));
        userState.SetGold(System.Convert.ToInt32(jData[0]["curgold"].ToString()));
        userState.SetNormalMaterial(System.Convert.ToInt32(jData[0]["normalMaterial"].ToString()));
        userState.SetRareMaterial(System.Convert.ToInt32(jData[0]["rareMaterial"].ToString()));
        DataManage._srcUpgrade[0].SetEnergy(System.Convert.ToInt32(jData[0]["fireSource"].ToString()));
        DataManage._srcUpgrade[1].SetEnergy(System.Convert.ToInt32(jData[0]["waterSource"].ToString()));
        DataManage._srcUpgrade[2].SetEnergy(System.Convert.ToInt32(jData[0]["plantSource"].ToString()));

        OnTakeData();
    }

    private void ResultUpdateUserData()
    {
        //SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        //strData = SecureManager.AESDecrypt(strData, _key, _iv);
        //strData = SecureManager.SimpleDecrypt(strData);
        //strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);

        //userState.uid = jData["uid"].ToString();
        //userState.mac = jData["mac"].ToString();
        //userState.bonus = System.Convert.ToInt32(jData["bonus"].ToString());
        userState.SetStage(System.Convert.ToInt32(jData["stage"].ToString()));
        userState.SetFractal(System.Convert.ToInt32(jData["fractal"].ToString()));
        userState.SetFriendshipPoint(System.Convert.ToInt32(jData["friendshipPoint"].ToString()));
        //userState.SetScore(System.Convert.ToInt32(jData["score"].ToString()));
        //PlayerPrefs.SetInt("tutorialForStep", System.Convert.ToInt32(jData["tutorial"].ToString()));
        //userState.ownMonster = jData["monster"];

        for (int i = 0; i < jData["userStage"].Count; i++)
        {
            userState.userStage.SetStarNumber(i, System.Convert.ToInt32(jData["userStage"][i]["starNumber"].ToString()));
        }
        userState.SetEntryNumber(System.Convert.ToInt32(jData["entryNumber"].ToString()));
        for (int i = 0; i < jData["entry"].Count; i++)
        {
            userState.SetEntry(i, System.Convert.ToInt32(jData["entry"][i]["monNumber"].ToString()));
        }
        
    }

    private void ResultDeleteUserData()
    {
        PlayerPrefs.DeleteKey("utoken");
        ServerInterface.Instance.JoinUser("id", "password");
    }

    private void ResultModifyUserData()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);

        userState.uid = jData["uid"].ToString();
        userState.mac = jData["mac"].ToString();
        userState.bonus = System.Convert.ToInt32(jData["bonus"].ToString());
        userState.SetStage(System.Convert.ToInt32(jData["stage"].ToString()));
        userState.SetFractal(System.Convert.ToInt32(jData["fractal"].ToString()));
        userState.SetFriendshipPoint(System.Convert.ToInt32(jData["friendshipPoint"].ToString()));
        userState.SetScore(System.Convert.ToInt32(jData["score"].ToString()));
        PlayerPrefs.SetInt("tutorialForStep", System.Convert.ToInt32(jData["tutorial"].ToString()));
        userState.ownMonster = jData["monster"];

        for (int i = 0; i < jData["userStage"].Count; i++)
        {
            userState.userStage.SetStarNumber(i, System.Convert.ToInt32(jData["userStage"][i]["starNumber"].ToString()));
        }
        userState.SetEntryNumber(System.Convert.ToInt32(jData["entryNumber"].ToString()));
        for (int i = 0; i < jData["entry"].Count; i++)
        {
            userState.SetEntry(i, System.Convert.ToInt32(jData["entry"][i]["monNumber"].ToString()));
        }
    }

    private void ResultAddUserData()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);
        print(strData);

        JsonData jData = JsonMapper.ToObject(strData);
        
        userState.uid = jData["uid"].ToString();
        userState.mac = jData["mac"].ToString();
        userState.bonus = System.Convert.ToInt32(jData["bonus"].ToString());

        userState.SetStage(System.Convert.ToInt32(jData["stage"].ToString()));
        userState.SetFractal(System.Convert.ToInt32(jData["fractal"].ToString()));
        userState.SetFriendshipPoint(System.Convert.ToInt32(jData["friendshipPoint"].ToString()));
        userState.SetScore(System.Convert.ToInt32(jData["score"].ToString()));
        PlayerPrefs.SetInt("tutorialForStep", System.Convert.ToInt32(jData["tutorial"].ToString()));
        userState.ownMonster = jData["monster"];

        for (int i = 0; i < jData["userStage"].Count; i++)
        {
            userState.userStage.SetStarNumber(i, System.Convert.ToInt32(jData["userStage"][i]["starNumber"].ToString()));
        }
        userState.SetEntryNumber(System.Convert.ToInt32(jData["entryNumber"].ToString()));
        for (int i = 1; i < System.Convert.ToInt32(jData["entryNumber"].ToString()) - 1; i++)
        {
            userState.SetEntry(i, System.Convert.ToInt32(jData["entry"][i]["monNumber"].ToString()));
        }
    }

    private void ResultGetRank()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
    }

    private void ResultUpdateRank()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);

        JsonData jData = JsonMapper.ToObject(strData);
    }

    private void ResultDeleteToken()
    {
        SecureManager.SimpleDecrypt(_result);

        string strData = _result;
        strData = SecureManager.AESDecrypt(strData, _key, _iv);
        strData = SecureManager.SimpleDecrypt(strData);
        strData = SecureManager.SimpleConverterD(strData);

        PlayerPrefs.SetString("utoken", "NoData");
        if (PlayerPrefs.GetString("utoken") == "NoData")
            print("NoData");
    }
    private void ResultGetMonsterExist()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);
        for (int i = 0; i < 15; i++)
        {
            //print(jData[0][string.Format("mon{0}", i)].ToString());
            DataManage._monster[i].monsterExist = System.Convert.ToBoolean(jData[0][string.Format("mon{0}", i)].ToString());
        }

    }

    private void ResultGetStageData()
    {
        string strData = _result;
        int checkBig = 0;
        int tempStage;
        JsonData jData = JsonMapper.ToObject(strData);
        
        for (int i = 0; i < jData.Count; i++)
        {
            if(checkBig <= System.Convert.ToInt32(jData[i]["stage"].ToString()))
            { 
                userState.SetStage(System.Convert.ToInt32(jData[i]["stage"].ToString()));
            }
        }

        for (int i = 0; i < jData.Count; i++)
        {

            if (System.Convert.ToInt32(jData[i]["stage"].ToString()) < 116)
            {
                tempStage = System.Convert.ToInt32(jData[i]["stage"].ToString()) - 101;
            }
            else if(System.Convert.ToInt32(jData[i]["stage"].ToString()) < 216)
            {
                tempStage = System.Convert.ToInt32(jData[i]["stage"].ToString()) - 186;
            }
            else if (System.Convert.ToInt32(jData[i]["stage"].ToString()) < 316)
            {
                tempStage = System.Convert.ToInt32(jData[i]["stage"].ToString()) - 271;
            }
            else if (System.Convert.ToInt32(jData[i]["stage"].ToString()) < 416)
            {
                tempStage = System.Convert.ToInt32(jData[i]["stage"].ToString()) - 356;
            }
            else
            {
                tempStage = System.Convert.ToInt32(jData[i]["stage"].ToString()) - 441;               
            }
            if ((jData[i]["starNumber"]) != null)
            {
                userState.userStage.SetStarNumber(tempStage, System.Convert.ToInt32(jData[i]["starNumber"].ToString()));
            }
            
        }
    
    }

    private void ResultUpdateStageData()
    {
        
    }

    private void ResultGetEntryMonster()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);
        for (int i = 0; i < System.Convert.ToInt32(jData[0]["entryNumber"].ToString()); i++)
        {
            userState.SetEntry(i, System.Convert.ToInt32(jData[0][string.Format("entry0{0}monsterNumber", i)].ToString()));
        }

    }

    private void ResultSetEntryMonster()
    {

    }

    private void ResultGachaMonster()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);

        DataManage._monster[System.Convert.ToInt32(jData[0]["monsterNumber"].ToString())].monsterExist = System.Convert.ToBoolean(jData[0][string.Format("mon{0}", jData[0]["monsterNumber"].ToString())].ToString());
    }
    
    private void ResultGetFriendsMonster()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);
        
        DataManage._friendEntrySlot.monsterNumber = jData[0]["monsterNumber"].ToString();

        // 친구 몬스터의 정보를 로컬에 저장해야 한다. 능력치 등등
    }

    private void ResultGetMonsterLevelData()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);
        
        int num = System.Convert.ToInt32(jData[0]["object"].ToString().Substring(3, 2));
        monsterState_L[num].Level = System.Convert.ToInt32(jData[0]["level"].ToString());
        monsterState_L[num].currentExp = System.Convert.ToInt32(jData[0]["currentExp"].ToString());
        monsterState_L[num].needExp = System.Convert.ToInt32(jData[0]["needExp"].ToString());
        userState.SetNormalMaterial(System.Convert.ToInt32(jData[0]["normalMaterial"].ToString()));
        userState.SetRareMaterial(System.Convert.ToInt32(jData[0]["rareMaterial"].ToString()));
        print("Success Level Data!!!!!!");
    }

    // 등급 업 후 몬스터의 능력을 최신화 하는 과정
    private void ResultSetMonsterAbility()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);

        int num = System.Convert.ToInt32(jData[0]["object"].ToString().Substring(3, 2));
        monsterState_L[num].Hp = System.Convert.ToInt32(jData[0]["Hp"].ToString());
        monsterState_L[num].Level = System.Convert.ToInt32(jData[0]["level"].ToString());
        monsterState_L[num].maxLv = System.Convert.ToInt32(jData[0]["maxLv"].ToString());
        monsterState_L[num].gradeStar = System.Convert.ToInt32(jData[0]["gradeStar"].ToString());
        monsterState_L[num].speed = System.Convert.ToInt32(jData[0]["speed"].ToString());
        monsterState_L[num].attackAir = System.Convert.ToInt32(jData[0]["attackAir"].ToString());
        monsterState_L[num].attackGround = System.Convert.ToInt32(jData[0]["attackGround"].ToString());

        monsterState_L[num].currentExp = System.Convert.ToInt32(jData[0]["currentExp"].ToString());
        monsterState_L[num].needExp = System.Convert.ToInt32(jData[0]["needExp"].ToString());

        userState.SetGold(System.Convert.ToInt32(jData[0]["curGold"].ToString()));

        print("Success Ability!!!!!!");
    }

    private void ResultIncreaseMonsterAbility()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);


        int num = System.Convert.ToInt32(jData[0]["object"].ToString().Substring(3, 2));
        
        monsterState_L[num].Hp = System.Convert.ToInt32(jData[0]["Hp"].ToString());
        monsterState_L[num].speed = System.Convert.ToInt32(jData[0]["speed"].ToString());
        monsterState_L[num].attackGround = System.Convert.ToInt32(jData[0]["attackGround"].ToString());
        monsterState_L[num].attackAir = System.Convert.ToInt32(jData[0]["attackAir"].ToString());
        
    }

    // 게임이 끝나고 결과창에 나온 재료들을 서버와 통신 후 다시 최신화 하는 과정
    private void ResultGetGameReward()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);

        userState.SetScore(System.Convert.ToInt32(jData[0]["score"].ToString()));
        DataManage._srcUpgrade[0].SetEnergy(System.Convert.ToInt32(jData[0]["fireMaterial"].ToString()));
        DataManage._srcUpgrade[1].SetEnergy(System.Convert.ToInt32(jData[0]["waterMaterial"].ToString()));
        DataManage._srcUpgrade[2].SetEnergy(System.Convert.ToInt32(jData[0]["plantMaterial"].ToString()));
        userState.SetNormalMaterial(System.Convert.ToInt32(jData[0]["normalMaterial"].ToString()));
        userState.SetRareMaterial(System.Convert.ToInt32(jData[0]["rareMaterial"].ToString()));
        userState.SetGold(System.Convert.ToInt32(jData[0]["money"].ToString()));

        
    }

    // 가챠 시 이미 있는 몬스터를 뽑았을 때 해당 몬스터 속성 재료를 addSourceNum 만큼 증가시킴
    private void ResultAlreadyGacha()
    {
        string strData = _result;

        JsonData jData = JsonMapper.ToObject(strData);

        DataManage._srcUpgrade[0].SetEnergy(System.Convert.ToInt32(jData[0]["fireSource"].ToString()));
        DataManage._srcUpgrade[1].SetEnergy(System.Convert.ToInt32(jData[0]["waterSource"].ToString()));
        DataManage._srcUpgrade[2].SetEnergy(System.Convert.ToInt32(jData[0]["plantSource"].ToString()));
    }
}