﻿using UnityEngine;
using System.Collections;

public class ButtonSystem : MonoBehaviour {

    Ray ray;
    RaycastHit hit;


    public GameObject Target;
    public string Function;
    public string UpSprite;
    public string DownSprite;

    private int UpSpriteID;
    private int DownSpriteID;
    private string prevUpSprite;
    private string prevDownSprite;
    tk2dSprite sprite2d;



	// Use this for initialization
    void Start()
    {
        sprite2d = this.GetComponent<tk2dSprite>();
        GetSpriteID();
    }

    void GetSpriteID()
    {
        if (UpSprite != "None" && UpSprite != null)
        {
            UpSpriteID = sprite2d.GetSpriteIdByName(UpSprite);
            prevUpSprite = UpSprite;
        }
        if (DownSprite != "None" && DownSprite != null)
        {
            DownSpriteID = sprite2d.GetSpriteIdByName(DownSprite);
            prevDownSprite = DownSprite;
        }
    }


    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == this.gameObject && (DownSprite != "None" && DownSprite != null))
            {
                
                if (DownSprite != prevDownSprite)
                {
                    DownSpriteID = sprite2d.GetSpriteIdByName(DownSprite);
                }
                sprite2d.spriteId = DownSpriteID;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (UpSprite != null) //sprite가 없으면 None으로 적어둘 것. null이면 실수로 안넣은것으로 판단.
            {
                if (UpSprite != "None")
                {
                    if (UpSprite != prevUpSprite)
                    {
                        UpSpriteID = sprite2d.GetSpriteIdByName(UpSprite);
                    }
                    sprite2d.spriteId = UpSpriteID;
                }

                if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == this.gameObject)
                {
                    if (Target != null)         //오류
                        Target.SendMessage(Function);
                }
            }
        }
	}
}
