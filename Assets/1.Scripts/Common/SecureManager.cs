﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System;

public class SecureManager : MonoBehaviour
{
    public static string AESEncrypt(string toEncrypt, string key, string iv)
    {
        RijndaelManaged aes = new RijndaelManaged();
        aes.KeySize = 256;
        aes.BlockSize = 128;
        aes.Padding = PaddingMode.PKCS7;
        aes.Key = Convert.FromBase64String(key);
        aes.IV = Convert.FromBase64String(iv);

        ICryptoTransform encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
        byte[] buff = null;
        using (MemoryStream ms = new MemoryStream())
        {
            using (CryptoStream cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
            {
                byte[] byToEncrypt = Encoding.UTF8.GetBytes(toEncrypt);
                cs.Write(byToEncrypt, 0, byToEncrypt.Length);
            }
            buff = ms.ToArray();
        }

        return Convert.ToBase64String(buff);
    }

    public static String AESDecrypt(string toDecrypt, string key, string iv)
    {
        RijndaelManaged aes = new RijndaelManaged();
        aes.KeySize = 256;
        aes.BlockSize = 128;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;
        aes.Key = Convert.FromBase64String(key);
        aes.IV = Convert.FromBase64String(iv);
        ICryptoTransform decrypt = aes.CreateDecryptor();
        byte[] buff = null;
        using (MemoryStream ms = new MemoryStream())
        {
            using (CryptoStream cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
            {
                byte[] byToDecrypt = Convert.FromBase64String(toDecrypt);
                cs.Write(byToDecrypt, 0, byToDecrypt.Length);
            }
            buff = ms.ToArray();
        }

        return Encoding.UTF8.GetString(buff);
    }

    public static string SimpleEncrypt(string str)
    {
        int a = UnityEngine.Random.Range(0, 9);
        int b = UnityEngine.Random.Range(0, 9);
        string strRes = "";
        strRes += a.ToString();
        strRes += b.ToString();

        int count = 0;
        foreach (char c in str)
        {
            int ascii = (int)c;
            if (0 == count % 2)
                strRes += (1 == (a % 2)) ? (char)(ascii + 1) : (char)(ascii - 1);
            else
                strRes += (1 == (b % 2)) ? (char)(ascii + 1) : (char)(ascii - 1);
            count++;
        }
        return strRes;
    }

    public static string SimpleDecrypt(string str)
    {
        int a = 0;
        int b = 0;
        string strRes = "";

        int count = 0;
        foreach (char c in str)
        {
            if (0 == count) { a = c; count++; continue; }
            if (1 == count) { b = c; count++; continue; }

            int ascii = (int)c;
            if (0 == count % 2)
                strRes += (1 == (a % 2)) ? (char)(ascii - 1) : (char)(ascii + 1);
            else
                strRes += (1 == (b % 2)) ? (char)(ascii - 1) : (char)(ascii + 1);
            count++;
        }
        return strRes;
    }

    public static string SimpleConverterE(string str)
    {
        str = str.Replace(",", "$");
        str = str.Replace("[", "!");
        str = str.Replace("]", "#");
        str = str.Replace("{", "^");
        str = str.Replace("}", "|");
        str = str.Replace("\"", "~");
        return str;
    }

    public static string SimpleConverterD(string str)
    {
        str = str.Replace("$", ",");
        str = str.Replace("!", "[");
        str = str.Replace("#", "]");
        str = str.Replace("^", "{");
        str = str.Replace("|", "}");
        str = str.Replace("~", "\"");
        return str;
    }
}
