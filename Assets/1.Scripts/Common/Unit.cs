﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

enum unitState
{
    IDLE,
    ATTACK,
    DIE,
    WALK,
    SEE,
    MEET
}

enum type
{
    GROUND,
    AIR
}

enum attackType
{
    GROUND,
    AIR,
    GROUNDANDAIR
}

enum element
{
    FIRE,
    WATER,
    GRASS
}
enum damageRange
{
    SINGLE,
    SPLASH
}
enum damageType
{
    NORMAL,
    DOT
}
public struct PublicState
{
    public string name;
    public int unitType; //0:ground, 1:sky
    public int posY;
    public int attackType;
    public int elementType; // 0: fire, 1: water , 2: nature
    public int resourceY;

};
public struct LevelState
{
    public int Level;
    public int maxLv;
    public int gradeStar;
    public float currentExp;
    public float needExp;
    public int armor;
    public int MaxHp;
    public int Hp;
    public int attackGround;
    public float attackGroundDelay;
    public int attackAir;
    public float attackAirDelay;
    public int speed;
    public int attackRange;
    public int attackCritical;
    public int attackPercent;
    public int attackPush;
    public int movingDelay;
    public int movingTime;
    public int attackPoison;
    public int attackPoisonPlus;
    public int gentime;
    public int attribute;
};
public class Unit : MonoBehaviour
{

    public Skill mySkill;

    public PublicState state_P;
    public LevelState state_L;

    public float UnitSize;
    public float Atk_Timer;

    public static List<GameObject> _lst_Enemy = new List<GameObject>();
    public static List<GameObject> _lst_Ally = new List<GameObject>();
    public tk2dSpriteAnimator animation;

    public GameObject hpBar;
    public GameObject instanceHpBar;
    public Vector3 hpBarPosition;
    public bool isHpBarExist;

    public int state;
    public bool isAttackPlaying;
    public bool isDotDamaged;
    public float dotTime;
    public int number;

    public AudioClip attackAudio;

    //function
    virtual public void idle() { }
    virtual public void attack() { }
    virtual public IEnumerator die() { yield return 0; }
    virtual public void walk() { }
    virtual public IEnumerator WalkTime() { yield return 0; }
    virtual public IEnumerator WalkDelay() { yield return 0; }
    virtual public IEnumerator Meeting() { yield return 0; }
    virtual public void DotDamage(Unit target) { }
    virtual public void DotDamaged(Unit target, int damage) { }
    virtual public void see() { }
    virtual public IEnumerator meet() { yield return 0; }
    virtual public void setTarget() { }
    virtual public void splash(int number) { }
    virtual public void Ending() { }

    virtual public Skill CreateSkill(string name) { return mySkill; }

    public void UseSkill(Unit enemy, int enemyType, int skillCount)
    {
        int random;
        if (state_P.attackType == (int)attackType.GROUND || state_P.attackType == (int)attackType.AIR)//스킬 중에 랜덤으로 선택해 공격하기
        {
            random = Random.Range(0, skillCount);
            ChoiceSkill(enemy, random);//얘는 enemyType이 중요하지 않음.
        }
        else if (state_P.attackType == (int)attackType.GROUNDANDAIR)
        {
            List<int> toUseSkills = new List<int>();

            for (int i = 0; i < skillCount + 1; i++) //스킬 갯수
            {
                if (enemyType == mySkill.ConfirmAttackType(i)) { toUseSkills.Add(i); } //스킬공격타입과 적 타입 같은 것 골라내기
            }
            random = Random.Range(0, toUseSkills.Count);
            ChoiceSkill(enemy, toUseSkills[random]);//선택한 스킬들 중 랜덤. 쿨타임돌면 다시 스킬 선택하도록 해야함..나중에 필요하면
        }
    }

    public void ChoiceSkill(Unit enemy, int number)
    {
        switch (number)
        {
            case 0:
                StartCoroutine(mySkill.Skill1(enemy, this));
                break;
            case 1:
                StartCoroutine(mySkill.Skill2(enemy, this));
                break;
            case 2:
                StartCoroutine(mySkill.Skill3(enemy, this));
                break;
            case 3:
                StartCoroutine(mySkill.Skill4(enemy, this));
                break;
        }
    }

}


