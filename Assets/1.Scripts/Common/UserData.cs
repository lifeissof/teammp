﻿using UnityEngine;
using System.Collections;
using LitJson;

public class UserStageInfo
{
    public int stageNumber;
    private int[] starNumber;

    public void SetStarNumber(int _stage, int _starNumber) { this.starNumber[_stage] = _starNumber; }
    public int GetStarNumber(int _stage) { return this.starNumber[_stage]; }

    public  UserStageInfo()
    {
        starNumber = new int[75]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

        //for (int i = 0; i < 15; ++i)   // 서버 로 불러들여오면 이 구문은 삭제
        //{
        //   SetStarNumber(i, 3);
        //}
        
    }
}

public class UserMonsterEntry
{
    public int[] MonsterNumber;     //나중에 추가.
}

public struct UserData
{

    public UserStageInfo userStage;
    public JsonData ownMonster;
 
    public string uid;
    public string collectionMonster;
    public string collectionStory;
    public string collectionEnemy;
    //서버에 필요한 내용들.
    public string upass;
    public string mac;
    public string utoken;
    public string item;
    public string nickname;
    public string pushGoogle; 

    public int curGold;
    public int sumGold;
    public int bonus;
    public int conType;
    public int currentTime;
    public int enterTime;
    public int turns;
    public int paidAmount;



    private int normalMaterial;
    private int rareMaterial;
    private int fireMaterial;
    private int waterMaterial;
    private int plantMaterial;
    private int stage;
    private int friendshipPoint;
    private int fractal;
    private int score;
    private int[] stageStarNumber;
    private int[] monsterNumber;
    private int entryNumber;

  

   //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //업그레이드 재료
    public void SetFireMaterial(int _fireMaterial) { this.fireMaterial = _fireMaterial; }
    public int GetFireMaterial() { return this.fireMaterial; }
    public void AddFireMaterial(int _fireMaterial) { this.fireMaterial += _fireMaterial; }

    public void SetWaterMaterial(int _waterMaterial) { this.waterMaterial = _waterMaterial; }
    public int GetWaterMaterial() { return this.waterMaterial; }
    public void AddWaterMaterial(int _waterMaterial) { this.waterMaterial += _waterMaterial; }

    public void SetPlantMaterial(int _plantMaterial) { this.plantMaterial = _plantMaterial; }
    public int GetPlantMaterial() { return this.plantMaterial; }
    public void AddPlantMaterial(int _plantMaterial) { this.plantMaterial += _plantMaterial; }



    //레벨업 재료
    public void SetNormalMaterial(int _nomalMaterial) { this.normalMaterial = _nomalMaterial; }
    public int GetNormalMaterial() { return this.normalMaterial; }
    public void AddNormalMaterial(int _addNormalMaterial) { this.normalMaterial += _addNormalMaterial; }
    public void SetRareMaterial(int _rareMaterial) { this.rareMaterial = _rareMaterial; }
    public int GetRareMaterial() { return this.rareMaterial; }
    public void AddRareMaterial(int _addRareMaterial) { this.rareMaterial += _addRareMaterial; }


    public void SetStage(int _stage) { this.stage = _stage; }
    public int GetStage() { return this.stage; }
    public void SetFriendshipPoint(int _friendshipPoint) { this.friendshipPoint = _friendshipPoint; }
    public int GetFriendshipPoint() { return this.friendshipPoint; }
    public void SetFractal(int _fractal) { this.fractal = _fractal; }
    public int GetFractal() { return this.fractal; }
    public void SetGold(int _gold) { this.curGold = _gold; }
    public int GetGold() { return this.curGold; }
    public void AddGold(int _sumgold) { this.curGold += _sumgold; }


    
    //================================================================================================================//


    public void SetScore(int _score) { this.score = _score; }
    public int GetScore() { return this.score; }
    public void SetEntry(int entryNumber, int _monsterNumber) { this.monsterNumber[entryNumber] = _monsterNumber; }
    public int GetEntry(int entryNumber) { return this.monsterNumber[entryNumber]; }
    public void SetEntryNumber(int _entryNumber) { this.entryNumber = _entryNumber; }
    public int GetEntryNumber() { return this.entryNumber; }

    public void AllocateUserStage()
    {
        userStage = new UserStageInfo();
    }

    public void AllocateUserEntry()
    {
        monsterNumber = new int[5] { 0, -1, -1, -1, -1 };
    }
    
    //public UserData()
    //{
    //    SetStage(2);
    //}

    public void ModifyMonsterData(int MonsterNum , int level , int exp)
    {
        if(DataManage._monster[MonsterNum].monsterExist == true)
        {
            DataManage._monster[MonsterNum].level += level;
            DataManage._monster[MonsterNum].exp += exp;
        }
        else
        {

        }
    }
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
