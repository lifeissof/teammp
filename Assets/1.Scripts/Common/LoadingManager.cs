﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoadingManager : MonoBehaviour {

    public tk2dSprite main;

    public GameObject[] loadingImage;


    float StartTime = 0.0f;


    int loadingImageNum = 0;
    static bool isVictory;

    public LoadingManager(bool _isVictory)      //생성자
    {
        isVictory = _isVictory;
    }
 

    void Awake()
    {
        int id;
        StartCoroutine(loadingGauge());
        //if (Application.loadedLevel == 3)
        if (SceneManager.sceneCount == 3)
        {
            id = main.GetSpriteIdByName("gameStart");
            main.spriteId = id;
        }
        //else if (SceneManager.sceneCount == 5)
        //{
        //    if (isVictory == true)
        //    {
        //        id = main.GetSpriteIdByName("gameStart");
        //    }
        //    else
        //    {
        //        id = main.GetSpriteIdByName("gameOver");
        //    }
        //    main.spriteId = id;
        //}
    }
    // Use this for initialization

    void Start()
    {
        StartCoroutine("Time");
    }

    void Update()
    {
        if(StartTime > 1.0f)
        {
            SceneManager.LoadScene("Game");
        }
    }

    IEnumerator Time()
    {
        yield return new WaitForSeconds(1.0f);
        StartTime += 1.0f;

        StartCoroutine(Time());
    }

    IEnumerator loadingGauge()
    {
        if (loadingImage[loadingImageNum].activeSelf == true)
            loadingImage[loadingImageNum].SetActive(false);
        else
            loadingImage[loadingImageNum].SetActive(true);

        if (loadingImageNum < 7)
            loadingImageNum++;
        else
            loadingImageNum = 0;
        yield return new WaitForSeconds(0.125f);
        StartCoroutine(loadingGauge());
    }

}
