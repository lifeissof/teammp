﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InitChracterInfo : MonoBehaviour {

    public List<GameObject> stars = new List<GameObject>();
    public tk2dTextMesh CharacterLv;
    public tk2dSprite CharacterAttributeSprite;
    private int starCount;
    public string selectNum;

	// Use this for initialization
	void Start () {

        initInfo();
        SetAttributeSprite();
	}

    public void initInfo()
    {

        selectNum = this.gameObject.name.Substring(4,2);

        if (DataManage._monster[System.Convert.ToInt32(selectNum)].monsterExist == true)
        {
            starCount = ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].gradeStar;
            print(starCount);
        }

        for (int i = 0; i < (int)Star.MAX; ++i)
        {
            if (starCount > i) stars[i].GetComponent<tk2dSprite>().SetSprite("star_v");
            else stars[i].GetComponent<tk2dSprite>().SetSprite("star_I");
        }

        CharacterLv.text = "Lv  " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].Level;

    }
    
    void SetAttributeSprite()
    {
        print(selectNum + "num : " + ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute);

        switch(ServerInterface.Instance.monsterState_L[System.Convert.ToInt32(selectNum)].attribute)
        {
            case 0:
                CharacterAttributeSprite.spriteId = CharacterAttributeSprite.GetSpriteIdByName("fireTypeIcon");
                break;

            case 1:
                CharacterAttributeSprite.spriteId = CharacterAttributeSprite.GetSpriteIdByName("waterTypeIcon");
                break;

            case 2:
                CharacterAttributeSprite.spriteId = CharacterAttributeSprite.GetSpriteIdByName("woodTypeIcon");
                break;
                
        }
    }
}
