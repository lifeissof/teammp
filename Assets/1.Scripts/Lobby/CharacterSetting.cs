﻿using UnityEngine;
using System.Collections;
using System.Text;

public class CharacterSetting : MonoBehaviour
{

    public tk2dSprite sprite;
    //   public tk2dSpriteAnimator animator;
    //   public tk2dSpriteAnimation[] animLib = new tk2dSpriteAnimation[15];
    private string num;

    void OnEnable()
    {
        CheckOX();
    }
    void CheckOX()
    {
        if (this.name.Substring(0, 5).Equals("Chara"))
        {
            num = this.name.Substring(9, 2);
            if (DataManage._monster[System.Convert.ToInt32(num)].monsterExist)      //몬스터가 있으면
            {
                if (System.Convert.ToInt32(num) < 15)
                {
                    //  animator.enabled = false;
                    // animator.Library = animLib[System.Convert.ToInt32(num)];
                    int ID_x = sprite.GetSpriteIdByName(string.Format("character{0}", num));
                    sprite.spriteId = ID_x;
                }
            }
            else
            {
                if (System.Convert.ToInt32(num) < 15)
                {
                    //  animator.enabled = false;
                    // animator.Library = animLib[System.Convert.ToInt32(num)];
                    int ID_x = sprite.GetSpriteIdByName(string.Format("character{0}_h", num));
                    sprite.spriteId = ID_x;
                }   
                else
                {
                    // animator.Library = animLib[0];
                    //     animator.Play("Character00");
                    //    animator.enabled = false;
                    //   int ID_x = sprite.GetSpriteIdByName("character00_h");
                    //  sprite.spriteId = ID_x;
                }
            }
        }
    }
}
