﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class StageManager : MonoBehaviour {
    
    public List<GameObject> StageSelectBar = new List<GameObject>();
    
    public GameObject currBar;
    public GameObject LeftBasis;
    public GameObject RightBasis;
    public tk2dSprite BattleField;
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject SoundObject;



    public AudioClip bgm;
    public AudioClip btn;
    public AudioClip btn_arrow;
    public AudioClip btn_play;

    public tk2dFontData StageNum;
    public tk2dFontData StageNum_n;
    public tk2dTextMesh fntTime;
    public tk2dTextMesh fntGameCount;


    public static int currZone;
    public static int currStage;
    public static int trueStage;
    public static int selectZone;
    public static int selectStage;
    public static int[,] InitEnemy = new int [3,3];

    Ray ray;
    RaycastHit hit;



    public List<GameObject> stage0 = new List<GameObject>();
    public List<GameObject> stage1 = new List<GameObject>();
    public List<GameObject> stage2 = new List<GameObject>();
    public List<GameObject> stage3 = new List<GameObject>();
    public List<GameObject> stage4 = new List<GameObject>();
    bool[,] stageOpen = new bool[5, 15] { { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false }, 
                                          { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
                                          { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
                                          { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false },
                                          { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false } };

    float Minutes;
    float Seconds;


    bool isSlidingCheck = false;
    bool isSelectReady = false;

    

	// Use this for initialization
	void Start () {


        Minutes = 5.0f;
        Seconds = 59.0f;
        fntGameCount.text = "x " + DataManage.GameCount.ToString();

        StartCoroutine("Time");

        InitEnemyValue();
        
        

        //currStage = ServerInterface.Instance.userState.GetStage() - currZone;
        currZone = ServerInterface.Instance.userState.GetStage() / 100;
        currStage = ServerInterface.Instance.userState.GetStage() - currZone * 100;
        if (currZone == 1)
        {
            trueStage = ServerInterface.Instance.userState.GetStage() - currZone * 100;
        }
        else if(currZone == 2)
        {
            trueStage = (ServerInterface.Instance.userState.GetStage() - currZone * 100) + 15;
        }
        else if (currZone == 3)
        {
            trueStage = (ServerInterface.Instance.userState.GetStage() - currZone * 100) + 30;
        }
        else if (currZone == 4)
        {
            trueStage = (ServerInterface.Instance.userState.GetStage() - currZone * 100) + 45;
        }
        else if (currZone == 5)
        {
            trueStage = (ServerInterface.Instance.userState.GetStage() - currZone * 100) + 60;
        }
        print("GetStage : "+ ServerInterface.Instance.userState.GetStage());
        print("currZone : " + currZone);       //currStage = 0인 값이 저장된다.
        print("currStage : " + currStage);

        SoundObject.GetComponent<AudioSource>().clip = bgm;
        SoundObject.GetComponent<AudioSource>().loop = true;
        SoundObject.GetComponent<AudioSource>().Play();

        //스테이지 갱신
        for (int i = 0; i < currZone; i++)
        {
            if (i < (currZone - 1))
            {
                for (int j = 0; j < 15; j++)
                {
                    if (stageOpen[i, j] == false)
                    {
                        stageOpen[i, j] = true;
                    }
                }
            }
            else
            {
                for (int j = 0; j < currStage; j++)
                {
                    if (stageOpen[i, j] == false)
                    {
                        stageOpen[i, j] = true;
                        print(stageOpen[i, j]);
                    }
                }
            }
        }
        //sprite 초기화
        for (int i = 0; i < 15; i++)
        {
            if (stageOpen[0, i] == true)
            {
                stage0[i].GetComponent<tk2dSprite>().SetSprite("stage_s");
                stage0[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(i); j++)
                {
                    stage0[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }
            else if (stageOpen[0, i] != true)
            {
                stage0[i].GetComponent<tk2dSprite>().SetSprite("stage_n");
                stage0[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum_n;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(i); j++)
                {
                    stage0[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }

            if (stageOpen[1, i] == true)
            {
                stage1[i].GetComponent<tk2dSprite>().SetSprite("stage_s");
                stage1[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(15 + i); j++)
                {
                    stage1[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }
            else if (stageOpen[1, i] != true)
            {
                stage1[i].GetComponent<tk2dSprite>().SetSprite("stage_n");
                stage1[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum_n;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(15 + i); j++)
                {
                    stage1[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }

            if (stageOpen[2, i] == true)
            {
                stage2[i].GetComponent<tk2dSprite>().SetSprite("stage_s");
                stage2[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(30 + i); j++)
                {
                    stage2[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }
            else if (stageOpen[2, i] != true)
            {
                stage2[i].GetComponent<tk2dSprite>().SetSprite("stage_n");
                stage2[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum_n;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(30 + i); j++)
                {
                    stage2[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }

            if (stageOpen[3, i] == true)
            {
                stage3[i].GetComponent<tk2dSprite>().SetSprite("stage_s");
                stage3[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(45 + i); j++)
                {
                    stage3[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }
            else if (stageOpen[3, i] != true)
            {
                stage3[i].GetComponent<tk2dSprite>().SetSprite("stage_n");
                stage3[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum_n;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(45 + i); j++)
                {
                    stage3[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }

            if (stageOpen[4, i] == true)
            {
                stage4[i].GetComponent<tk2dSprite>().SetSprite("stage_s");
                stage4[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(60 + i); j++)
                {
                    stage4[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }
            else if (stageOpen[4, i] != true)
            {
                stage4[i].GetComponent<tk2dSprite>().SetSprite("stage_n");
                stage4[i].transform.GetChild(0).GetComponent<tk2dTextMesh>().font = StageNum_n;
                for (int j = 1; j <= ServerInterface.Instance.userState.userStage.GetStarNumber(45 + i); j++)
                {
                    stage3[i].transform.GetChild(j).GetComponent<tk2dSprite>().SetSprite("star_s");
                }
            }
        }
	
	}
	
	// Update is called once per frame
	void Update () {

        SoundControl();
        sideButton();
        

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //Application.LoadLevel("Lobby");
            SceneManager.LoadScene("Lobby");
        }


        if (Input.GetMouseButtonDown(0))
        {
            print("GetMouseButtonDown");
            print(isSlidingCheck);
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.name.Substring(0, 5) == "Stage" && isSlidingCheck == false)
            {

                GetComponent<AudioSource>().clip = btn_play;
                GetComponent<AudioSource>().Play();

                print(" Stage && isSlidingCheck");
                int hitNum = System.Convert.ToInt32(hit.collider.name.Substring(5, 2));
                selectStage = hitNum + 1;
                print(currStage + " cur!!!!");
                if (currBar == StageSelectBar[0] && stageOpen[0, hitNum] == true)//isStageOpen0[hitNum] == true)
                {
                    print(" currBar && StageSelectBar[0]");
                    selectZone = 1;

                    switch (hitNum)
                    {

                        case 0:
                            GoStage(hitNum);

                            break;
                        case 1:
                            GoStage(hitNum);
                            break;
                        case 2:
                            GoStage(hitNum);
                            break;
                        case 3:
                            GoStage(hitNum);
                            break;
                        case 4:
                            GoStage(hitNum);
                            break;
                        case 5:
                            GoStage(hitNum);
                            break;
                        case 6:
                            GoStage(hitNum);
                            break;
                        case 7:
                            GoStage(hitNum);
                            break;
                        case 8:
                            GoStage(hitNum);
                            break;
                        case 9:
                            GoStage(hitNum);
                            break;
                        case 10:
                            GoStage(hitNum);
                            break;
                        case 11:
                            GoStage(hitNum);
                            break;
                        case 12:
                            GoStage(hitNum);
                            break;
                        case 13:
                            GoStage(hitNum);
                            break;
                        case 14:
                            GoStage(hitNum);
                            break;
                    }
                }

                else if (currBar == StageSelectBar[1] && stageOpen[1, hitNum - 15] == true)//isStageOpen1[hitNum] == true)
                {
                    selectZone = 2;

                    switch (hitNum)
                    {
                        case 15:
                            GoStage(hitNum);
                            break;
                        case 16:
                            GoStage(hitNum);
                            break;
                        case 17:
                            GoStage(hitNum);
                            break;
                        case 18:
                            GoStage(hitNum);
                            break;
                        case 19:
                            GoStage(hitNum);
                            break;
                        case 20:
                            GoStage(hitNum);
                            break;
                        case 21:
                            GoStage(hitNum);
                            break;
                        case 22:
                            GoStage(hitNum);
                            break;
                        case 23:
                            GoStage(hitNum);
                            break;
                        case 24:
                            GoStage(hitNum);
                            break;
                        case 25:
                            GoStage(hitNum);
                            break;
                        case 26:
                            GoStage(hitNum);
                            break;
                        case 27:
                            GoStage(hitNum);
                            break;
                        case 28:
                            GoStage(hitNum);
                            break;
                        case 29:
                            GoStage(hitNum);
                            break;
                    }
                }

                else if (currBar == StageSelectBar[2] && stageOpen[2, hitNum - 30] == true)//isStageOpen2[hitNum] == true)
                {
                    selectZone = 3;

                    switch (hitNum)
                    {
                        case 30:
                            GoStage(hitNum);
                            break;
                        case 31:
                            GoStage(hitNum);
                            break;
                        case 32:
                            GoStage(hitNum);
                            break;
                        case 33:
                            GoStage(hitNum);
                            break;
                        case 34:
                            GoStage(hitNum);
                            break;
                        case 35:
                            GoStage(hitNum);
                            break;
                        case 36:
                            GoStage(hitNum);
                            break;
                        case 37:
                            GoStage(hitNum);
                            break;
                        case 38:
                            GoStage(hitNum);
                            break;
                        case 39:
                            GoStage(hitNum);
                            break;
                        case 40:
                            GoStage(hitNum);
                            break;
                        case 41:
                            GoStage(hitNum);
                            break;
                        case 42:
                            GoStage(hitNum);
                            break;
                        case 43:
                            GoStage(hitNum);
                            break;
                        case 44:
                            GoStage(hitNum);
                            break;
                    }
                }

                else if (currBar == StageSelectBar[3] && stageOpen[3, hitNum - 45] == true)//isStageOpen3[hitNum] == true)
                {
                    selectZone = 4;

                    switch (hitNum)
                    {
                        case 45:
                            GoStage(hitNum);
                            break;
                        case 46:
                            GoStage(hitNum);
                            break;
                        case 47:
                            GoStage(hitNum);
                            break;
                        case 48:
                            GoStage(hitNum);
                            break;
                        case 49:
                            GoStage(hitNum);
                            break;
                        case 50:
                            GoStage(hitNum);
                            break;
                        case 51:
                            GoStage(hitNum);
                            break;
                        case 52:
                            GoStage(hitNum);
                            break;
                        case 53:
                            GoStage(hitNum);
                            break;
                        case 54:
                            GoStage(hitNum);
                            break;
                        case 55:
                            GoStage(hitNum);
                            break;
                        case 56:
                            GoStage(hitNum);
                            break;
                        case 57:
                            GoStage(hitNum);
                            break;
                        case 58:
                            GoStage(hitNum);
                            break;
                        case 59:
                            GoStage(hitNum);
                            break;
                    }
                }
                else if (currBar == StageSelectBar[4] && stageOpen[4, hitNum - 60] == true)//isStageOpen3[hitNum] == true)
                {
                    selectZone = 5;

                    switch (hitNum)
                    {
                        case 60:
                            GoStage(hitNum);
                            break;
                        case 61:
                            GoStage(hitNum);
                            break;
                        case 62:
                            GoStage(hitNum);
                            break;
                        case 63:
                            GoStage(hitNum);
                            break;
                        case 64:
                            GoStage(hitNum);
                            break;
                        case 65:
                            GoStage(hitNum);
                            break;
                        case 66:
                            GoStage(hitNum);
                            break;
                        case 67:
                            GoStage(hitNum);
                            break;
                        case 68:
                            GoStage(hitNum);
                            break;
                        case 69:
                            GoStage(hitNum);
                            break;
                        case 70:
                            GoStage(hitNum);
                            break;
                        case 71:
                            GoStage(hitNum);
                            break;
                        case 72:
                            GoStage(hitNum);
                            break;
                        case 73:
                            GoStage(hitNum);
                            break;
                        case 74:
                            GoStage(hitNum);
                            break;
                    }
                }
            }
        }
	
	}


    void GoToLobby()
    {
        GetComponent<AudioSource>().clip = btn;
        GetComponent<AudioSource>().Play();

        //Application.LoadLevel("Lobby");
        SceneManager.LoadScene("Lobby");
    }
    void btnTouchLeft()
    {
        if(isSlidingCheck == false)
        {
            isSlidingCheck = true;
            
            GetComponent<AudioSource>().clip = btn_arrow;
            GetComponent<AudioSource>().Play();

            if(currBar == StageSelectBar[0])
            {

                currBar.transform.DOMoveX(-800, 0.5f, false);
                StageSelectBar[1].transform.DOMoveX(0, 0.5f, false);
                StageSelectBar[2].transform.DOMoveX(800, 0.5f, false);
                StageSelectBar[3].transform.DOMoveX(1600, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(2400, 0.5f, false);               
                currBar = StageSelectBar[1];
            }

            else if (currBar == StageSelectBar[1])
            {

                currBar.transform.DOMoveX(-800, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(-1600, 0.5f, false);
                StageSelectBar[2].transform.DOMoveX(0, 0.5f, false);
                StageSelectBar[3].transform.DOMoveX(800, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(1600, 0.5f, false);
                currBar = StageSelectBar[2];
            }

            else if (currBar == StageSelectBar[2])
            {

                currBar.transform.DOMoveX(-800, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(-2400, 0.5f, false);
                StageSelectBar[1].transform.DOMoveX(-1600, 0.5f, false);
                StageSelectBar[3].transform.DOMoveX(0, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(800, 0.5f, false);
                currBar = StageSelectBar[3];
            }

            else if (currBar == StageSelectBar[3])
            {

                currBar.transform.DOMoveX(-800, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(-3200, 0.5f, false);
                StageSelectBar[1].transform.DOMoveX(-2400, 0.5f, false);
                StageSelectBar[2].transform.DOMoveX(-1600, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(0, 0.5f, false);
                currBar = StageSelectBar[4];
            }

            else if (currBar == StageSelectBar[4])
            {
                isSlidingCheck = false;
                currBar = StageSelectBar[4];
            }
            ChangeZoneImage();
            slidingFlag();
        }
    }

    void btnTouchRight()
    {
        if (isSlidingCheck == false)
        {
            isSlidingCheck = true;

            GetComponent<AudioSource>().clip = btn_arrow;
            GetComponent<AudioSource>().Play();

            if (currBar == StageSelectBar[0])
            {

                isSlidingCheck = false;
                currBar = StageSelectBar[0];
            }

            else if (currBar == StageSelectBar[4])
            {
                currBar.transform.DOMoveX(800, 0.5f, false);
                StageSelectBar[3].transform.DOMoveX(0, 0.5f, false);
                StageSelectBar[2].transform.DOMoveX(-800, 0.5f, false);
                StageSelectBar[1].transform.DOMoveX(-1600, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(-2400, 0.5f, false);
                currBar = StageSelectBar[3];
            }

            else if (currBar == StageSelectBar[3])
            {

                currBar.transform.DOMoveX(800, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(1600, 0.5f, false);
                StageSelectBar[2].transform.DOMoveX(0, 0.5f, false);
                StageSelectBar[1].transform.DOMoveX(-800, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(-1600, 0.5f, false);
                currBar = StageSelectBar[2];
            }

            else if (currBar == StageSelectBar[2])
            {

                currBar.transform.DOMoveX(800, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(2400, 0.5f, false);
                StageSelectBar[3].transform.DOMoveX(1600, 0.5f, false);
                StageSelectBar[1].transform.DOMoveX(0, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(-800, 0.5f, false);
                currBar = StageSelectBar[1];
            }

            else if (currBar == StageSelectBar[1])
            {

                currBar.transform.DOMoveX(800, 0.5f, false);
                StageSelectBar[4].transform.DOMoveX(3200, 0.5f, false);
                StageSelectBar[3].transform.DOMoveX(2400, 0.5f, false);
                StageSelectBar[2].transform.DOMoveX(1600, 0.5f, false);
                StageSelectBar[0].transform.DOMoveX(0, 0.5f, false);
                currBar = StageSelectBar[0];
            }
            ChangeZoneImage();
            slidingFlag();
        }
    }

    void ChangeZoneImage()      //스테이지 이미지를 바꾸기 위한 함수
    {
        if(currBar == StageSelectBar[0])
        {
            BattleField.GetComponent<tk2dSprite>().SetSprite("stage0");
        }
        else if(currBar == StageSelectBar[1])
        {
            BattleField.GetComponent<tk2dSprite>().SetSprite("stage1");
        }
        else if (currBar == StageSelectBar[2])
        {
            BattleField.GetComponent<tk2dSprite>().SetSprite("stage2");
        }
        else if (currBar == StageSelectBar[3])
        {
            BattleField.GetComponent<tk2dSprite>().SetSprite("stage3");
        }
        else if (currBar == StageSelectBar[4])
        {
            BattleField.GetComponent<tk2dSprite>().SetSprite("stage4");
        }
    }

    void slidingFlag()
    {
        isSlidingCheck = false;
    }

    IEnumerator Time()
    {
        fntTime.text = "TIME " + Minutes.ToString() + ":" + Seconds.ToString();
        yield return new WaitForSeconds(1.0f);
        Seconds -= 1.0f;
        StartCoroutine(Time());
    }
    IEnumerator Play()
    {

        if (isSelectReady == false)
        {
            //popUpSmall.SetActive(true);       스테이지를 선택하지 않았을 경우.    팝업창을 띄운다.
            yield return new WaitForSeconds(1.0f);
            //popUpSmall.SetActive(false);
        }


        if (DataManage.GameCount > 0)
        {
            //Application.LoadLevel("Lobby");
            SceneManager.LoadScene("Lobby");
        }
    }

    void sideButton()
    {
        if (currBar == StageSelectBar[0])
        {
            leftButton.SetActive(false);
        }
        else if (currBar == StageSelectBar[4])
        {
            rightButton.SetActive(false);
        }
        else
        {
            rightButton.SetActive(true);
            leftButton.SetActive(true);
        }
    }

    void SoundControl()
    {

        if (Setting.bgmOX)
        {
            SoundObject.GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            SoundObject.GetComponent<AudioSource>().volume = 0;
        }

        if (Setting.soundOX)
        {
            GetComponent<AudioSource>().volume = 1;
        }
        else
        {
            GetComponent<AudioSource>().volume = 0;
        }
    }



    void GoStage(int hitStageNum)
    {

        for (int i = 0; i < 3; ++i)
        {
            ServerInterface.Instance.enemyState_L[i].attackAir += (ServerInterface.Instance.enemyState_L[i].attackAir / 10) * (hitStageNum + 1);
            ServerInterface.Instance.enemyState_L[i].attackGround += (ServerInterface.Instance.enemyState_L[i].attackGround / 10) * (hitStageNum + 1);
            ServerInterface.Instance.enemyState_L[i].MaxHp += (ServerInterface.Instance.enemyState_L[i].attackGround / 20) * (hitStageNum + 1);
        }

        if (LobbyManager.isLoad == false)
        {
           
            LobbyManager.isLoad = true;
            for (int i = 0; i < (int)SOURCE_GAME.TOTAL; ++i)
            {
                //DataManage._srcGame[i].SetEnergySmall(0);
                DataManage._srcGame[i].SetEnergy(0);
                DataManage._srcAct[i].SetEnergy(100);
                //DataManage._srcAct[i].SetEnergy(0);
            }
            DataManage.GameCount--;
            //Application.LoadLevel(4);
            SceneManager.LoadScene("Loading");
        }
    }

    void InitEnemyValue()
    {

        print("적들의 초기 공격력을 저장");
        for (int i = 0; i < 3; ++i)
        {

            InitEnemy[i,0] = ServerInterface.Instance.enemyState_L[i].attackGround.GetHashCode();
            InitEnemy[i,1] = ServerInterface.Instance.enemyState_L[i].attackAir.GetHashCode();
            InitEnemy[i,2] = ServerInterface.Instance.enemyState_L[i].MaxHp.GetHashCode();
        }
    }

    //void ChangeEnemyValue()
    //{
    //    print("적들의 초기 공격력으로 초기화");
    //    for(int i = 0; i < 3; ++i)
    //    {
    //        ServerInterface.Instance.enemyState_L[i].attackGround = InitEnemy[i][0];
    //        ServerInterface.Instance.enemyState_L[i].attackAir = InitEnemy[i][1];
    //        ServerInterface.Instance.enemyState_L[i].MaxHp = InitEnemy[i][2];
    //    }
    //}

}
