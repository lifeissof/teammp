﻿using UnityEngine;
using System.Collections;

public class GenPortionWave : MonoBehaviour
{

    public tk2dClippedSprite portion;
    Vector3 first_Pos;
    float Offset;

    bool Init = false;

    void Update()
    {
        if (Init == false)
        {
            Init = true;
            first_Pos = transform.position;
            
        }
        if (portion.ClipRect.y + 1 <= 0.5f)
        {
            transform.localScale = new Vector3((((portion.ClipRect.y + 1) * (portion.ClipRect.y + 1)) * (-6.2f)) + (portion.ClipRect.y + 1) * 5.5f, 0.5f, 0);
        }
        else if (portion.ClipRect.y + 1 > 0.5f)
        {
            transform.localScale = new Vector3(((1 - (portion.ClipRect.y + 1)) * (1 - (portion.ClipRect.y + 1))) * (-6.2f) + (1 - (portion.ClipRect.y + 1)) * 5.5f, 0.5f, 0);
        }
        if (portion.ClipRect.y + 1 == 1)
        {
            this.gameObject.GetComponent<Renderer>().enabled = false;
        }
        else
        {
            this.gameObject.GetComponent<Renderer>().enabled = true;
        }
        transform.position = first_Pos + new Vector3(0, ((portion.ClipRect.y + 1) * 130) , 0);

    }
}
