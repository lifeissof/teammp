﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour {


    public List<GameObject> characterSelectBar = new List<GameObject>();
    public GameObject currBar;
    public GameObject basisLeft;
    public GameObject basisRight;
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject SoundObject;

    public tk2dTextMesh fntTime;
    public tk2dTextMesh fntGameCount;

    public GameObject UserFractal;
    public GameObject UserFriendShip;
    public GameObject UserGold;
    public GameObject ExitObject;

    float Minutes;
    float Seconds;


    public AudioClip bgm;
    public AudioClip btn_play;
    public AudioClip btn_arrow;
    public AudioClip btn;



    bool isSliding;
    bool isPlayReady;

    public static bool isLoad = false;


	// Use this for initialization
	void Start () {


        UserFriendShip.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFriendshipPoint()); // 우정포인트 정보 서버에서 가져오기
        UserFractal.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetFractal());
        UserGold.transform.GetChild(1).GetComponent<tk2dTextMesh>().text = System.Convert.ToString(ServerInterface.Instance.userState.GetGold()); 


        isSliding = false;
        isPlayReady = false;
        isLoad = false;

        Minutes = 5.0f;
        Seconds = 59.0f;
        StartCoroutine("Time");

        fntGameCount.text = "x " + DataManage.GameCount.ToString();

        if (Setting.bgmOX)
        {
            SoundObject.GetComponent<AudioSource>().clip = bgm;
            SoundObject.GetComponent<AudioSource>().loop = true;
            SoundObject.GetComponent<AudioSource>().Play();
        }


	
	}
	
	// Update is called once per frame
	void Update () {
            
        sideButton();

         if (Input.GetKey("escape"))
         {
             ExitObject.SetActive(true);
         }
        
	}

     void btnTouchLeft()
    {
       if (isSliding == false)
        {
          isSliding = true;

          GetComponent<AudioSource>().clip = btn_arrow;
          GetComponent<AudioSource>().Play();


              if (currBar == characterSelectBar[0])
              {
                  currBar.transform.DOMoveX(-800, 0.5f, false);
                  characterSelectBar[1].transform.DOMoveX(0, 0.5f, false);
                  characterSelectBar[2].transform.DOMoveX(800, 0.5f, false);

                  currBar = characterSelectBar[1];
              }

              else if(currBar == characterSelectBar[1])
              {

                  currBar.transform.DOMoveX(-800, 0.5f, false);
                  characterSelectBar[2].transform.DOMoveX(0, 0.5f, false);
                  characterSelectBar[0].transform.DOMoveX(basisLeft.transform.position.x, 0.5f, false);

                  currBar = characterSelectBar[2];
              }

              else if (currBar == characterSelectBar[2])
              {

                  
                  isSliding = false;
                  currBar = characterSelectBar[2];
              }
              slidingFlag();
        }

    }

     void btnTouchRight()
     {
         if (isSliding == false)
         {
             isSliding = true;

             GetComponent<AudioSource>().clip = btn_arrow;
             GetComponent<AudioSource>().Play();

             if (currBar == characterSelectBar[2])
             {
                 currBar.transform.DOMoveX(800, 0.5f, false);
                 characterSelectBar[1].transform.DOMoveX(0, 0.5f, false);
                 characterSelectBar[0].transform.DOMoveX(-800, 0.5f, false);

                 currBar = characterSelectBar[1];

             }
             else if (currBar == characterSelectBar[1])
             {
                 currBar.transform.DOMoveX(800, 0.5f, false);
                 characterSelectBar[0].transform.DOMoveX(0, 0.5f, false);
                 characterSelectBar[2].transform.DOMoveX(basisRight.transform.position.x, 0.5f, false);

                 currBar = characterSelectBar[0];

             }
             else if (currBar == characterSelectBar[0])
             {
                 isSliding = false;
                 currBar = characterSelectBar[0];
             }
             slidingFlag();
         }

     }
    void sideButton()
     {
         if (currBar == characterSelectBar[0])
         {
             leftButton.SetActive(false);
         }
         else if(currBar == characterSelectBar[2])
         {
             rightButton.SetActive(false);
         }
         else
         {
             rightButton.SetActive(true);
             leftButton.SetActive(true);
         }
     }

     void slidingFlag()
     {
         isSliding = false;
     }


     void SoundControl()
     {
         if (Setting.bgmOX)
         {
             SoundObject.GetComponent<AudioSource>().volume = 1;
         }
         else
         {
             SoundObject.GetComponent<AudioSource>().volume = 0;
         }
         if (Setting.soundOX)
         {
             GetComponent<AudioSource>().volume = 1;
         }
         else
         {
             GetComponent<AudioSource>().volume = 0;
         }
     }

     void ExitFuncYes()
     {
         GetComponent<AudioSource>().clip = btn;
         GetComponent<AudioSource>().Play();
         Application.Quit();
     }

     void ExitFuncNo()
     {
         GetComponent<AudioSource>().clip = btn;
         GetComponent<AudioSource>().Play();
         ExitObject.SetActive(false);
     }


     void PlayFunc()
     {
         StopCoroutine("Play");
         StartCoroutine("Play");

     }
     void HiveFunc()
     {
             isLoad = true;
             GetComponent<AudioSource>().clip = btn;
             GetComponent<AudioSource>().Play();
             SceneManager.LoadScene("Chamber");
    }

     void EvolutionFunc()
     {

         if (isLoad == false)
         {
             isLoad = true;
             GetComponent<AudioSource>().clip = btn;
             GetComponent<AudioSource>().Play();
            //Application.LoadLevel("Upgrade");
            SceneManager.LoadScene("Upgrade");
        }
     }

     void NewHiveFunc()
     {

         if (isLoad == false)
         {
             isLoad = true;
             GetComponent<AudioSource>().clip = btn;
             GetComponent<AudioSource>().Play();
            //Application.LoadLevel("NewHive");
            SceneManager.LoadScene("NewHive");
        }
     }
    IEnumerator FriendsFunc()
    {
        if(PlayerPrefs.GetString("utoken") == "testid")
        {
            ServerInterface.Instance.GetFriendsMonster("friend");
        }
        else if(PlayerPrefs.GetString("utoken") == "friend")
        {
            ServerInterface.Instance.GetFriendsMonster("testid");
        }
        yield return new WaitForSeconds(0.1f);
        DataManage._friendEntrySlot = DataManage._monster[System.Convert.ToInt32(DataManage._friendEntrySlot.monsterNumber)];

    }
    IEnumerator Time()
     {
         fntTime.text = "TIME " + Minutes.ToString() + ":" + Seconds.ToString();
         yield return new WaitForSeconds(1.0f);
         Seconds -= 1.0f;
         StartCoroutine(Time());
     }
    IEnumerator Play()
     {

        if(isPlayReady == false)
        {
            //popUpSmall.SetActive(true);       몬스터를 넣지 않았을 경우.    팝업창을 띄운다.
            yield return new WaitForSeconds(0.3f);
            //popUpSmall.SetActive(false);
        }


        if (isLoad == false)
        {
            if (DataManage.GameCount > 0)
            {
                GetComponent<AudioSource>().clip = btn_play;
                GetComponent<AudioSource>().Play();
                //Application.LoadLevel("Stage");
                SceneManager.LoadScene("Stage");
            }
        }
     }

}
