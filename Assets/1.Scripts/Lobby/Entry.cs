﻿using UnityEngine;
using System.Collections;

public class Entry : MonoBehaviour {


    Ray ray;
    RaycastHit hit;

    public GameObject[] entryLock = new GameObject[2];          //엔트리를 잠구는 목록 2개
    public GameObject[] entryslot = new GameObject[5];          //총 슬롯 5개
    public GameObject friendEntryslot;
    public tk2dSpriteAnimation[] entryLib = new tk2dSpriteAnimation[15];    //현재 리소스가 있는 캐릭터 15개의 배열

    public AudioClip btn_register_ok;
    public AudioClip btn_register_cancel;
    public AudioClip btn_register_fail;

    
    
    void OnEnable()
    {
        //OnEnable()은 게임오브젝트가 활성화되면 실행할 함수를 호출하는 기능이다.
        EntrySlotSetting();           //엔트리 슬롯을 몇개 열것인지 -> 캐릭터를 몇개를 사용할것인지.
        SlotSetting();
        FriendSlotSetting();
     
    }

    void OnDisable()
    {
        //OnDisable()은게임오브젝트가 비활성화되면 실행할 함수를 코딩

    }

    void EntrySlotSetting()
    {
        print(DataManage.entrySlotNum);
        switch (DataManage.entrySlotNum)
        {
            case 3:
                {
                    entryslot[3].SetActive(false);
                    entryslot[4].SetActive(false);
                    //entryslot[5].SetActive(true);
                    entryLock[0].SetActive(true);
                    entryLock[1].SetActive(true);
                    break;
                }
            case 4:
                {
                    entryslot[3].SetActive(true);
                    entryslot[4].SetActive(false);
                    //entryslot[5].SetActive(true);
                    entryLock[0].SetActive(false);
                    entryLock[1].SetActive(true);
                    break;
                }
            case 5:
                {
                    entryslot[3].SetActive(true);
                    entryslot[4].SetActive(true);
                    //entryslot[5].SetActive(true);
                    entryLock[0].SetActive(false);
                    entryLock[1].SetActive(false);
                    break;
                }
            default:
                {
                    entryslot[3].SetActive(false);
                    entryslot[4].SetActive(false);
                    //entryslot[5].SetActive(true);
                    entryLock[0].SetActive(true);
                    entryLock[1].SetActive(true);
                    break;
                }
        }
    }

    void SlotSetting()
    {
        
        for (int i = 0; i < DataManage.entrySlotNum; ++i)           
        {
            tk2dSpriteAnimator anim = entryslot[i].GetComponent<tk2dSpriteAnimator>();

            if (DataManage._entrySlot[i].monsterName != "None")
            {
                entryslot[i].name = string.Format("Character{0}", DataManage._entrySlot[i].monsterNumber);
                anim.enabled = true;
                anim.Library = entryLib[System.Convert.ToInt32(DataManage._entrySlot[i].monsterNumber)];
                anim.Play(entryslot[i].name);
            }
            else
            {
                entryslot[i].name = "None";
                tk2dSprite sprite = entryslot[i].GetComponent<tk2dSprite>();
                anim.Library = entryLib[0];
                anim.Play("Character00");
                anim.enabled = false;
                int ID_x = sprite.GetSpriteIdByName("character00_h");
                sprite.spriteId = ID_x;
            }
        }
        ServerInterface.Instance.SetEntryMonster();
    }

    public void FriendSlotSetting()
    {
        tk2dSpriteAnimator anim = friendEntryslot.GetComponent<tk2dSpriteAnimator>();

        if (DataManage._friendEntrySlot.monsterName != "None")
        {
            //print("DSADSA");
            friendEntryslot.name = string.Format("Character{0}", DataManage._friendEntrySlot.monsterNumber);
            //print(friendEntryslot.name);
            anim.enabled = true;
            anim.Library = entryLib[System.Convert.ToInt32(DataManage._friendEntrySlot.monsterNumber)];
            anim.Play(friendEntryslot.name);
        }
        else
        {
            friendEntryslot.name = "None";
            tk2dSprite sprite = friendEntryslot.GetComponent<tk2dSprite>();
            anim.Library = entryLib[0];
            anim.Play("Character00");
            anim.enabled = false;
            int ID_x = sprite.GetSpriteIdByName("character00_h");
            sprite.spriteId = ID_x;
        }
        //print("FriendSlotSetting is Done");
    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        FriendSlotSetting();
        if (Input.GetMouseButtonDown(0))
        {

            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit) && hit.collider.tag.Substring(0, 5) == "Entry")
            {
                GetComponent<AudioSource>().clip = btn_register_cancel;
                GetComponent<AudioSource>().Play();
                
                int hitNum = System.Convert.ToInt32(hit.collider.tag.Substring(5, 1));
                DataManage._entrySlot[hitNum] = DataManage.monsterNone;
                //DataManage._friendEntrySlot = DataManage._monster[System.Convert.ToInt32(DataManage._friendEntrySlot.monsterNumber)];
                //this.SendMessage("SlotSetting");
                SlotSetting();
                
            }

            else if (Physics.Raycast(ray, out hit) && hit.collider.tag.Substring(0, 5) == "Frien")
            {
                GetComponent<AudioSource>().clip = btn_register_cancel;
                GetComponent<AudioSource>().Play();

                DataManage._friendEntrySlot = DataManage.monsterNone;

                FriendSlotSetting();
            }
        }

        else if (Physics.Raycast(ray, out hit) && hit.collider.name.Substring(0, 4) == "Char" && hit.collider.tag != "FriendLeader")
        {
            
            int num = System.Convert.ToInt32(hit.collider.name.Substring(9, 2));
            if (DataManage._monster[num].monsterExist == true)       //캐릭터가 있는지 없는지 판별하는 조건문
            {

                bool entry_ok = true;
                string hitName = hit.collider.name;
                
                for (int i = 0; i < DataManage.entrySlotNum; ++i)
                {
                    if (entryslot[i].name == hitName)
                    {
                        entry_ok = false;
                        break;
                    }
                }
                if (entry_ok == true)
                {
                    int entryNum = 0;
                    for (int i = 0; i < DataManage.entrySlotNum; ++i)
                    {
                        if (DataManage._entrySlot[i].monsterName == "None")
                        {
                            GetComponent<AudioSource>().clip = btn_register_ok;
                            GetComponent<AudioSource>().Play();

                            //if (i == 0)리더 시스템은 나중에.
                            //{
                            //    leader = System.Convert.ToInt32(hitName.Substring(9, 2));
                            //}
                            

                            DataManage._entrySlot[i] = DataManage._monster[System.Convert.ToInt32(hitName.Substring(9, 2))];
                            
                            //this.SendMessage("SlotSetting");
                            SlotSetting();
                            //FriendSlotSetting();
                            break;
                        }
                        else
                            entryNum++;
                    }
                    if (entryNum == DataManage.entrySlotNum)//엔트리가 꽉참
                    {
                        
                    }
                }
                else    //엔트리에 이미 있을때
                {

                }
            }
            else    // 없는 유닛 등록시
            {
            }
        }
	}
}
