﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Setting : MonoBehaviour
{

    public tk2dSprite settingBtn;
    public tk2dSprite soundBtn;
    public tk2dSprite bgmBtn;
    public tk2dSprite homeBtn;

    public GameObject ColliderBg;


    public static bool isSet = false;
    public static bool soundOX = true;
    public static bool bgmOX = true;

    public AudioClip btn;

    Ray ray;
    RaycastHit hit;


    //bool isLogedIn;
    //string uid;
    //string upass;
    public BattleScroll battleScroll = new BattleScroll();

    void Start()
    {
        LoadSettingData();
        
        //CheckLoginInfo();
    }

    void Update()
    {
        if (Input.GetKeyDown("a"))
        {
            HomeBtn();
        }
        LoadSettingData();
      
    }

    //public void Login()
    //{
    //    if (isLogedIn == false)     // 로그인 하는 부분
    //    {
    //        uid = "id";
    //        upass = "123456";
    //        PlayerPrefs.SetString("uid", uid);
    //        PlayerPrefs.SetString("upass", upass);
    //    }
    //    else        // 이미 로그인을 했을 경우
    //    {
    //        print("로그인시도");
    //    }
    //}

    public void SettingBtn()
    {
        if (soundOX)
        {
            GetComponent<AudioSource>().clip = btn;
            GetComponent<AudioSource>().Play();
        }
        if (isSet == false)
        {
            Time.timeScale = 0;
            int id = settingBtn.GetSpriteIdByName("setting_n");
            settingBtn.spriteId = id;
            soundBtn.gameObject.SetActive(true);
            bgmBtn.gameObject.SetActive(true);
            homeBtn.gameObject.SetActive(true);
            ColliderBg.SetActive(true);
            isSet = true;
        }
        else
        {
            Time.timeScale = 1;
            int id = settingBtn.GetSpriteIdByName("setting_n");
            settingBtn.spriteId = id;
            soundBtn.gameObject.SetActive(false);
            bgmBtn.gameObject.SetActive(false);
            homeBtn.gameObject.SetActive(false);
            ColliderBg.SetActive(false);
            isSet = false;
        }
      
    }

    public void SoundBtn()
    {
        if (soundOX == false)
        {
            int id = soundBtn.GetSpriteIdByName("sound_n");
            soundBtn.spriteId = id;
            soundOX = true;
        }
        else
        {
            int id = soundBtn.GetSpriteIdByName("sound_s");         
            soundBtn.spriteId = id;
            soundOX = false;
        }
       
    }
    public void BgmBtn()
    {
        
        if (bgmOX == false)
        {
            int id = bgmBtn.GetSpriteIdByName("bgm_n");
            bgmBtn.spriteId = id;
            bgmOX = true;
        }
        else
        {
            int id = bgmBtn.GetSpriteIdByName("bgm_s");
            bgmBtn.spriteId = id;
            bgmOX = false;
        }
       
       
    }

    public void HomeBtn()
    {
        settingBtn.GetSpriteIdByName("setting_s");
        isSet = false;
        LobbyManager.isLoad = false;
        Time.timeScale = 1;
        //Unit._lst_Ally.Clear();
        //Unit._lst_Enemy.Clear();
        //BattleScroll.isopening = true;
        //BattleScroll.isending = false;
        ////if (Tutorial.stepNumber == 0)
        ////{
        ////    Box.createCount = 0;
        ////    tutorial.ReleaseEvent();
        ////    puzzle.ReleaseEvent();
        ////}
        //PlanetMonster.AllyDie -= new PlanetMonster.ScrollHandler(battleScroll.ScrollMove);
        //SceneManager.LoadScene("Lobby");


        for (int i = 0; i < Unit._lst_Ally.Count; ++i)
        {
            Unit._lst_Ally[i].SendMessage("Ending");
        }
        for (int i = 0; i < Unit._lst_Enemy.Count; ++i)
        {
            Unit._lst_Enemy[i].SendMessage("Ending");
            print(2);
        }

        Unit._lst_Ally.Clear();
        Unit._lst_Enemy.Clear();
        battleScroll.SendMessage("ChangeEnemyValue");
        God.playOnce = true;
        BattleScroll.isopening = true;
        BattleScroll.isending = false;
        SceneManager.LoadScene("Lobby");

    }

    public static void SaveSettingData()
    {
        PlayerPrefs.SetInt("bgmOX", (bgmOX ? 1 : 2));
        PlayerPrefs.SetInt("soundOX", (soundOX ? 1 : 2));
    }

    void LoadSettingData()
    {
        if (PlayerPrefs.GetInt("bgmOX") == 1 || bgmOX == true)
        {
            int id = bgmBtn.GetSpriteIdByName("bgm_n");
            bgmBtn.spriteId = id;
            bgmOX = true;
        }
        else if (PlayerPrefs.GetInt("bgmOX") == 2 || bgmOX == false)
        {
            int id = bgmBtn.GetSpriteIdByName("bgm_s");
            bgmBtn.spriteId = id;
            bgmOX = false;
        }

        if (PlayerPrefs.GetInt("soundOX") == 1 || soundOX == true)
        {
            int id = soundBtn.GetSpriteIdByName("sound_n");
            soundBtn.spriteId = id;
            soundOX = true;
        }
        else if (PlayerPrefs.GetInt("soundOX") == 2 || soundOX == false)
        {
            int id = soundBtn.GetSpriteIdByName("sound_s");
            soundBtn.spriteId = id;
            soundOX = false;
        }
    }

    //void CheckLoginInfo()
    //{
    //    if (PlayerPrefs.GetString(uid).Length == 0 || PlayerPrefs.GetString(upass).Length == 0)
    //    {
    //        isLogedIn = false;
    //    }
    //}
}
