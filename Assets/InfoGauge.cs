﻿using UnityEngine;
using System.Collections;

public class InfoGauge : MonoBehaviour {

    public void SetStat(float hp, float air, float ground, float speed)
    {
        if (hp == 0) this.gameObject.transform.FindChild("BarHp_F").gameObject.transform.localScale = new Vector3(0, 0, 0);
        else
        {
            this.gameObject.transform.FindChild("BarHp_F").transform.localScale = new Vector3(1, 1, 1);
            
            this.gameObject.transform.FindChild("BarHp_F").GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(hp * 200.0f * 0.001f + 20, 21);
        }

        if (air == 0) this.gameObject.transform.FindChild("BarAttackAir_F").transform.localScale = new Vector3(0, 0, 0);
        else
        {
            this.gameObject.transform.FindChild("BarAttackAir_F").transform.localScale = new Vector3(1, 1, 1);
            this.gameObject.transform.FindChild("BarAttackAir_F").GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(air * 200.0f * 0.001f + 20, 21);
        }

        if (ground == 0) this.gameObject.transform.FindChild("BarAttackGround_F").transform.localScale = new Vector3(0, 0, 0);
        else
        {
            this.gameObject.transform.FindChild("BarAttackGround_F").transform.localScale = new Vector3(1, 1, 1);
            this.gameObject.transform.FindChild("BarAttackGround_F").GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(ground * 200.0f * 0.001f + 20, 21);
        }

        if (speed == 0) this.gameObject.transform.FindChild("BarSpeed_F").transform.localScale = new Vector3(0, 0, 0);
        else
        {
            this.gameObject.transform.FindChild("BarSpeed_F").transform.localScale = new Vector3(1, 1, 1);
            this.gameObject.transform.FindChild("BarSpeed_F").GetComponent<tk2dSlicedSprite>().dimensions = new Vector2(speed * 200.0f * 0.001f + 20, 21);
        }
    }
}


